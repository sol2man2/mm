package com.lge.config.marquee.test;

import junit.framework.TestCase;
import marquee.adlib.utils.MarqueeEntityInfo;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.lge.config.marquee.control.MarqueeManagerControl;
import com.lge.config.marquee.control.MarqueeManagerControlInterface;
import com.lge.config.marquee.model.MarqueeManager;

public class MarqueeManagerControlTestCase  extends TestCase {

	private MarqueeManagerControlInterface controlMarqueeInterface = null;
	private MarqueeManagerControl	controlMarquee = null;
	private MarqueeManager	modelMarquee = null;	
	
	@Before
	protected void setUp() throws Exception {		
		super.setUp();
		
		modelMarquee			= new MarqueeManager();
		controlMarqueeInterface = new MarqueeManagerControl(modelMarquee);
		controlMarquee			= new MarqueeManagerControl(modelMarquee);
	}

	@After
	protected void tearDown() throws Exception {
		super.tearDown();
	}

//	@Test
//	public void testGetMarqueeCount() {	
//		if (2 == controlMarqueeInterface.getMarqueeCount()) {
//			System.out.println("getMarqueeCount() is PASS\n");
//		}
//		else {
//			System.out.println("getMarqueeCount() is FAIL\n");
//		}
//	}
//	
//	@Test
//	public void testAddNewMarqueeEntity() {	
//		boolean result = false;
//		int count = controlMarqueeInterface.getMarqueeCount();
//		
//		result = controlMarqueeInterface.addNewMarqueeEntity(new MarqueeEntityInfo(0, 3333, 11, "marqueeApplication_v1.1.java", true, 1, "testpkg3.tar", true, 22, 1366, 768));
//		
//		if (result) {
//			if ((count + 1) == controlMarqueeInterface.getMarqueeCount()) {
//				System.out.println(" Add Device(ID : 3333) is PASS\n");
//			}
//			else {
//				System.out.println("Add Device(ID : 3333) is FAIL\n");
//			}		
//		} else {
//			System.out.println("Add Device(ID : 3333) is FAIL\n");
//		}
//	}
//	
//	@Test
//	public void testRemoveMarqueeEntity() {		
//		int count = controlMarqueeInterface.getMarqueeCount();
//		int index = count-2;
//		
//		controlMarqueeInterface.removeMarqueeEntity(controlMarqueeInterface.getMarqueeEntity(index));
//		
//		if ((count - 1) == controlMarqueeInterface.getMarqueeCount()) {
//			System.out.println("Remove index " + (index) + ": PASS\n");
//		}
//		else {
//			System.out.println("Remove index " + (index) + ": FAIL\n");
//		}
//	}
//	
	@Test
	public void testRemoveAllMarqueeEntity() {				
		controlMarqueeInterface.removeAllMarqueeEntity();
	}
//	
//	@Test
//	public void testOberserPattern() {				
//		modelMarquee.update(new MarqueeEntityInfo(0, 6789, 11, "marqueeApplication_v1.1.jar", true, 1, "testpkg3.tar", true, 22, 1366, 768));
//	}
	
//	@Test
//	public void testCheckMarqueeUpdate() {		
//		MarqueeEntityInfo marqueeInfo = new MarqueeEntityInfo(0, 1111, 0, null, false, 0, null, false, 0, 0, 0);
//		
//		controlMarquee.checkMaqueeUpdate(marqueeInfo);
//	}
}
