package com.lge.config.comm.test;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.net.Socket;
import java.net.UnknownHostException;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.lge.adlib.comm.AdPackageRequest;
import com.lge.adlib.comm.AdPackageResponse;
import com.lge.adlib.comm.SoftwareRequest;
import com.lge.adlib.comm.SoftwareResponse;
import com.lge.adlib.comm.SystemInfo;
import com.lge.config.comm.UpdateServer;

public class UpdateServerTestcase {

	private final int BUFFER_SIZE = 1024 * 8;
	UpdateServer updateServer = null;

	@Before
	public void setUp() throws Exception {
		updateServer = UpdateServer.startUpdateServer();
	}

	@After
	public void tearDown() throws Exception {
		boolean result = UpdateServer.stopUpdateServer();
	}
/*
	@Ignore
	public void testUpdate() {
		requestUpdate();
	}
*/
	private void requestUpdate() {
		Socket socket = null;

		InputStream isSocket = null;
		BufferedInputStream bisSocket = null;
		ObjectInputStream oisSocket = null;

		OutputStream osSocket = null;
		BufferedOutputStream bosSocket = null;
		ObjectOutputStream oosSocket = null;

		try {
			socket = new Socket("192.168.123.177", 8886);

			osSocket = socket.getOutputStream();
			bosSocket = new BufferedOutputStream(osSocket, 1024 * 8);
			oosSocket = new ObjectOutputStream(bosSocket);

			SoftwareRequest softwareRequest = new SoftwareRequest();
			softwareRequest.setVersion(101L);
			oosSocket.writeObject(softwareRequest);
			oosSocket.flush();

			isSocket = socket.getInputStream();
			bisSocket = new BufferedInputStream(isSocket, 1024 * 8);
			oisSocket = new ObjectInputStream(bisSocket);

			SoftwareResponse softwareResponse = (SoftwareResponse) oisSocket.readObject();

			File f = new File("received_" + softwareResponse.getFileName());
			FileOutputStream fos = new FileOutputStream(f);
			BufferedOutputStream bosFile = new BufferedOutputStream(fos, 1024 * 8);

			final int buffLength = 1024 * 8;
			byte[] buff = new byte[buffLength];
			int pos = 0;

			int receivedSize = 0;
			int receivedTotalSize = 0;

			while((receivedSize = bisSocket.read(buff, pos, buffLength)) != -1) {
				bosFile.write(buff, 0, receivedSize);
				bosFile.flush();
				receivedTotalSize += receivedSize;
			}

			fos.close();
			bosFile.close();
			System.out.println("response: code: " + softwareResponse.getResponseCode() + ", version: " + softwareResponse.getVersion() + ", total: " + receivedTotalSize);
		} catch (UnknownHostException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
		}
		
		try {
			oisSocket.close();
			bisSocket.close();
			isSocket.close();
			
			oosSocket.close();
			bosSocket.close();
			osSocket.close();

			socket.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@Test
	public void testUpdateSoftware() {
		requestSoftware();
	}
	private void requestSoftware() {

		Socket socket = null;

		InputStream is = null;
		BufferedInputStream bis = null;
		ObjectInputStream ois = null;

		OutputStream os = null;
		BufferedOutputStream bos = null;
		ObjectOutputStream oos = null;

		try {
			socket = new Socket(SystemInfo.SERVER_ADDRESS, SystemInfo.SERVER_PORT);

			//	open output stream
			os = socket.getOutputStream();
			bos = new BufferedOutputStream(os, 1024 * 8);
			oos = new ObjectOutputStream(bos);

			//	send software request
			SoftwareRequest softwareRequest = new SoftwareRequest();
			softwareRequest.setVersion(101L);
			oos.writeObject(softwareRequest);
			oos.flush();

			//	open input stream
			is = socket.getInputStream();
			bis = new BufferedInputStream(is, 1024 * 8);
			ois = new ObjectInputStream(bis);

			//	receive response
			SoftwareResponse softwareResponse = (SoftwareResponse) ois.readObject();

			//	receive software image
			File f = new File("received_" + softwareResponse.getFileName());
			FileOutputStream fos = new FileOutputStream(f);
			BufferedOutputStream bosFile = new BufferedOutputStream(fos, BUFFER_SIZE);

			final int buffLength = BUFFER_SIZE;
			byte[] buff = new byte[buffLength];

			int receivedSize = 0;
			int receivedTotalSize = 0;

			long sizeToReceive = softwareResponse.getFileSize();

			for(;;) {
				if( sizeToReceive >= buffLength ){
					receivedSize = bis.read(buff, 0, buffLength);
					sizeToReceive -= receivedSize;
					bosFile.write(buff, 0, receivedSize);
					bosFile.flush();
				} else {
					receivedSize = bis.read(buff, 0, (int)sizeToReceive);
					sizeToReceive -= receivedSize;
					bosFile.write(buff, 0, receivedSize);
					bosFile.flush();
					if( sizeToReceive == 0 ) break;
				}
			}

			fos.close();
			bosFile.close();
			
			//	send adpackage request
			AdPackageRequest adPackageRequest = new AdPackageRequest();

			adPackageRequest.setVersion(201L);
			oos.writeObject(adPackageRequest);
			oos.flush();

			//	receive response
			AdPackageResponse adPackageResponse = (AdPackageResponse) ois.readObject();

			//	receive adpackage image
			f = new File("received_" + adPackageResponse.getFileName());
			fos = new FileOutputStream(f);
			bosFile = new BufferedOutputStream(fos, BUFFER_SIZE);

			receivedSize = 0;
			receivedTotalSize = 0;

			sizeToReceive = adPackageResponse.getFileSize();

			for(;;) {
				if( sizeToReceive >= buffLength ){
					receivedSize = bis.read(buff, 0, buffLength);
					sizeToReceive -= receivedSize;
					bosFile.write(buff, 0, receivedSize);
					bosFile.flush();
				} else {
					receivedSize = bis.read(buff, 0, (int)sizeToReceive);
					sizeToReceive -= receivedSize;
					bosFile.write(buff, 0, receivedSize);
					bosFile.flush();
					if( sizeToReceive == 0 ) break;
				}
			}

			fos.close();
			bosFile.close();

			System.out.println("response: code: " + softwareResponse.getResponseCode() + ", version: " + softwareResponse.getVersion() + ", total: " + receivedTotalSize);
		} catch (UnknownHostException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
		}
		
		try {
			ois.close();
			bis.close();
			is.close();
			
			oos.close();
			bos.close();
			os.close();
			
			socket.close();
		} catch (IOException e) {
			e.printStackTrace();
		}

		System.out.println("client out");
	}
}
