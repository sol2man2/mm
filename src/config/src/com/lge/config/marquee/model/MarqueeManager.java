package com.lge.config.marquee.model;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.LinkedList;
import java.util.Observable;

import marquee.adlib.utils.MarqueeEntityInfo;
import marquee.adlib.utils.MarqueeResourcePath;

public class MarqueeManager extends Observable implements MarqueeManagerInterface {
	private String marqueeListDB = null;
	private LinkedList<MarqueeEntityInfo> marqueeLinkedLists;

	public MarqueeManager() {
		
		this.marqueeListDB	= MarqueeResourcePath.getInstance().getConfigPath() + "/marqueeLists.dat";

		if(!this.loadData()) {
			marqueeLinkedLists = new LinkedList<MarqueeEntityInfo>();			
			this.fillMarquee();		
		}
		
		if(0 == marqueeLinkedLists.size()) {
			marqueeLinkedLists = new LinkedList<MarqueeEntityInfo>();			
			this.fillMarquee();		
		}
		
		this.saveData();
//		this.printMarquee();
	}
	
   public void update(Object marqueeInfo) {
      setChanged();
      notifyObservers(marqueeInfo);
      //notifyObservers(n);
      //위와같이 notifyObservers()를 호출할 때 아규먼트를 전달하면 Observer.update()메소드의 2번째 파라미터로 전달됨.
   }

	private void saveData() {
		File file = new File(this.marqueeListDB);
		if(  file.exists() )
			file.delete();

		FileOutputStream fos 	= null;
		ObjectOutputStream oos 	= null;
		try {
			fos = new FileOutputStream(file);
			oos = new ObjectOutputStream(fos);

			oos.writeObject(marqueeLinkedLists);
			oos.flush();
		} catch (FileNotFoundException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			try {
				oos.close();
				fos.close();
			} catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
	
	@SuppressWarnings("unchecked")
	private boolean loadData() {
		File file = new File(this.marqueeListDB);
		
		if( !file.exists() )
			return false;
		
		FileInputStream fis 	= null;
		ObjectInputStream ois 	= null;
		
		Object obj = null;
		try {
			fis = new FileInputStream(file);
			ois = new ObjectInputStream(fis);

			obj = ois.readObject();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			try {
				ois.close();
				fis.close();
			} catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		marqueeLinkedLists = (LinkedList<MarqueeEntityInfo>) obj;
		
		return true;
	}

	private void fillMarquee () {
		marqueeLinkedLists.add(new MarqueeEntityInfo(0L, 1111L, 0L, "marquee.jar", true, 0L, "AdPac_v1.tar", true, 42L, 1920L, 1080L));
		marqueeLinkedLists.add(new MarqueeEntityInfo(0L, 2222L, 0L, "marquee.jar", true, 0L, "AdPac_v1.tar", true, 22L, 1366L, 768L));
		marqueeLinkedLists.add(new MarqueeEntityInfo(0L, 3333L, 0L, "marquee.jar", true, 0L, "AdPac_v1.tar", true, 22L, 1366L, 768L));
		marqueeLinkedLists.add(new MarqueeEntityInfo(0L, 4444L, 0L, "marquee.jar", true, 0L, "AdPac_v1.tar", true, 22L, 1366L, 768L));
		marqueeLinkedLists.add(new MarqueeEntityInfo(0L, 5555L, 0L, "marquee.jar", true, 0L, "AdPac_v1.tar", true, 22L, 1366L, 768L));
		marqueeLinkedLists.add(new MarqueeEntityInfo(0L, 6666L, 0L, "marquee.jar", true, 0L, "AdPac_v1.tar", true, 22L, 1366L, 768L));
		marqueeLinkedLists.add(new MarqueeEntityInfo(0L, 7777L, 0L, "marquee.jar", true, 0L, "AdPac_v1.tar", true, 22L, 1366L, 768L));
		marqueeLinkedLists.add(new MarqueeEntityInfo(0L, 8888L, 0L, "marquee.jar", true, 0L, "AdPac_v1.tar", true, 22L, 1366L, 768L));
		marqueeLinkedLists.add(new MarqueeEntityInfo(0L, 9999L, 0L, "marquee.jar", true, 0L, "AdPac_v1.tar", true, 22L, 1366L, 768L));
	}

	private void printMarqueeEntity (MarqueeEntityInfo marqueeInfo, String title) {			
		System.out.println(title + "  ID(" + marqueeInfo.getMarqueeGroupID() + " : " + marqueeInfo.getMarqueeDeviceID()
				+ ")     SW(" + marqueeInfo.getMarqueeApplicationVer() + (marqueeInfo.isbIsNewApplication() ? " (New) : " : " (Same) : ") 
				+ marqueeInfo.getMarqueeApplication() 
				+ ")     AD(" + marqueeInfo.getMarqueePackageVer() + (marqueeInfo.isbIsNewPackage() ? " (New) : " : " (Same) : ") 
				+ marqueeInfo.getMarqueePackage() 
				+ ")     SIZE(" + marqueeInfo.getMarqueeDiplaySize() 
				+ " : " + marqueeInfo.getMarqueeResolustionWidth() + " x " + marqueeInfo.getMarqueeResolustionHeight() + ")");		
	}

//	private void printMarquee () {
//		int index = 0;
//		MarqueeEntityInfo marqueeEntity;
//		
//		for (index = 0; index < marqueeLinkedLists.size(); index++) {
//			marqueeEntity = marqueeLinkedLists.get(index);
//			this.printMarqueeEntity(marqueeEntity);
//		}
//		System.out.println(" ");
//	}
	
	public int getMarqueeCount() {
		return marqueeLinkedLists.size();
	}

	public int getMarqueeIndex(long marqueeDeviceID) {
		int index;
		
		for (index = 0; index < marqueeLinkedLists.size(); index++) {
			if (marqueeDeviceID == marqueeLinkedLists.get(index).getMarqueeDeviceID()) {
				return index;
			}
		}
		
		return -1;
	}
	
	public MarqueeEntityInfo getMarqueeEntity(int index) {
		MarqueeEntityInfo marqueeEntity;
		if (index < marqueeLinkedLists.size()) {
			marqueeEntity = marqueeLinkedLists.get(index);
//			printMarqueeEntity(marqueeEntity);
			return marqueeEntity;
		}
		
		return null;
	}

	public MarqueeEntityInfo getMarqueeEntity(long marqueeDeviceID) {
		MarqueeEntityInfo marqueeEntity;
		int index;
		
		for (index = 0; index < marqueeLinkedLists.size(); index++) {
			if (marqueeDeviceID == marqueeLinkedLists.get(index).getMarqueeDeviceID()) {
				marqueeEntity = marqueeLinkedLists.get(index);
//				printMarqueeEntity(marqueeEntity);
				return marqueeEntity;
			}
		}
		
		return null;
	}

	public MarqueeEntityInfo checkMaqueeUpdate(MarqueeEntityInfo marqueeInfo) {	
		if (this.refreshMarqueeEntity(marqueeInfo)) {
			return this.getMarqueeEntity(marqueeInfo.getMarqueeDeviceID());
		}
		else {
			printMarqueeEntity(marqueeInfo, "[MARQUEE : RECEIVED  ]");

			marqueeInfo.setbIsNewApplication(false);
			marqueeInfo.setMarqueeApplication(null);
			marqueeInfo.setbIsNewPackage(false);
			marqueeInfo.setMarqueePackage(null);

			printMarqueeEntity(marqueeInfo, "[MARQUEE : INVALID   ]");

			return marqueeInfo;
		}
	}
	
	public String checkNewApplication(MarqueeEntityInfo marqueeInfo) {
		MarqueeEntityInfo marqueeEntity = this.getMarqueeEntity(marqueeInfo.getMarqueeDeviceID());
		
		if (marqueeEntity.isbIsNewApplication()) {
			return marqueeEntity.getMarqueeApplication();
		}
	
		return null;
	}

	public String checkNewPackage(MarqueeEntityInfo marqueeInfo) {
		MarqueeEntityInfo marqueeEntity = this.getMarqueeEntity(marqueeInfo.getMarqueeDeviceID());
		
		if (marqueeEntity.isbIsNewPackage()) {
			return marqueeEntity.getMarqueePackage();
		}
	
		return null;
	}

	public boolean updateMarqueeEntity(MarqueeEntityInfo marqueeInfo) {
		MarqueeEntityInfo marqueeEntity = null;
		int 	index = 0;

//		printMarqueeEntity(marqueeInfo, "called updateMarqueeEntity(marqueeInfo)");

		index = this.getMarqueeIndex(marqueeInfo.getMarqueeDeviceID());	
		if (index < 0) {				
			// Should be called addNewMarqueeEntity().
			return false;
		}
				
		marqueeEntity = marqueeLinkedLists.get(index);
//		printMarqueeEntity(marqueeEntity, "saved info will be same (marqueeEntity)");
	
		if (marqueeEntity.getMarqueeApplicationVer() == marqueeInfo.getMarqueeApplicationVer()) {
			marqueeEntity.setbIsNewApplication(false);	// Same application
			
		} else if (marqueeEntity.getMarqueeApplicationVer() > marqueeInfo.getMarqueeApplicationVer()) {
			// New application.
			marqueeEntity.setMarqueeApplicationVer(marqueeInfo.getMarqueeApplicationVer());
			marqueeEntity.setMarqueeApplication(marqueeInfo.getMarqueeApplication());
			marqueeEntity.setbIsNewApplication(true);
			
		} else {	
			// Application version should not be smaller than saved one.
			return false;
		}

		if (marqueeEntity.getMarqueePackageVer() == marqueeInfo.getMarqueePackageVer()) {
			marqueeEntity.setbIsNewPackage(false);	// Same package
			
		} else if (marqueeEntity.getMarqueePackageVer() < marqueeInfo.getMarqueePackageVer()) {
			// New package.
			marqueeEntity.setMarqueePackageVer(marqueeInfo.getMarqueePackageVer());
			marqueeEntity.setMarqueePackage(marqueeInfo.getMarqueePackage());
			marqueeEntity.setbIsNewPackage(true);
			
		} else {	
			// Package version should not be smaller than saved one.
			return false;
		}

		marqueeEntity.setMarqueeDiplaySize(marqueeInfo.getMarqueeDiplaySize());
		marqueeEntity.setMarqueeResolustionWidth(marqueeInfo.getMarqueeResolustionWidth());
		marqueeEntity.setMarqueeResolustionHeight(marqueeInfo.getMarqueeResolustionHeight());
		
		marqueeLinkedLists.set(index, marqueeEntity); 
		
		printMarqueeEntity(marqueeEntity, "[SERVER : MODIFY     ]");
		
		this.saveData();
//		this.loadData();
//		
//		index = this.getMarqueeIndex(marqueeEntity.getMarqueeDeviceID());	
//		if (index < 0) {				
//			// Should be called addNewMarqueeEntity().
//			return false;
//		}
//		marqueeEntity = marqueeLinkedLists.get(index);
//		printMarqueeEntity(marqueeEntity, "confirm saving (marqueeEntity)");
	
		return true;
	}

	@Override
	public boolean refreshMarqueeEntity(MarqueeEntityInfo marqueeInfo) {
		MarqueeEntityInfo marqueeEntity = null;
		int 	index = 0;
		
		printMarqueeEntity(marqueeInfo, "[MARQUEE : RECEIVED  ]");
		
		index = this.getMarqueeIndex(marqueeInfo.getMarqueeDeviceID());
		if (index < 0) {	
			
			this.update (marqueeInfo);
			 /*  
			  * Add NEW MARQUEE CONNECTION NOTIFICATION (Not registered marquee) code.
			  * 
			  * */
			return false;
		}

		marqueeEntity = marqueeLinkedLists.get(index);
		printMarqueeEntity(marqueeEntity, "[SERVER : SAVING     ]");
			
		if (marqueeEntity.getMarqueeApplicationVer() == marqueeInfo.getMarqueeApplicationVer()) {
			// Same application
			marqueeEntity.setbIsNewApplication(false);	
			
		} else if (marqueeEntity.getMarqueeApplicationVer() > marqueeInfo.getMarqueeApplicationVer()) {
			// Mobile Marquee has an old application.
			marqueeEntity.setbIsNewApplication(true);
		
			/*
			 *  Add OLD APPLICATION NOTIFICATION (APPLICATION UPDATE) code.
			 *  
			 * */			
		} else {
			// Mobile Marquee has an invalid application.
			
			/*
			 *  Add INVALID APPLICATION NOTIFICATION code.
			 *  
			 * */			

			marqueeEntity.setbIsNewApplication(false);
//			return false;
		}

		if (marqueeEntity.getMarqueePackageVer() == marqueeInfo.getMarqueePackageVer()) {
			// Same package
			marqueeEntity.setbIsNewPackage(false);	
			
		} else if (marqueeEntity.getMarqueePackageVer() > marqueeInfo.getMarqueePackageVer()) {
			// Mobile Marquee has an old package.
			marqueeEntity.setbIsNewPackage(true);
		
			/*
			 *  Add OLD AD PACKAGE NOTIFICATION (APPLICATION UPDATE) code.
			 *  
			 * */			
		} else {
			// Mobile Marquee has an invalid application.
			
			/*
			 *  Add INVALID AD PACKAGE NOTIFICATION code.
			 *  
			 * */			

			marqueeEntity.setbIsNewPackage(false);
//			return false;
		}

		if (0 != marqueeInfo.getMarqueeDiplaySize())
			marqueeEntity.setMarqueeDiplaySize(marqueeInfo.getMarqueeDiplaySize());
	
		if (0 != marqueeInfo.getMarqueeResolustionWidth())
			marqueeEntity.setMarqueeResolustionWidth(marqueeInfo.getMarqueeResolustionWidth());
		
		if (0 != marqueeInfo.getMarqueeResolustionHeight())
			marqueeEntity.setMarqueeResolustionHeight(marqueeInfo.getMarqueeResolustionHeight());

		marqueeLinkedLists.set(index, marqueeEntity); 
			
		this.saveData();
		
		this.update (marqueeEntity);

		printMarqueeEntity(marqueeEntity, "[SERVER : UPDATE     ]");
		
		return true;
	}
	
	public boolean addNewMarqueeEntity(MarqueeEntityInfo marqueeInfo) {
		boolean result = false;
		int 	index = 0;
		
		
		index = this.getMarqueeIndex(marqueeInfo.getMarqueeDeviceID());
	
		if (index < 0) {	
			result = marqueeLinkedLists.add(marqueeInfo);
			
			printMarqueeEntity(marqueeInfo, "[SERVER              ]");
			this.saveData();
			
			return result;
		}			
		
		/*
		 * New Device Addition.
		 * Same device ID already exist.
		 * Device ID should be unique.
		 * 
		 * */		
		
		System.out.println("Duplicated device ID");
		return false;
	}
	
	public boolean removeMarqueeEntity(MarqueeEntityInfo marqueeInfo) {	
		boolean result;
		
		printMarqueeEntity(marqueeInfo, "[SERVER              ]");
		result = marqueeLinkedLists.remove(marqueeInfo);	
	
		this.saveData();
		
		return result;
	}

	public boolean removeMarqueeEntity(long marqueeDeviceID) {	
		boolean result;
		MarqueeEntityInfo marqueeInfo = this.getMarqueeEntity(marqueeDeviceID);
		
		printMarqueeEntity(marqueeInfo, "[SERVER              ]");
		result = marqueeLinkedLists.remove(marqueeInfo);
		
		this.saveData();
		
		return result;
	}

	public boolean removeAllMarqueeEntity() {	
		boolean result;
		
		result = marqueeLinkedLists.removeAll(marqueeLinkedLists);
		
		this.saveData();
		
		return result;
	}
}	

