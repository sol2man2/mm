package com.lge.config.marquee.model;

import marquee.adlib.utils.MarqueeEntityInfo;

public interface MarqueeManagerInterface {

	public int getMarqueeCount();
	public int getMarqueeIndex(long marqueeDeviceID);

	public MarqueeEntityInfo getMarqueeEntity(int index);
	public MarqueeEntityInfo getMarqueeEntity(long marqueeDeviceID);

	public MarqueeEntityInfo 	checkMaqueeUpdate(MarqueeEntityInfo marqueeInfo);
	public String 				checkNewApplication(MarqueeEntityInfo marqueeInfo);
	public String 				checkNewPackage(MarqueeEntityInfo marqueeInfo);
	
	public boolean updateMarqueeEntity(MarqueeEntityInfo marqueeInfo);	// Called by Marquee View
	public boolean refreshMarqueeEntity(MarqueeEntityInfo marqueeInfo);	// Called by Mobile Marquee through network.
	
	public boolean addNewMarqueeEntity(MarqueeEntityInfo marqueeInfo);
	public boolean removeMarqueeEntity(MarqueeEntityInfo marqueeInfo);
	public boolean removeMarqueeEntity(long marqueeDeviceID);
	
	public boolean removeAllMarqueeEntity();
}
