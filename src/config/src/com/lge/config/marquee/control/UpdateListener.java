package com.lge.config.marquee.control;

import marquee.adlib.utils.MarqueeEntityInfo;

public interface UpdateListener {
	public String getBasePath();
	public MarqueeEntityInfo checkMaqueeUpdate(MarqueeEntityInfo marqueeInfo);
	public boolean refreshMarqueeEntity(MarqueeEntityInfo marqueeInfo);
}
