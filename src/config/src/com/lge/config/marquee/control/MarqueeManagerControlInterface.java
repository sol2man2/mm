package com.lge.config.marquee.control;

import marquee.adlib.utils.MarqueeEntityInfo;

import com.lge.config.marquee.view.MarqueeView;

public interface MarqueeManagerControlInterface {

	public int getMarqueeCount();
	public int getMarqueeIndex(long marqueeDeviceID);
	
	public MarqueeEntityInfo getMarqueeEntity(int index);
	public MarqueeEntityInfo getMarqueeEntity(long marqueeDeviceID);
	
	public String 				checkNewApplication(MarqueeEntityInfo marqueeInfo);
	public String 				checkNewPackage(MarqueeEntityInfo marqueeInfo);	
	
	public boolean updateMarqueeEntity(MarqueeEntityInfo marqueeInfo);	// Called by Marquee View
	
	public boolean addNewMarqueeEntity(MarqueeEntityInfo marqueeInfo);
	public boolean removeMarqueeEntity(MarqueeEntityInfo marqueeInfo);
	public boolean removeMarqueeEntity(long marqueeDeviceID);
	
	public boolean removeAllMarqueeEntity();
	
	public MarqueeView getViewMarquee();
}
