package com.lge.config.marquee.control;


import java.util.Observable;
import java.util.Observer;

import marquee.adlib.utils.ConfigFileUtils;
import marquee.adlib.utils.MarqueeEntityInfo;

import com.lge.config.comm.UpdateServer;
import com.lge.config.marquee.model.MarqueeManager;
import com.lge.config.marquee.model.MarqueeManagerInterface;
import com.lge.config.marquee.view.MarqueeView;
//import marquee.adlib.utils.IObserver;

public class MarqueeManagerControl implements MarqueeManagerControlInterface, Observer, UpdateListener {
	MarqueeManagerInterface modelMarqueeInf = null;
	MarqueeView				viewMarquee		= null;
	// Add here Marquee View  
	// MarqueeView (MarqueeManagerInterface modelMarquee, MarqueeManagerControlInterface controlMarquee);

	UpdateServer updateServer = null;
	
	public MarqueeManagerControl() { 
		super();
		MarqueeManager 	modelMarquee = new MarqueeManager();

		this.modelMarqueeInf 	= (MarqueeManagerInterface)modelMarquee;
		this.viewMarquee 		= new MarqueeView(this, modelMarquee);	//this.modelMarquee, this);
		
		modelMarquee.addObserver(this);
		startUpdateServer();
	}
	public MarqueeManagerControl(MarqueeManager modelMarquee) { 
		super();
		
		this.modelMarqueeInf 	= (MarqueeManagerInterface)modelMarquee;
		this.viewMarquee 		= new MarqueeView(this, modelMarquee);	//this.modelMarquee, this);
		
		modelMarquee.addObserver(this);
	}	
	public boolean startUpdateServer() {
		updateServer = UpdateServer.startUpdateServer();
		updateServer.setUpdateListener(this);
		return true;
	}

	public boolean stopUpdateServer() {
//		TODO : stop the server when swing dies
		return false;
	}

	@Override
	public String getBasePath() {
		ConfigFileUtils myUtils = new ConfigFileUtils();
		return myUtils.getPathOfAdPkg();
	}

	@Override
	public MarqueeEntityInfo checkMaqueeUpdate(MarqueeEntityInfo marqueeInfo){
		return this.modelMarqueeInf.checkMaqueeUpdate(marqueeInfo);
	}
	
	// Called by Mobile Marquee through network.
	@Override
	public boolean refreshMarqueeEntity(MarqueeEntityInfo marqueeInfo) {
		return this.modelMarqueeInf.refreshMarqueeEntity(marqueeInfo);
	}

	public int getMarqueeCount() {
		return this.modelMarqueeInf.getMarqueeCount();
	}	
	public int getMarqueeIndex(long marqueeDeviceID){
		return this.modelMarqueeInf.getMarqueeIndex(marqueeDeviceID);
	}
	public MarqueeEntityInfo getMarqueeEntity(int index){
		return this.modelMarqueeInf.getMarqueeEntity(index);
	}
	public MarqueeEntityInfo getMarqueeEntity(long marqueeDeviceID){
		return this.modelMarqueeInf.getMarqueeEntity(marqueeDeviceID);
	}
	public String checkNewApplication(MarqueeEntityInfo marqueeInfo){
		return this.modelMarqueeInf.checkNewApplication(marqueeInfo);
	}
	public String checkNewPackage(MarqueeEntityInfo marqueeInfo){
		return this.modelMarqueeInf.checkNewPackage(marqueeInfo);
	}

	public boolean updateMarqueeEntity(MarqueeEntityInfo marqueeInfo){
		return this.modelMarqueeInf.updateMarqueeEntity(marqueeInfo);
	}
	
	public boolean addNewMarqueeEntity(MarqueeEntityInfo marqueeInfo){
		return this.modelMarqueeInf.addNewMarqueeEntity(marqueeInfo);
	}
	public boolean removeMarqueeEntity(MarqueeEntityInfo marqueeInfo){
		return this.modelMarqueeInf.removeMarqueeEntity(marqueeInfo);
	}
	public boolean removeMarqueeEntity(long marqueeDeviceID){
		return this.modelMarqueeInf.removeMarqueeEntity(marqueeDeviceID);
	}	
	public boolean removeAllMarqueeEntity(){
		return this.modelMarqueeInf.removeAllMarqueeEntity();
	}
	
	public MarqueeView getViewMarquee() {
		return this.viewMarquee;
	}
	
	@Override
	public void update(Observable arg0, Object arg1) {
		MarqueeEntityInfo marqueeInfo = (MarqueeEntityInfo)arg1;
		// TODO Auto-generated method stub
		
		System.out.println("_____________ OBSERVER (CONTROL) _____________");
		modelMarqueeInf.addNewMarqueeEntity(marqueeInfo);
	}
}
