package com.lge.config.marquee.view;

import javax.swing.table.DefaultTableModel;
import marquee.adlib.utils.MarqueeEntityInfo;

public class MarqueeTableModelPane extends DefaultTableModel{
	MarqueeTableModelPane(){
		super();
	}
	
	public void insertRow(int row, MarqueeEntityInfo mEntityInfo){
		Object[] data = {
				row,
				mEntityInfo.getMarqueeGroupID(), 
				mEntityInfo.getMarqueeDeviceID(), 
				mEntityInfo.getMarqueeApplication(),
				mEntityInfo.getMarqueeApplicationVer(), 
				mEntityInfo.isbIsNewApplication(),
				mEntityInfo.getMarqueePackage(),
				mEntityInfo.getMarqueePackageVer(),
				mEntityInfo.isbIsNewPackage(),
				mEntityInfo.getMarqueeDiplaySize(),
				mEntityInfo.getMarqueeResolustionWidth(),
				mEntityInfo.getMarqueeResolustionHeight()};
		super.insertRow(row, data);
	}
	
	public boolean isCellEditable(int row, int column){
//		if(column < 2){
//			return false;
//		}
//		else{
//			return true;
//		}
		if( column == 5 || column == 6 || column == 8){
			return false;
		}
		return true;
	}

	public void addRow(MarqueeEntityInfo mEntityInfo){
		Object[] data = {
				getRowCount(),
				mEntityInfo.getMarqueeGroupID(), 
				mEntityInfo.getMarqueeDeviceID(), 
				mEntityInfo.getMarqueeApplication(),
				mEntityInfo.getMarqueeApplicationVer(), 
				mEntityInfo.isbIsNewApplication(),
				mEntityInfo.getMarqueePackage(),
				mEntityInfo.getMarqueePackageVer(),
				mEntityInfo.isbIsNewPackage(),
				mEntityInfo.getMarqueeDiplaySize(),
				mEntityInfo.getMarqueeResolustionWidth(),
				mEntityInfo.getMarqueeResolustionHeight()};
		super.addRow(data);
	}
	
	public void removeRow(int row){
		super.removeRow(row);
	}
	
	MarqueeTableModelPane(Object[] columnNames, int rowCount) {
        super(columnNames, rowCount);
	}
}
