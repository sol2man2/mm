package com.lge.config.marquee.view;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Observable;

import javax.swing.JFileChooser;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import javax.swing.SwingConstants;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;
import javax.swing.filechooser.FileFilter;
import javax.swing.filechooser.FileNameExtensionFilter;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.TableColumnModel;

import marquee.adlib.utils.MarqueeEntityInfo;

import com.lge.config.marquee.control.MarqueeManagerControl;
import com.lge.config.marquee.control.MarqueeManagerControlInterface;
import com.lge.config.marquee.model.MarqueeManager;
import com.lge.config.marquee.model.MarqueeManagerInterface;

public class MarqueeView extends JPanel{
	private MarqueeManagerControlInterface 	controlMarqueeInf 	= null;
	private MarqueeManagerInterface			modelMarqueeInf 	= null;
	
	private String[] titles = new String[] {"Index", "Group ID", "Device ID", "App Name", "App Ver", "Need App Update", "Package Name", "Package Ver", "Need Package Update", "Display Size", "Width", "Height"};
	private MarqueeTableModelPane mTableModelPane = new MarqueeTableModelPane(titles, 0);
	private JTable mTable = new JTable(mTableModelPane);

	private MarqueeEntityInfo mEntityInfo;
	private long marqueeDeviceID = 0;
	private File choosePath = new File("C:\\test\\ad_pkg\\");

	private final String fileError = "똑바로해";

/*	public void updateMarqueeData(){
		int selectedCount = mTable.getSelectedRowCount();
		int i = 0;
		while(selectedCount > i ){
			int index[] = mTable.getSelectedRows();
			System.out.println("Marquee update for index : " + index[i++]);
		}
	}*/
	
	public void newMarqueeList(){
		// Get the number of row
		int countMarquee = controlMarqueeInf.getMarqueeCount();
		int i = 0;
		ArrayList<Integer> nums = new ArrayList<Integer>();
		while(countMarquee > i){
			nums.add(new Integer((int)controlMarqueeInf.getMarqueeEntity(i).getMarqueeDeviceID()));
			i++;
		}
		Collections.sort(nums);
		
		mEntityInfo = new MarqueeEntityInfo(0, nums.get(i-1)+1, 11, "marqueeApplication_v1.0.java", true, 1, "pkg1.tar", true, 15, 960, 540);
		controlMarqueeInf.addNewMarqueeEntity(mEntityInfo);
		
		mTableModelPane.addRow(mEntityInfo);
		
		ListSelectionModel selectionModel = mTable.getSelectionModel();
		selectionModel.setSelectionInterval(countMarquee, countMarquee);
				
		saveMarqueeList();
	}
	
	public void saveMarqueeList(){
		int selectedCount = mTable.getSelectedRowCount();
		int index[] = mTable.getSelectedRows();
		int i = 0;
		String str = null;
		
		while(selectedCount > i ){
			str = new String(mTable.getModel().getValueAt(index[i], 1).toString());
			mEntityInfo.setMarqueeGroupID(Long.parseLong(str));
			str = new String(mTable.getModel().getValueAt(index[i], 2).toString());
			mEntityInfo.setMarqueeDeviceID(Long.parseLong(str));
			str = new String(mTable.getModel().getValueAt(index[i], 3).toString());
			mEntityInfo.setMarqueeApplication(str);
			str = new String(mTable.getModel().getValueAt(index[i], 4).toString());
			mEntityInfo.setMarqueeApplicationVer(Long.parseLong(str));
			str = new String(mTable.getModel().getValueAt(index[i], 5).toString());
			mEntityInfo.setbIsNewApplication(Boolean.parseBoolean(str));
			str = new String(mTable.getModel().getValueAt(index[i], 6).toString());
			mEntityInfo.setMarqueePackage(str);
			str = new String(mTable.getModel().getValueAt(index[i], 7).toString());
			mEntityInfo.setMarqueePackageVer(Long.parseLong(str));
			str = new String(mTable.getModel().getValueAt(index[i], 8).toString());
			mEntityInfo.setbIsNewPackage(Boolean.parseBoolean(str));
			str = new String(mTable.getModel().getValueAt(index[i], 9).toString());
			mEntityInfo.setMarqueeDiplaySize(Long.parseLong(str));
			str = new String(mTable.getModel().getValueAt(index[i], 10).toString());
			mEntityInfo.setMarqueeResolustionWidth(Long.parseLong(str));
			str = new String(mTable.getModel().getValueAt(index[i], 11).toString());
			mEntityInfo.setMarqueeResolustionHeight(Long.parseLong(str));
			
			controlMarqueeInf.updateMarqueeEntity(mEntityInfo);
			
			i++;
		}
	}

	public void deleteMarqueeList(){
		int selectedCount = mTable.getSelectedRowCount();
		int index[] = mTable.getSelectedRows();
		System.out.println("selectedCount : " + selectedCount);
		int i = 0;
		String str = null;
		while(selectedCount > i ){
			System.out.println("Delete Marquee for index : " + index[i]);
			
			str = new String(mTable.getModel().getValueAt(index[selectedCount-i-1], 2).toString());
			marqueeDeviceID = Long.parseLong(str);
			
			controlMarqueeInf.removeMarqueeEntity(marqueeDeviceID);
			
			mTableModelPane.removeRow(index[selectedCount-i-1]);
			i++;
		}
	}
	
	public MarqueeView(MarqueeManagerControl controlMarquee, MarqueeManager modelMarquee){
		this.controlMarqueeInf 	= (MarqueeManagerControlInterface)controlMarquee;
		this.modelMarqueeInf	= (MarqueeManagerInterface)modelMarquee;
		
		//JFrame f = new JFrame("MMView");
		
		int listCount = this.controlMarqueeInf.getMarqueeCount();
//		System.out.println("listCount = " + listCount);
		
		for(int count=0 ; count<listCount ; count++){
			mEntityInfo = this.controlMarqueeInf.getMarqueeEntity(count);
			mTableModelPane.insertRow(count, this.controlMarqueeInf.getMarqueeEntity(count));
		}
		
		setLayout(new BorderLayout());
		setBackground(Color.LIGHT_GRAY);
		
		JScrollPane jsp = new JScrollPane(mTable); 
		
		// Set alignment in the middle
		DefaultTableCellRenderer tCellRenderer = new DefaultTableCellRenderer();
		tCellRenderer.setHorizontalAlignment(SwingConstants.CENTER);

		TableColumnModel tColumnModel = mTable.getColumnModel();
		
		for (int i = 0; i < tColumnModel.getColumnCount(); i++) {
			tColumnModel.getColumn(i).setCellRenderer(tCellRenderer);
		}
		
		//jsp.setSize(1300, 350);
		
		add(jsp);
		//setSize(1300, 500);
		setVisible(true);	
		//f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		AddListenerFunctions();
		AddTableModelListener();
	}
	
	public void AddListenerFunctions(){
		mTable.addMouseListener(new MouseAdapter() {
        	@Override
        	public void mouseClicked(MouseEvent e) {
        		int clickCount = e.getClickCount();

        		// Table에 선택되면 선택된 Row에 해당하 Contents List 를 보여줌
        		if(clickCount == 2){
        			String selectedFile = changePackageFile();
        			
        			if(selectedFile != fileError){
	        			selectedFile = getExtension(selectedFile);
	        			mTableModelPane.setValueAt(selectedFile, mTable.getSelectedRow(), 6);
	        			System.out.println(selectedFile);
        			}
        			
        		}
            }
		});
	}
	
	public void AddTableModelListener(){
		mTable.getModel().addTableModelListener(new TableModelListener() {
			
			@Override
			public void tableChanged(TableModelEvent e) {
				// TODO Auto-generated method stub
				if(e.getType() == TableModelEvent.UPDATE){
					int selectedColumn = e.getColumn();
					int selectedRow = mTable.getSelectedRow();
					System.out.println("Selected Row : "+ selectedRow + ", Column : " + selectedColumn);
					
					mEntityInfo = controlMarqueeInf.getMarqueeEntity(selectedRow);
					String str = new String(mTable.getModel().getValueAt(selectedRow, selectedColumn).toString());
					
					switch(selectedColumn){
					case 1:
						mEntityInfo.setMarqueeGroupID(Long.parseLong(str));
						break;					
					case 2:	
						mEntityInfo.setMarqueeDeviceID(Long.parseLong(str));
						break;
					case 3:
						mEntityInfo.setMarqueeApplication(str);
						break;
					case 4:	
						mEntityInfo.setMarqueeApplicationVer(Long.parseLong(str));
						break;
					case 5:
						mEntityInfo.setbIsNewApplication(Boolean.parseBoolean(str));
						break;
					case 6:
						mEntityInfo.setMarqueePackage(str);
						break;
					case 7:
						mEntityInfo.setMarqueePackageVer(Long.parseLong(str));
						break;
					case 8:
						mEntityInfo.setbIsNewPackage(Boolean.parseBoolean(str));
						break;
					case 9:
						mEntityInfo.setMarqueeDiplaySize(Long.parseLong(str));
						break;
					case 10:
						mEntityInfo.setMarqueeResolustionWidth(Long.parseLong(str));
						break;
					case 11:
						mEntityInfo.setMarqueeResolustionHeight(Long.parseLong(str));
						break;
					}				
					controlMarqueeInf.updateMarqueeEntity(mEntityInfo);
				}
			}
			
			});
	}
		
	public static String getExtension(String filepath) {
	      int dot = filepath.lastIndexOf('\\');
	      if (dot > -1) {
	          filepath = filepath.substring(dot+1);
	          return filepath;
	      } 
	      else {
	          return null;
	      }
	  }
	
	private String changePackageFile(){
		if(mTable.getSelectedColumn() != 6){
			return fileError;
		}
		JFileChooser fc = new JFileChooser();
//		FileFilter fPkg = new FileNameExtensionFilter("Package(*.pkg)","pkg");
//		fc.addChoosableFileFilter(fPkg);
		FileFilter fXml = new FileNameExtensionFilter("manifest(*.tar)","tar");
		fc.setFileFilter(fXml);
		fc.setCurrentDirectory(choosePath);
		fc.setAcceptAllFileFilterUsed(false);
		int result = fc.showOpenDialog(this);
		
		// if we selected an image, load the image
		if(result == JFileChooser.APPROVE_OPTION) {
		    String strFile = fc.getSelectedFile().getPath();
		    choosePath = fc.getSelectedFile();
		    return strFile;
		}
		else{
			return fileError;
		}
	}

	public void update(Observable arg0, Object arg1) {
		MarqueeEntityInfo marqueeInfo = (MarqueeEntityInfo)arg1;
		// TODO Auto-generated method stub
		
		System.out.println("_____________ OBSERVER (VIEW) _____________");
		modelMarqueeInf.addNewMarqueeEntity(marqueeInfo);
	}
}
