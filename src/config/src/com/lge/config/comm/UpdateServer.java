package com.lge.config.comm;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;


import com.lge.adlib.comm.SystemInfo;
import com.lge.config.marquee.control.UpdateListener;

public class UpdateServer implements Runnable, SystemInfo {

	protected ExecutorService server = null;
	protected ExecutorService updateService = null;

	private UpdateListener updateListener = null;

	public void setUpdateListener(UpdateListener updateListener) {
		this.updateListener = updateListener;
	}

	int port;
	private ServerSocket serverSocket;

	public UpdateServer() {
		port = SystemInfo.SERVER_PORT;
		server = Executors.newFixedThreadPool(1);
		updateService = Executors.newCachedThreadPool();
	}

	public static UpdateServer updateServer = null;
	
	public static UpdateServer startUpdateServer() {
		if( null == updateServer ) {
			updateServer = new UpdateServer();
		}
		updateServer.execute();
		return updateServer;
	}

	public void execute() {
		server.execute(this);
	}
	
	public static boolean stopUpdateServer() throws InterruptedException {
		if( null == updateServer ) {
			return false;
		}
		else {
			updateServer.shutdownNow();
			updateServer.awaitTermination(2000, TimeUnit.MILLISECONDS);
			return true;
		}
	}

	private void closeServerSocket() {
		try {
			serverSocket.close();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			serverSocket = null;
		}
	}

	public void shutdown() {
		closeServerSocket();
		server.shutdown();
		updateService.shutdown();
	}

	public void shutdownNow() {
		closeServerSocket();
		server.shutdownNow();
		updateService.shutdownNow();
	}

	public void awaitTermination(int i, TimeUnit milliseconds) throws InterruptedException {
		server.awaitTermination(i, milliseconds);
		updateService.awaitTermination(i, milliseconds);
	}

	public void run() {
		try {
			serverSocket = new ServerSocket(port);
			Socket socket = null;
			while (true) {
				socket = serverSocket.accept();
				updateService.submit(new RequestHandler(socket, updateListener));
			}
		} catch (SocketException e) {
			System.out.println("ServerSocket is closed! " + e.toString());
		} catch (IOException e) {
			System.out.println(e.toString());
		}
	}
}
