package com.lge.config.comm;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.net.Socket;

import marquee.adlib.utils.MarqueeEntityInfo;

import com.lge.adlib.comm.Acknowledge;
import com.lge.adlib.comm.Acknowledge.AcknowledgeCode;
import com.lge.adlib.comm.AdPackageRequest;
import com.lge.adlib.comm.AdPackageResponse;
import com.lge.adlib.comm.MarqueeInfoRequest;
import com.lge.adlib.comm.MarqueeInfoResponse;
import com.lge.adlib.comm.Request;
import com.lge.adlib.comm.Response;
import com.lge.adlib.comm.SoftwareRequest;
import com.lge.adlib.comm.SoftwareResponse;
import com.lge.adlib.comm.SystemInfo;
import com.lge.config.marquee.control.UpdateListener;

public class RequestHandler implements Runnable, SystemInfo{

	private final int BUFFER_SIZE = 1024 * 8;
	protected String softwareFile = "scala-library.jar";
	protected String adPackageFile = "scala-compiler.jar";

	private UpdateListener updateListener = null;
	
	Socket socket = null;

	InputStream isFromSocket = null;
	BufferedInputStream bisFromSocket = null;
	ObjectInputStream oisFromSocket = null;

	OutputStream osToSocket = null;
	BufferedOutputStream bosToSocket = null;
	ObjectOutputStream oosToSocket = null;
	
	FileInputStream fisFromFile = null;
	BufferedInputStream bisFromFile = null;

	public RequestHandler(Socket socket, UpdateListener updateListener) {
		this.socket = socket;
		this.updateListener = updateListener;  
	}

	@Override
	public void run() {

		try {
			handleMarqueeInfoRequest();
		} catch (Exception e) {
		} finally {
			try {
				closeInputStreamFromSocket();
				closeOutputStreamToSocket();

				socket.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	public Response handleRequst(Object obj) {
		Request req = (Request) obj;
		Response res = null;
		if( obj instanceof SoftwareRequest ) {
			res =  new SoftwareResponse();
			res.setVersion(req.getVersion());

			// TODO check wheth9er new sw exists or not
			res.setResponseCode(Response.ResponseCode.EXISTENCE_NEW_UPDATE);

			// TODO change new Software Image path
			res.setFileName(softwareFile);
			File f = new File(softwareFile);
			res.setFileSize(f.length());
		} else if( obj instanceof AdPackageRequest ) {
			res =  new AdPackageResponse();
			res.setVersion(req.getVersion());

			// TODO check whether new AdPackage exists or not
			res.setResponseCode(Response.ResponseCode.EXISTENCE_NEW_UPDATE);

			// TODO change new AdPackage path
			res.setFileName(adPackageFile);
			File f = new File(adPackageFile);
			res.setFileSize(f.length());
		} else if( obj instanceof MarqueeInfoRequest) {

			MarqueeEntityInfo receivedMarqueeEntityInfo = ((MarqueeInfoRequest)obj).getMarqueeEntityInfo();
			MarqueeEntityInfo marqueeEntityInfoToSend = updateListener.checkMaqueeUpdate(receivedMarqueeEntityInfo);

			res = new MarqueeInfoResponse(marqueeEntityInfoToSend);
		}
		return res;
	}

	private void handleMarqueeInfoRequest() throws IOException, ClassNotFoundException, FileNotFoundException {
		openInputStreamFromSocket();

		Request req = receiveRequest();
		Response res = handleRequst(req);

		openOutputStreamToSocket();
		sendResponse(res);

		Acknowledge ack = receiveAcknowledge();

		if( ack.getResponseCode() == AcknowledgeCode.SUCCESS )
			updateListener.refreshMarqueeEntity(((MarqueeInfoResponse)res).getMarqueeEntityInfo());
	}

	private void openInputStreamFromSocket() throws IOException {
		isFromSocket = socket.getInputStream();
		bisFromSocket = new BufferedInputStream(isFromSocket, BUFFER_SIZE);
		oisFromSocket = new ObjectInputStream(bisFromSocket);
	}

	private Request receiveRequest() throws IOException, ClassNotFoundException {
		return (Request)oisFromSocket.readObject();
	}

	private Acknowledge receiveAcknowledge() throws IOException, ClassNotFoundException {
		return (Acknowledge)oisFromSocket.readObject();
	}

	private void openOutputStreamToSocket() throws IOException {
		osToSocket = socket.getOutputStream();
		bosToSocket = new BufferedOutputStream(osToSocket, BUFFER_SIZE);
		oosToSocket = new ObjectOutputStream(bosToSocket);
	}

	private void sendResponse(Response res) throws IOException {
		MarqueeInfoResponse marqueeInfoResponse = (MarqueeInfoResponse) res;
		MarqueeEntityInfo marqueeEntityInfo = marqueeInfoResponse.getMarqueeEntityInfo();
		
		String basePath = updateListener.getBasePath();
		File fBasePath = new File(basePath);

		if(marqueeEntityInfo.isbIsNewApplication()) {
			File file = new File(fBasePath, marqueeEntityInfo.getMarqueeApplication());
			marqueeEntityInfo.setSizeMarqueeApplication(file.length());
		}

		if(marqueeEntityInfo.isbIsNewPackage()) {
			File file = new File(fBasePath, marqueeEntityInfo.getMarqueePackage());
			marqueeEntityInfo.setSizeMarqueePackage(file.length());
		}

		oosToSocket.writeObject(res);
		oosToSocket.flush();

		if( marqueeEntityInfo.isbIsNewApplication() ) {
			sendFileThruSocket(basePath + File.separator + marqueeEntityInfo.getMarqueeApplication(), marqueeEntityInfo.getSizeMarqueeApplication());
		}

		if( marqueeEntityInfo.isbIsNewPackage() ) {
			String str = basePath + File.separator + marqueeEntityInfo.getMarqueePackage();
			sendFileThruSocket(str, marqueeEntityInfo.getSizeMarqueePackage());
		}
	}

	private void openInputStreamFromFile(String filePath) throws FileNotFoundException {
		fisFromFile = new FileInputStream(new File(filePath));
		bisFromFile = new BufferedInputStream(fisFromFile, 1024 * 8);
	}

	private void closeInputStreamFromFile() throws IOException {
		bisFromFile.close();
		fisFromFile.close();
	}

	private void sendFileThruSocket(String filePath, long fileSize) throws IOException {
		final int buffLength = BUFFER_SIZE;
		byte[] buff = new byte[buffLength];

		int sentSize = 0;
		int sentTotalSize = 0;

		openInputStreamFromFile(filePath);
		
		long sizeToSend = fileSize;

		for(;;) {
			if( sizeToSend >= buffLength ){
				sentSize = bisFromFile.read(buff, 0, buffLength);
				sizeToSend -= sentSize;
				bosToSocket.write(buff, 0, sentSize);
				bosToSocket.flush();
			} else {
				sentSize = bisFromFile.read(buff, 0, (int)sizeToSend);
				sizeToSend -= sentSize;
				bosToSocket.write(buff, 0, sentSize);
				bosToSocket.flush();
				if( sizeToSend == 0 ) break;
			}
		}
		closeInputStreamFromFile();
	}

	private void closeOutputStreamToSocket() throws IOException {
		oosToSocket.close();
		bosToSocket.close();
		osToSocket.close();
	}

	private void closeInputStreamFromSocket() throws IOException {
		oisFromSocket.close();
		bisFromSocket.close();
		isFromSocket.close();
	}
}
