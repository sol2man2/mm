package com.lge.config.comm;



public class ConfigCommMain {
	public static void main(String[] arg) {

//		IUpdateListener controlUpdate = new UpdateControl();
		UpdateServer updateServer = UpdateServer.startUpdateServer();
//		updateServer.setUpdateListener(controlUpdate);
		
		try {
			while(true) {
				Thread.sleep(20000);
			}
		} catch (InterruptedException e) {
			e.printStackTrace();
		}

		boolean result = false;
		try {
			result = UpdateServer.stopUpdateServer();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		System.out.println("UpdateServer down: " + result);
	}
}
