package marquee.config.launcher;

import marquee.adlib.ads.AdModelInterface;
import marquee.adlib.ads.impl.AdPackage;
import marquee.config.control.ConfigControllerInterface;
import marquee.config.control.impl.AdConfigControl;

import com.lge.config.marquee.control.MarqueeManagerControl;
import com.lge.config.marquee.control.MarqueeManagerControlInterface;
import com.lge.config.marquee.model.MarqueeManager;
import com.lge.config.marquee.model.MarqueeManagerInterface;


public class LauncherConfigTool {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		//1. Model을 생성한다.
		AdModelInterface 	modelConfig 	= (AdModelInterface) new AdPackage();
		
		//2. Controller를 생성하면서, Model을 넘겨준다.
		ConfigControllerInterface 	controlConfig 	= new AdConfigControl(modelConfig);
	}
}
