package marquee.config.control.impl;

import java.awt.Rectangle;
import java.util.ArrayList;

import javax.swing.JOptionPane;

import com.lge.config.marquee.control.MarqueeManagerControl;

// Model Component
import marquee.adlib.ads.AdModelInterface;
import marquee.adlib.ads.AdPackageDataObject;


// Control Component
import marquee.config.control.ConfigControllerInterface;
// View Component

import marquee.config.view.ConfigMainFrame;

// Util
import marquee.adlib.utils.IObserver;

public class AdConfigControl extends ConfigControllerInterface implements IObserver {

	AdModelInterface modelConfig;
	ConfigMainFrame viewConfigTool;
	
	public AdConfigControl(AdModelInterface modelConfig) {
		super();
		this.modelConfig = modelConfig;
		
		modelConfig.registerObserver(this);
		
		// Create CONTROL for marquee manager.
		MarqueeManagerControl controlMarquee = new MarqueeManagerControl();		

		viewConfigTool = new ConfigMainFrame(this, this.modelConfig, controlMarquee.getViewMarquee());
	}

	@Override
	public void newAdPackage() {
		modelConfig.newAdPackage();
		viewConfigTool.initial();
		// View에 초기화를 알린다.
		// 작성 중일 경우 newAdPackage를 요청할 경우는 Save로직으로 가야하는가?
		//JOptionPane.showMessageDialog(null, "새로운 Package를 만듭니다.");
	}

	@Override
	public void loadAdPackage(String strFileName) {
		boolean bRet = modelConfig.loadAdPackage(strFileName);
		if(bRet == false)
		{
			//View에 Load 실패에 대해서 알린다.
			JOptionPane.showMessageDialog(null, "Loading Fail!!!!!");
		}
		else
			viewConfigTool.initial();
		//JOptionPane.showMessageDialog(null, "선택한 Package를 로딩합니다.");
	}

	@Override
	public void saveAdPackage(String strFileName) {
		boolean bRet = modelConfig.saveAdPackage(strFileName);
		if(bRet == false){
			//View에 Save 실패에 대해서 알린다.
			JOptionPane.showMessageDialog(null, "Saving Fail!!!!!");
		}
		//JOptionPane.showMessageDialog(null, "현재 작성된 Package를 저장합니다.");
	}

	@Override
	public void displayPreview() {
		JOptionPane.showMessageDialog(null, "Preview를 실행합니다.");
	}


	@Override
	public void deletePane(int id) {
		// 삭제가 성공하면..
		if(modelConfig.deletePane(id)){
			
		}
		else{
			
		}
	}
	
	public void addPane(AdPackageDataObject configData){
		modelConfig.addPane(configData);
	}
	
	public void modifyPane(int id, AdPackageDataObject DO){
		modelConfig.modifyPane(id, DO);
	}
	
	@Override
	// Model Component에 변경이 발생하였을때 호출되는 Call Back 함수라고 보면됨.
	public void update() {
		// TODO Auto-generated method stub
	}
	
	@Override
	public int getSelectedPaneID(){
		return modelConfig.getSelectedPaneID();
	}
	
	@Override
	public void setSelectedPaneID(int selectedId){
		modelConfig.setSelectedPaneID(selectedId);
	}
		
	public boolean checkOverlapByID(int ID, Rectangle rect){
		ArrayList<AdPackageDataObject> listCon =  modelConfig.getData();
		
		for(AdPackageDataObject pane : listCon){
			 if(pane.getID() != ID){		// Selected component는 제외
				 if(pane.getRectangle().intersects(rect)){
					 return false;
				 }
			 }
		 }
		 return true;
	}
	
	public Rectangle checkRange(Rectangle rect){
		
		// width, height check
		if(rect.width < 20) rect.width = 20;
		if(rect.height < 20) rect.height = 20;
		if(rect.width > ConfigMainFrame.widthCanvas)
			rect.width = ConfigMainFrame.widthCanvas;
		if(rect.height > ConfigMainFrame.heightCanvas)
			rect.height = ConfigMainFrame.heightCanvas;
		
		// x, y check
		if(rect.x < 0) rect.x = 0;
		if(rect.y < 0) rect.y = 0;
		if(rect.x > ConfigMainFrame.widthCanvas)
			rect.x = rect.x - (ConfigMainFrame.widthCanvas - rect.width);
		if(rect.y > ConfigMainFrame.heightCanvas )
			rect.y = rect.y - (ConfigMainFrame.heightCanvas - rect.height);
		
		if(rect.x + rect.width > ConfigMainFrame.widthCanvas)
			rect.width = ConfigMainFrame.widthCanvas - rect.x;
		if(rect.y + rect.height > ConfigMainFrame.heightCanvas)
			rect.height = ConfigMainFrame.heightCanvas - rect.y;
		return rect;
	}
}
