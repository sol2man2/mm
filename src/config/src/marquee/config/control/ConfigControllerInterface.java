package marquee.config.control;

import java.awt.Rectangle;

import marquee.adlib.ads.AdPackageDataObject;

public abstract class ConfigControllerInterface {
	
	// View Component에서 들어오는 이벤트 핸들을 가지고 대응한다.
	public abstract void newAdPackage();
	public abstract void loadAdPackage(String strFileName);
	public abstract void saveAdPackage(String strFileName);
	
	public abstract void displayPreview();
	
	public abstract void addPane(AdPackageDataObject configData);
	public abstract void modifyPane(int id, AdPackageDataObject configData);
	public abstract void deletePane(int id);
	
	public abstract Rectangle checkRange(Rectangle rect);
	public abstract boolean checkOverlapByID(int ID, Rectangle rect);
	
	public abstract int getSelectedPaneID();
	public abstract void setSelectedPaneID(int selectedId);
}
