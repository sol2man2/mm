package marquee.config.view;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Component;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.Point;
import java.awt.Rectangle;
import java.util.ArrayList;

import javax.swing.ImageIcon;

import marquee.adlib.ads.AdPackageDataObject;

public class DrawableVideoAd extends IDrawable {
	
	final String strVideoIcon = System.getProperty("user.dir") + "\\resource\\video.jpg";
	
	ImageIcon imageIcon = new ImageIcon(strVideoIcon);
	ArrayList<String> listCont = null;
	
	final static float dash1[]={1.f};
	final static BasicStroke dashed = new BasicStroke(3.0f,BasicStroke.CAP_ROUND,BasicStroke.JOIN_ROUND, 10.0f,dash1, 0.0f);
	
	// Constructor
	public DrawableVideoAd(){
		AdDO = AdPackageDataObject.createDO();
	}
	
	// Setter - DataObject
	public void setContentsList(ArrayList<String> listCon){
		listCont = listCon;
	}
	
	@Override
	public void draw(Component c, Graphics g) {
		Graphics2D g2 = (Graphics2D)g;
		Color color = g2.getColor();
		g2.setColor(Color.WHITE);
		g2.clearRect(nowRect.x, nowRect.y, nowRect.width, nowRect.height);
		
		nowRect.x = newRect.x;
		nowRect.y = newRect.y;
		nowRect.width = newRect.width;
		nowRect.height = newRect.height;
		
		if(listCont.size() == 0){
			g2.setColor(Color.GRAY);    // Set the color.
			g2.drawString("No Picture", nowRect.x+5, nowRect.y+5);
			g2.drawRect( nowRect.x, nowRect.y, nowRect.width, nowRect.height);   
		}
		else{
			Image img = imageIcon.getImage();
			g2.drawImage(img, nowRect.x, nowRect.y, nowRect.width, nowRect.height, c);
		}
		
		if(selected){
			nowRect.grow(-2, -2);
			g2.setColor(Color.GREEN);
			g2.setStroke(dashed);
			g2.drawRect(nowRect.x, nowRect.y, nowRect.width, nowRect.height);
		}
		g2.setColor(color);
	}
}
