package marquee.config.view;

import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

import marquee.adlib.ads.AdPackageDataObject;
import marquee.adlib.ads.AdPaneType;
import marquee.adlib.ads.ScrollType;
import marquee.config.control.ConfigControllerInterface;

public class ModifyPaneDialog extends JFrame implements ActionListener{
	private JComboBox comboPaneType = null;
	private JPanel panLabel = new JPanel();
	private JPanel panData = new JPanel();
	private JPanel panControl = new JPanel();
	private ConfigControllerInterface controlConfig = null;
	private JButton btnModify = new JButton("Modify");
	private JButton btnClose = new JButton("Close");
	
	private JTextField editPosX = new JTextField();
	private JTextField editPosY = new JTextField();
	private JTextField editWidth = new JTextField();
	private JTextField editHeight = new JTextField();
	private JTextField editzOrder = new JTextField();
	private JTextField editInterval = new JTextField();
	
	private ArrayList<String> listcontent;
	
	private int ID;
	
	private File choosePath = null;
	
	public void setConfigControllIf(ConfigControllerInterface cfg){
		controlConfig = cfg;
	}
	
	public void setAdPane(int id, AdPackageDataObject pane){
		listcontent = pane.getContentsList();
		editPosX.setText(Integer.toString((int)pane.getRectangle().getX()));
		editPosY.setText(Integer.toString((int)pane.getRectangle().getY()));
		editWidth.setText(Integer.toString((int)pane.getRectangle().getWidth()));
		editHeight.setText(Integer.toString((int)pane.getRectangle().getHeight()));
		editzOrder.setText(Integer.toString((int)pane.getzOrder()));
		editInterval.setText(Integer.toString((int)pane.getInterval()));
		comboPaneType.setSelectedItem(pane.getAdPaneType().toString());
		if(pane.getAdPaneType().toString() == "SCROLLING"){
			if(pane.getScrollType() == ScrollType.HORIZONTAL){
				comboPaneType.setSelectedItem("H_Scroll");
			}
			else{
				comboPaneType.setSelectedItem("V_Scroll");
			}
		}
		
		ID = id;
		System.out.println("ID : " + ID);
	}
	
	public ModifyPaneDialog() {
		CreateGUI();
	}
	
	public void actionPerformed(ActionEvent ae) 
	  {
		  if(ae.getSource() == btnModify) {
			  AdPackageDataObject pane = null;
			  
			  pane = AdPackageDataObject.createDO();
			  
			  pane.setScrollType(ScrollType.HORIZONTAL);
			  if("STATIC" == comboPaneType.getSelectedItem()){
				  pane.setAdPaneType(AdPaneType.STATIC);
			  }
			  else if("PAGING" == comboPaneType.getSelectedItem()){
				  pane.setAdPaneType(AdPaneType.PAGING);
			  }
			  else if("V_Scroll" == comboPaneType.getSelectedItem()){
				  pane.setAdPaneType(AdPaneType.SCROLLING);
				  pane.setScrollType(ScrollType.VERTICAL);
			  }
			  else if("H_Scroll" == comboPaneType.getSelectedItem()){
				  pane.setScrollType(ScrollType.HORIZONTAL);
				  pane.setAdPaneType(AdPaneType.SCROLLING);
			  }
			  else if("VIDEO" == comboPaneType.getSelectedItem()){
				  pane.setAdPaneType(AdPaneType.VIDEO);
			  }
			  
			  pane.setRectangle(new Rectangle(Integer.parseInt(editPosX.getText()), Integer.parseInt(editPosY.getText()), Integer.parseInt(editWidth.getText()), Integer.parseInt(editHeight.getText())) );
			  pane.setzOrder(Integer.parseInt(editzOrder.getText()));
			  pane.setInterval(Integer.parseInt(editInterval.getText()));
			  pane.setContentsList(listcontent);
			  pane.setID(ID);
			  
			  Rectangle rect = pane.getRectangle();
			  rect = controlConfig.checkRange(rect);
			  pane.setRectangle(rect);
			  if(controlConfig.checkOverlapByID(ID, rect) == false){
				  if (JOptionPane.showConfirmDialog(new JFrame(),
		    			  "Do you want to confirm this position ?", "Warning - Overlap",
		    			  JOptionPane.YES_NO_OPTION) == JOptionPane.YES_OPTION)
					  controlConfig.modifyPane(ID, pane);
			  }
			  else{
				  controlConfig.modifyPane(ID, pane);
			  }
			  
			  //controlConfig.modifyPane(ID, pane);
			  System.out.println("잘 됐니");
		  }
		  else if(ae.getSource() == btnClose) {
			  setVisible(false);
		  }
	  }//end actionPerformed()
	
	private void SetInitValues(){
		editPosX.setText("0");
		editPosY.setText("0");
		editWidth.setText("100");
		editHeight.setText("100");
		editzOrder.setText("-1");
		editInterval.setText("500");

	}
	
	public void CreateGUI(){
		String PaneType[] = {"STATIC", "PAGING", "V_Scroll", "H_Scroll", "VIDEO"};
		JLabel labelType = new JLabel("PaneType");
		JLabel labelPosX = new JLabel("Position X");
		JLabel labelPosY = new JLabel("Position Y");
		JLabel labelWidth = new JLabel("Width");
		JLabel labelHeight = new JLabel("Height");
		JLabel labelzOrder = new JLabel("Z-Order");
		JLabel labelInterval = new JLabel("Interval");
		
		comboPaneType = new JComboBox(PaneType);
		comboPaneType.setEnabled(false);
		
		panLabel.setLayout(new GridLayout(1, 6, 5, 5));
		panData.setLayout(new GridLayout(1, 6, 5, 5));
		
		panLabel.add(labelType);
		panLabel.add(labelPosX);
		panLabel.add(labelPosY);
		panLabel.add(labelWidth);
		panLabel.add(labelHeight);
		panLabel.add(labelzOrder);
		panLabel.add(labelInterval);
		
		panData.add(comboPaneType);
		panData.add(editPosX);
		panData.add(editPosY);
		panData.add(editWidth);
		panData.add(editHeight);
		panData.add(editzOrder);
		panData.add(editInterval);
		
		panControl.add(btnModify);
		panControl.add(btnClose);
		
		add(panLabel, BorderLayout.NORTH);
		add(panData, BorderLayout.CENTER);
		add(panControl, BorderLayout.SOUTH);
		setSize(900, 100);
		
		btnModify.addActionListener(this);//Event 등록
		btnClose.addActionListener(this);//Event 등록
		
		SetInitValues();
		
		setTitle("Modify Pane");
	}
}
