package marquee.config.view;

public class CheckFileExtesion {
	public static boolean CheckExtesion(String FileName, String Extesion){
		String ExtReadfromFileName = new String(FileName);
		
		int dot = FileName.lastIndexOf('.');
		if (dot > -1) {
			ExtReadfromFileName = FileName.substring(dot+1);
			if(0 == Extesion.compareTo(ExtReadfromFileName)){
				return true;
			}
		}
		
		FileName = FileName + "." + Extesion;
		return false;
	}
}
