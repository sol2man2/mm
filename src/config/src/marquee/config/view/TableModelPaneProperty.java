package marquee.config.view;

import javax.swing.table.DefaultTableModel;

public class TableModelPaneProperty extends DefaultTableModel{
	TableModelPaneProperty(){
		super();
	}
	
	public void insertRow(int row, PaneInfo paneInfo) {
		Object[] rowData = {
				paneInfo.getNo(), 
				paneInfo.getPaneType(),
				paneInfo.getPosX(),
				paneInfo.getPosY(),
				paneInfo.getSizeWidth(),
				paneInfo.getSizeHeight(),
				paneInfo.getZOrder(),
				paneInfo.getInterval()};
        super.insertRow(row, rowData);
    }
	
	public boolean isCellEditable(int row, int column){
//		if(column < 2){
//			return false;
//		}
//		else{
//			return true;
//		}
		return false;
	}
	
	TableModelPaneProperty(Object[] columnNames, int rowCount) {
        super(columnNames, rowCount);
	}
}
