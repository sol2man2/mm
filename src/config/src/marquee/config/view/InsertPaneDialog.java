package marquee.config.view;

import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.io.File;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.filechooser.FileFilter;
import javax.swing.filechooser.FileNameExtensionFilter;

import marquee.adlib.ads.AdPaneType;
import marquee.adlib.ads.ScrollType;
import marquee.adlib.ads.impl.AdPane;
import marquee.config.control.ConfigControllerInterface;

public class InsertPaneDialog extends JFrame implements ActionListener{
	private JComboBox comboPaneType = null;
	private JPanel panLabel = new JPanel();
	private JPanel panData = new JPanel();
	private JPanel panControl = new JPanel();
	private ConfigControllerInterface controlConfig = null;
	private JButton btnAdd = new JButton("Add");
	private JButton btnClose = new JButton("Close");
	
	//private JTextPane editPosX = new JTextPane();
	private JTextField editPosX = new JTextField();
	private JTextField editPosY = new JTextField();
	private JTextField editWidth = new JTextField();
	private JTextField editHeight = new JTextField();
	private JTextField editInterval = new JTextField();
	
	private File choosePath = null;
	
	public void setConfigControllIf(ConfigControllerInterface cfg){
		controlConfig = cfg;
	}
	
	public InsertPaneDialog() {
		CreateGUI();
		
	}
	
	public void actionPerformed(ActionEvent ae) 
	  {
		  if(ae.getSource() == btnAdd) {
			  AdPane pane = new AdPane();

			  pane.setID(10);
			  pane.setScrollType(ScrollType.HORIZONTAL);
			  if("STATIC" == comboPaneType.getSelectedItem()){
				  pane.setAdPaneType(AdPaneType.STATIC);
			  }
			  else if("PAGING" == comboPaneType.getSelectedItem()){
				  pane.setAdPaneType(AdPaneType.PAGING);
			  }
			  else if("V_Scroll" == comboPaneType.getSelectedItem()){
				  pane.setAdPaneType(AdPaneType.SCROLLING);
				  pane.setScrollType(ScrollType.VERTICAL);
			  }
			  else if("H_Scroll" == comboPaneType.getSelectedItem()){
				  pane.setAdPaneType(AdPaneType.SCROLLING);
			  }
			  else if("VIDEO" == comboPaneType.getSelectedItem()){
				  pane.setAdPaneType(AdPaneType.VIDEO);
			  }
			  int posx = Integer.parseInt(editPosX.getText());
			  pane.setRectangle(new Rectangle(posx, Integer.parseInt(editPosY.getText()), Integer.parseInt(editWidth.getText()), Integer.parseInt(editHeight.getText())) );
			  pane.setInterval(Integer.parseInt(editInterval.getText()));
			  
			  ArrayList<String> listContent = AddContent(pane.getAdPaneType());
			  if(listContent.size() == 0){
				  System.err.println("이건 아니지... 파일이 없어");
				  return;
			  }
			  else{
				  pane.setContentsList(listContent);
				  // 상배가 만든 코드 
				  Rectangle rect = pane.getRectangle();
				  rect = controlConfig.checkRange(rect);
				  pane.setRectangle(rect);
				  if(controlConfig.checkOverlapByID(0, rect) == false){
					  if (JOptionPane.showConfirmDialog(new JFrame(),
			    			  "Do you want to confirm this position ?", "Warning - Overlap",
			    			  JOptionPane.YES_NO_OPTION) == JOptionPane.YES_OPTION)
						  controlConfig.addPane(pane);
				  }
				  else{
					  controlConfig.addPane(pane);
				  }
			  }
			  
		  }
		  else if(ae.getSource() == btnClose) {
			  setVisible(false);
		  }
	  }//end actionPerformed()
	
	private ArrayList<String> AddContent(AdPaneType paneType){
		 ArrayList<String> listContent = new ArrayList<String>();
		  
		 String[] strFilter = null;
		 String strFilterDesc = null;
		 
		 if(paneType == AdPaneType.VIDEO){
			 strFilter = FileExtFilter.GetMovieExtFilter();
			 strFilterDesc = FileExtFilter.GetMovieExtDesc();
		 }
		 else{
			 strFilter = FileExtFilter.GetPictureExtFilter();
			 strFilterDesc = FileExtFilter.GetPictureExtDesc();
		 }
		JFileChooser fc = new JFileChooser();
		FileFilter fXml = new FileNameExtensionFilter(strFilterDesc, strFilter);
		
		fc.setFileFilter(fXml);
		if(choosePath != null)
			fc.setCurrentDirectory(choosePath);
		fc.setAcceptAllFileFilterUsed(false);
		int result = fc.showOpenDialog(this);
		
		// if we selected an image, load the image
		if(result == JFileChooser.APPROVE_OPTION) {
			choosePath = fc.getSelectedFile();
		    String strFile = fc.getSelectedFile().getPath();
		    listContent.add(strFile);
		}
		
	   return listContent;
	}
	
	private void SetInitValues(){
		editPosX.setText("0");
		editPosY.setText("0");
		editWidth.setText("100");
		editHeight.setText("100");
		editInterval.setText("10");

	}
	
	private void setDefaultInterval(){
		if("PAGING" == comboPaneType.getSelectedItem().toString()){
    		editInterval.setText("500");
    	}
    	else{
    		editInterval.setText("10");
    	}
	}
	
	private void addComboListener(){
		comboPaneType.addItemListener(new ItemListener() {
		    public void itemStateChanged(ItemEvent e) {
		        if(e.getStateChange() == ItemEvent.SELECTED){
		        	setDefaultInterval();
		        }
		    }
		});
	}
	public void CreateGUI(){
		String PaneType[] = {"STATIC", "PAGING", "V_Scroll", "H_Scroll", "VIDEO"};
		JLabel labelType = new JLabel("PaneType");
		JLabel labelPosX = new JLabel("Position X");
		JLabel labelPosY = new JLabel("Position Y");
		JLabel labelWidth = new JLabel("Width");
		JLabel labelHeight = new JLabel("Height");
		JLabel labelInterval = new JLabel("Interval");
		
		comboPaneType = new JComboBox(PaneType);
		addComboListener();
		
		panLabel.setLayout(new GridLayout(1, 6, 5, 5));
		panData.setLayout(new GridLayout(1, 6, 5, 5));
		
		panLabel.add(labelType);
		panLabel.add(labelPosX);
		panLabel.add(labelPosY);
		panLabel.add(labelWidth);
		panLabel.add(labelHeight);
		panLabel.add(labelInterval);
		
		panData.add(comboPaneType);
		panData.add(editPosX);
		panData.add(editPosY);
		panData.add(editWidth);
		panData.add(editHeight);
		panData.add(editInterval);
		
		panControl.add(btnAdd);
		panControl.add(btnClose);
		
		add(panLabel, BorderLayout.NORTH);
		add(panData, BorderLayout.CENTER);
		add(panControl, BorderLayout.SOUTH);
		setSize(900, 100);
		
		btnAdd.addActionListener(this);//Event 등록
		btnClose.addActionListener(this);//Event 등록
		
		SetInitValues();
		setTitle("Add Pane");
	}
}
