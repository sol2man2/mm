package marquee.config.view;

public class FileExtFilter {
	final static String strImageDesc = new String("Image(jpg, gif png)");
	final static String[] strImageFilter = {"jpg", "gif", "png"};
	final static String strMovieDesc = new String("Movie(avi, mpg, mp4, flv, wmv)");
	final static String[] strMovieFilter = {"avi", "mpg", "mp4", "flv", "wmv"};
	
	public static String GetPictureExtDesc(){
		return strImageDesc;
	}
	
	public static String[] GetPictureExtFilter(){
		return strImageFilter;
	}
	
	public static String GetMovieExtDesc(){
		return strMovieDesc;
	}
	
	public static String[] GetMovieExtFilter(){
		return strMovieFilter;
	}
}
