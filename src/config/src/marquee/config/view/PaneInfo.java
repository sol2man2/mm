package marquee.config.view;

import marquee.adlib.ads.ScrollType;


public class PaneInfo{
	public int getNo() {
		return no;
	}
	public String getPaneType() {
		return paneType;
	}
	public void setNo(int no){
		this.no = no;
	}
	public void setPaneType(String paneType) {
		this.paneType = paneType;
	}
	public int getPosX() {
		return posX;
	}
	public void setPosX(int posX) {
		this.posX = posX;
	}
	public int getPosY() {
		return posY;
	}
	public void setPosY(int posY) {
		this.posY = posY;
	}
	public int getSizeWidth() {
		return sizeWidth;
	}
	public void setSizeWidth(int sizeWidth) {
		this.sizeWidth = sizeWidth;
	}
	public int getSizeHeight() {
		return sizeHeight;
	}
	public void setSizeHeight(int sizeHeight) {
		this.sizeHeight = sizeHeight;
	}
	public int getZOrder() {
		return zOrder;
	}
	public void setZOrder(int zOrder) {
		this.zOrder = zOrder;
	}
	public int getInterval() {
		return interval;
	}
	public void setInterval(int interval) {
		this.interval = interval;
	}
	private String paneType = new String("Static");
	public PaneInfo(String paneType, int posX, int posY, int sizeWidth,
			int sizeHeight, int zOrder, int interval) {
		super();
		this.no = -1;
		this.paneType = paneType;
		this.posX = posX;
		this.posY = posY;
		this.sizeWidth = sizeWidth;
		this.sizeHeight = sizeHeight;
		this.zOrder = zOrder;
		this.interval = interval;
	}
	private int no = 0;
	private int posX = 0;
	private int posY = 0;
	private int sizeWidth = 100;
	private int sizeHeight = 100;
	private int zOrder = 10;
	private int interval = 10;
}
