package marquee.config.view;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Component;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.util.ArrayList;

import javax.swing.ImageIcon;

import marquee.adlib.ads.AdPackageDataObject;
import marquee.adlib.ads.ScrollType;

public class DrawableScrollAd extends IDrawable {

	private ArrayList<Image> listImg = new ArrayList<Image>();
	final static float dash1[]={1.f};
	final static BasicStroke dashed = new BasicStroke(3.0f,BasicStroke.CAP_ROUND,BasicStroke.JOIN_ROUND, 10.0f,dash1, 0.0f);
	
	// Constructor
	public DrawableScrollAd(){
		AdDO = AdPackageDataObject.createDO();
	}
	
	//============================================================== Setter - DataObject
	public void setContentsList(ArrayList<String> listCon){
		for(String strCont : listCon){
			ImageIcon imageIcon = new ImageIcon(strCont);
			Image img = imageIcon.getImage();
			listImg.add(img);
		}
	}
	
	//============================================================== Component Drawing Interface
	@Override
	public void draw(Component c, Graphics g) {
		Graphics2D g2 = (Graphics2D)g;
		Color color = g2.getColor();
		g2.setColor(Color.WHITE);
		g2.clearRect(nowRect.x, nowRect.y, nowRect.width, nowRect.height);
		
		nowRect.x = newRect.x;
		nowRect.y = newRect.y;
		nowRect.width = newRect.width;
		nowRect.height = newRect.height;
		
		if(listImg.size() == 0){
			g2.setColor(Color.GRAY);    // Set the color.
			g2.drawString("No Picture", nowRect.x+5, nowRect.y+5);
			g2.drawRect( nowRect.x, nowRect.y, nowRect.width, nowRect.height);   
		}
		else{
			ScrollType scrollType = AdDO.getScrollType();
			
			if(scrollType == ScrollType.HORIZONTAL)
				fittingLandscape(g2,c);
			else
				fittingPortrait(g2,c);
		}
		
		if(selected){
			nowRect.grow(-2, -2);
			g2.setColor(Color.GREEN);
			g2.setStroke(dashed);
			g2.drawRect(nowRect.x, nowRect.y, nowRect.width, nowRect.height);
		}
		g2.setColor(color);
	}
	
	//============================================================== Fitting Image to Rectangle
	private void fittingLandscape(Graphics2D g2, Component c){
		// height,y는 고정임. x와 width를 원래 이미지 비율에 맞추어 구하면 된다.
		int x = nowRect.x;
		int height = nowRect.height;
		int width;
		
		for(Image img : listImg){
			// 이미지 사이즈를 구한다.
			int w = img.getWidth(null);
			int h = img.getHeight(null);
			
			width = (height*w)/h;
			if(x + width > nowRect.x+nowRect.width){
				int width2 = nowRect.x+nowRect.width-x;
				int w2 = (width2*w)/width;
				g2.drawImage(img, x, nowRect.y, nowRect.x+nowRect.width, nowRect.y+height,0,0,w2,h, c);
				break;
			}
			else	
				g2.drawImage(img, x, nowRect.y, width, height, c);

			x += width;
		}
	}
	
	private void fittingPortrait(Graphics2D g2, Component c){
		// x, width는 고정임. y, height를 원래 이미지 비율에 맞추어 구하면 된다.
		int y = nowRect.y;
		int width = nowRect.width;
		int height;
		
		for(Image img : listImg){
			// 이미지 사이즈를 구한다.
			int w = img.getWidth(null);
			int h = img.getHeight(null);
			
			height = (width*h)/w;
			if(y+height > nowRect.y+nowRect.height){
				int height2 = nowRect.y+nowRect.height-y;
				int h2 = (height2*h)/height;
				g2.drawImage(img, nowRect.x, y, nowRect.x+width, nowRect.y+nowRect.height,0,0,w,h2, c);
				break;
			}
			else
				g2.drawImage(img, nowRect.x, y, width, height, c);
			
			y += height;
		}
	}
}
