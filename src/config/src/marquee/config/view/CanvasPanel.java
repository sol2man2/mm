package marquee.config.view;

import java.awt.*;
import java.awt.event.*;
import java.awt.image.BufferedImage;
import java.util.ArrayList;

import javax.swing.*;

import marquee.adlib.ads.AdModelInterface;
import marquee.adlib.ads.AdPackageDataObject;
import marquee.adlib.ads.AdPaneType;
import marquee.config.view.IDrawable.StateDraw;

///////////////////////////////////////////////////////////////////// PaintPanel
public class CanvasPanel extends JPanel implements MouseListener, MouseMotionListener {
 
	 //================================================================ constants
	 private static final int SIZE = 300;     // Size of paint area.
//	 private static final Shape INIIIAL_SHAPE = Shape.RECTANGLE;
	 private static final Color INITIAL_COLOR = Color.WHITE;
	 private static final Color COLOR_BACKGROUND = Color.WHITE;
	 private enum  State { IDLE, DRAGGING }
	 
	 //=================================================================== fields
	 private State _state = State.IDLE;
//	 private Shape _shape = INIIIAL_SHAPE;
	 private Color _color = INITIAL_COLOR;
	 
	 //... BufferedImage stores the underlying saved painting.
	 //    Initialized first time paintComponent is called.
	 private BufferedImage _bufImage = null;
	 
	 private ArrayList<IDrawable> listChild = new ArrayList<IDrawable>();
	 private IDrawable selectedComponent = null;
	 
	 private Point _init = null; // Where mouse is pressed.
	 private Point _start = null; // Where mouse is pressed.
	 private Point _end   = null; // Where mouse is dragged to or released.
	 
	 //============================================================== constructor
	 public CanvasPanel() {
	     setPreferredSize(new Dimension(SIZE, SIZE));
	     setBackground(Color.white);
	     
	     this.addMouseListener(this);
	     this.addMouseMotionListener(this);
	 }
	 
	 public void initial(){
		 selectedComponent = null;
	 }
	 
	 //=========================================================== paintComponent
	 @Override public void paintComponent(Graphics g) {
	     Graphics2D g2 = (Graphics2D)g;  // Downcast to Graphics2D
	     
	     //... One time initialization of in-memory, saved image.
	     if (_bufImage == null) {
	         //... This is the first time, initialize _bufImage
	         int w = this.getWidth();
	         int h = this.getHeight();
	         _bufImage = (BufferedImage)this.createImage(w, h);
	         Graphics2D gc = _bufImage.createGraphics();
	         gc.setColor(COLOR_BACKGROUND);
	         gc.fillRect(0, 0, w, h); // fill in background
	     }
	     //... Overwrite the screen display with currently dragged image.
	     if (_state == State.DRAGGING) {
	         //... Write shape that is being dragged over the screen image,
	         //    but not into the saved buffered image.  It will be written
	         //    on the saved image when the mouse is released.
	    	 drawCurrentShape(_bufImage.createGraphics());   
	     }
	     //... Display the saved image.
	     g2.drawImage(_bufImage, null, 0, 0);
	 }
	 
	 //========================================================= drawCurrentShape
	 private void drawCurrentShape(Graphics2D g2) {
	     //... Draws current shape on a graphics context, either
	     //    on the context passed to paintComponent, or the
	     //    context for the BufferedImage.
		 g2.setColor(COLOR_BACKGROUND);
         g2.fillRect(0, 0, this.getWidth(), this.getHeight()); // fill in background
	     for(IDrawable drawing : listChild){
	    	 drawing.draw(this, g2);
	     }
	 }
	 
	 //============================================================= mousePressed
	 public void mousePressed(MouseEvent e) {

		 Point pt = e.getPoint();
		 selectItemCheck(pt);
		 
		 if(selectedComponent != null){
	    	 StateDraw stateDraw = selectedComponent.checkState(pt);
	    	 checkChangeCursor(stateDraw);
	    	 selectedComponent.setState(stateDraw);
	     }
	 }
	 
	 //============================================================= mouseDragged
	 public void mouseDragged(MouseEvent e) {
		 Point pt = e.getPoint();
//	     if(_state == State.DRAGGING){   // We're dragging to create a shape.
	    	 _end = pt;       // Set end point of drag.  May change.
	    	 
	    	if(selectedComponent != null){
	    		_state = State.DRAGGING;
	    		int dx = _end.x - _start.x;
	    		int dy = _end.y - _start.y;
	    		
	    		// Overlap Check
	    		Rectangle rect = selectedComponent.checkRange(dx,dy);
    			selectedComponent.mouseDragged(rect);
	    		
	    		drawCurrentShape(_bufImage.createGraphics());
	    	    this.repaint();            // After change, show new shape
	    	    
	    	    _start = _end;
	    	}
//	     }
	     System.out.println("mouseDragged");
	 }
	 
	 //============================================================ mouseReleased
	 public void mouseReleased(MouseEvent e) {
	     //... If released at end of drag, write shape into the BufferedImage,
	     //    which saves it in the drawing.
	     if (_state == State.DRAGGING) {
	         _state = State.IDLE;
	    	 
	         _end = e.getPoint();       // Set end point of drag.  May change.
	    	 
	    	 int dx = _end.x - _start.x;
	    	 int dy = _end.y - _start.y;
	    	 
	    	 Rectangle rect = selectedComponent.checkRange(dx,dy);
	    	 if(checkOverlap(rect) == false)
	    		 selectedComponent.mouseReleased(dx, dy);
	    	 else{
	    		 if (JOptionPane.showConfirmDialog(new JFrame(),
	    			  "Do you want to confirm this position ?", "Warning - Overlap",
	    			  JOptionPane.YES_NO_OPTION) == JOptionPane.YES_OPTION)
	    			 selectedComponent.mouseReleased(dx, dy);
	    		 else{
	    			 dx = _init.x - _start.x;
	    			 dy = _init.y - _start.y;
	    			 selectedComponent.mouseReleased(dx, dy);
	    		 }
	    	 }

	         //... Draw current shape in saved buffered image.
	         drawCurrentShape(_bufImage.createGraphics());
	         
	         this.repaint();
	     }
	     System.out.println("mouseReleased");
	 }
	 
	//============================================================ mouseMoved
	 public void mouseMoved  (MouseEvent e) {
		// Dragging 중이 아닐때 마우스 커서 바꾸기..
	     // top, bottom, left, right, lt, rt, lb, rb --> 8가지 상태 체크...
		 Point pt = e.getPoint();
		 
	     if(selectedComponent != null){
	    	 StateDraw stateDraw = selectedComponent.checkState(pt);
	    	 checkChangeCursor(stateDraw);
	     }
	     
//	     System.out.println("mouseMoved");
	 }
	 
	 //================================================== ignored mouse listeners
	 public void mouseEntered(MouseEvent e) {}
	 public void mouseExited (MouseEvent e) {}
	 public void mouseClicked(MouseEvent e) {}
	 
	//================================================== model data update listner.
	 public void update(AdModelInterface adInterface){
		ArrayList<AdPackageDataObject> listPane =  adInterface.getData();
		 
		listChild.clear();
		int selectedId = adInterface.getSelectedPaneID();
		
		for(AdPackageDataObject pane : listPane){
			 AdPaneType type = pane.getAdPaneType();
			 IDrawable adDraw = null;
			 if(type == AdPaneType.PAGING || type == AdPaneType.STATIC){
				 adDraw = new DrawableStaticAd();
			 }
			 else if(type == AdPaneType.SCROLLING){
				 adDraw = new DrawableScrollAd();
			 }
			 else if(type == AdPaneType.VIDEO){
				 adDraw = new DrawableVideoAd();
			 }
			 else{
				 continue;
			 }
			 if(selectedComponent != null){
				 if(selectedComponent.getID() == pane.getID()){
					adDraw.setSelected(true);
					selectedComponent = adDraw;
				 }
			 }
			 
			 if(selectedId == pane.getID()){
				 adDraw.setSelected(true);
			 }
			 adDraw.setAdPaneData(pane);
			 listChild.add(adDraw);
		 }

		 drawCurrentShape(_bufImage.createGraphics());
		 this.repaint();
	 }
	 
	//================================================== private Method
	 private void checkChangeCursor(StateDraw stateD){
		 switch(stateD){
    	 case IDLE:
    		 setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
    		 break;
    	 case DRAGGING:
    		 setCursor(Cursor.getPredefinedCursor(Cursor.MOVE_CURSOR));
    		 break;
    	 case RESIZING_T:
    		 setCursor(Cursor.getPredefinedCursor(Cursor.N_RESIZE_CURSOR));
    		 break;
    	 case RESIZING_B:
    		 setCursor(Cursor.getPredefinedCursor(Cursor.S_RESIZE_CURSOR));
    		 break;
    	 case RESIZING_L:
    		 setCursor(Cursor.getPredefinedCursor(Cursor.W_RESIZE_CURSOR));
    		 break;
    	 case RESIZING_R:
    		 setCursor(Cursor.getPredefinedCursor(Cursor.E_RESIZE_CURSOR));
    		 break;
    	 case RESIZING_LT:
    		 setCursor(Cursor.getPredefinedCursor(Cursor.NW_RESIZE_CURSOR));
    		 break;
    	 case RESIZING_RT:
    		 setCursor(Cursor.getPredefinedCursor(Cursor.NE_RESIZE_CURSOR));
    		 break;
    	 case RESIZING_LB:
    		 setCursor(Cursor.getPredefinedCursor(Cursor.SW_RESIZE_CURSOR));
    		 break;
    	 case RESIZING_RB:
    		 setCursor(Cursor.getPredefinedCursor(Cursor.SE_RESIZE_CURSOR));
    		 break;
    	 }
	 }
	 
	 //================================================== 마우스 위치에 Item 있는지 체크함.
	 private void selectItemCheck(Point pt){
		 boolean bExist = false;
		 selectedComponent = null;
		 // Click된 지점에 오브젝트가 있는지 확인한다.

		 for(int i=listChild.size()-1; i>= 0; i--){
			 IDrawable drawing = listChild.get(i);
		 //for(IDrawable drawing : listChild){
	    	 if(drawing.contains(pt) && bExist == false){
	    		 drawing.setSelected(true);
	    		 selectedComponent = drawing;
	    		 bExist = true;
	    		 drawing.setSelectePanedId();
	    	 }
	    	 else{
	    		 drawing.setSelected(false);
	    	 }
	     }

		 // 1. 있으면...... 해당 오브젝트를 Select Object로 지정한다.
		 //    이후 Dragging 시에 해당 오브젝트를 무빙 시킨다.
		 if(bExist){
//			 _state = State.DRAGGING;   // Assume we're starting a drag. 
			 _start = pt;     			// Save start point, and also initially
		     _end   = _start;           // as end point, which drag will change.
		     _init = _start;
		 }
		 // 2. 없으면...... 
		 //    아무것도 안한다.
		 else{
			 for(IDrawable drawing : listChild){
	    		 drawing.setSelected(false);
		     }
		 }
		 drawCurrentShape(_bufImage.createGraphics());
         this.repaint();
	 }
	 
		
	//================================================== 오버랩 체크하기
	 public boolean checkOverlap(Rectangle rect){
		 
		 for(IDrawable pane : listChild){
			 if(pane.isSelected() ==false){		// Selected component는 제외
				 if(pane.intersections(rect)){
					 return true;
				 }
			 }
		 }
		 return false;
	 }
}
