package marquee.config.view;

import java.awt.Component;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.filechooser.FileFilter;
import javax.swing.filechooser.FileNameExtensionFilter;

import marquee.adlib.utils.ConfigFileUtils;

public class CDialogPackaging extends JDialog implements ActionListener{
	
	private JPanel	   jpSource = new JPanel();
	private JPanel	   jpPackage = new JPanel();
	private JPanel	   jpButton = new JPanel();
	
	private JTextField tfSourcePath = new JTextField();
	private JTextField tfPackageName = new JTextField();
	private JButton    btnSourceChooser = new JButton("Browse...");
	private JButton    btnPackageStart = new JButton("Start Packaging");
	
	private JButton    btnCancel = new JButton("Cancel");
	
	private JLabel     jlSource = new JLabel("Source Path  ");
	private JLabel	   jlPackage= new JLabel("Package Name  ");
	
	private String 	strSourcePath = "";
//	private String  strPackagePath = new String("c:\\");
	
	public CDialogPackaging() {
		super();
		
		createGUI();
		setSize(300, 200);
	}
	
	private void createGUI(){

		getContentPane().setLayout(new GridLayout(3,1,5,5)); 
		
		tfSourcePath.setEditable(false);
		tfSourcePath.setText("");
		tfPackageName.setText("");
		
		btnSourceChooser.addActionListener(this);
		btnPackageStart.addActionListener(this);
		btnCancel.addActionListener(this);
		
		jpSource.setLayout(new BoxLayout(jpSource,BoxLayout.LINE_AXIS));
		
		btnSourceChooser.setAlignmentX(Component.RIGHT_ALIGNMENT);
		jpSource.add(jlSource);
		jpSource.add(tfSourcePath);
		jpSource.add(btnSourceChooser);

		getContentPane().add(jpSource);

		jpPackage.setLayout(new BoxLayout(jpPackage,BoxLayout.LINE_AXIS));
		jlPackage.setAlignmentX(Component.LEFT_ALIGNMENT);
		jpPackage.add(jlPackage);
		jpPackage.add(tfPackageName);

		getContentPane().add(jpPackage);

		jpButton.setLayout(new GridLayout(1, 2, 5, 5));
		jpButton.add(btnPackageStart);
		jpButton.add(btnCancel);

		getContentPane().add(jpButton);
	}

	@Override
	public void actionPerformed(ActionEvent arg0) {
		if(arg0.getSource() == btnSourceChooser) {
			String strRet = selectSourceFile();
			
			if(strRet == null){
				tfSourcePath.setText("");
				strSourcePath = "";
			}
			else{
				tfSourcePath.setText(strRet);
				strSourcePath = strRet;
			}
		}
		else if(arg0.getSource() == btnPackageStart){
			// Source File 유효성 검사하기
			if(checkSourcePath()){	
				// 입력한 Package 이름에 확장자가 있을 경우와 없을 경우를 나누어서 한다.
				String strPackage = /*strPackagePath+*/tfPackageName.getText();
				String exe = SplitPath(strPackage);
				
				if(exe.equals("tar") == false){
					strPackage = strPackage + ".tar";
				}
				// packing 하기
				ConfigFileUtils myUtils = new ConfigFileUtils();
				boolean bRet = myUtils.packAdPackage(strSourcePath, strPackage);
				if(bRet)
					JOptionPane.showMessageDialog(null, "Packaging is success !!!");
				else
					JOptionPane.showMessageDialog(null, "Packaging File is invalid\nCheck Packaging File !!");
				setVisible(false);
			}
			else{
				JOptionPane.showMessageDialog(null, "Your Selected File is invalid. Check File!!");	
			}
		}
		else if(arg0.getSource() == btnCancel){
			setVisible(false);
		}
	}
	
	private String selectSourceFile(){
		String strFile=null;
		JFileChooser fc = new JFileChooser();
		FileFilter fXml = new FileNameExtensionFilter("manifest(*.xml)","xml");
		fc.setFileFilter(fXml);
		fc.setAcceptAllFileFilterUsed(false);
		int result = fc.showOpenDialog(this);
		
		// if we selected an image, load the image
		if(result == JFileChooser.APPROVE_OPTION) {
		    strFile = fc.getSelectedFile().getPath();
		}
		return strFile;
	}
	
	private boolean checkSourcePath(){
		File checkF = new File(strSourcePath);
		
		return checkF.exists();
	}
	
	private String SplitPath(String strPath){
		String exeName = null;
		//int file = strPath.lastIndexOf("/");
	    int ext = strPath.lastIndexOf(".");
	    int length = strPath.length();
	    //fileName = strPath.substring(file+1,ext);
	    exeName = strPath.substring(ext+1,length);
	    
	    return exeName;
	}
}
