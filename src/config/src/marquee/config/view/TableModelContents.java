package marquee.config.view;

import javax.swing.table.DefaultTableModel;

public class TableModelContents extends DefaultTableModel{
	TableModelContents(){
		super();
	}
	
	public static String getExtension(String filepath) {
	      int dot = filepath.lastIndexOf('\\');
	      if (dot > -1) {
	          filepath = filepath.substring(dot+1);
	          return filepath;
	      } 
	      else {
	          return null;
	      }
	  }

	
	public void insertRow(int row, ContentInfo contentInfo ) {
		String strNo = new String();
		if(contentInfo.getNo() >= 0){
			strNo = Integer.toString(contentInfo.getNo());
		}
		else{
			strNo = "";
		}
		Object[] rowData = {
				strNo, 
				getExtension(contentInfo.getContentPath())};
		
        super.insertRow(row, rowData);
    }
	
	public boolean isCellEditable(int row, int column){
//		if(column < 2){
//			return false;
//		}
//		else{
//			return true;
//		}
		return false;
	}

	
	TableModelContents(Object[] columnNames, int rowCount) {
        super(columnNames, rowCount);
	}
}
