package marquee.config.view;

//
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Font;
import java.awt.HeadlessException;
import java.awt.Panel;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.File;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTabbedPane;
import javax.swing.SwingConstants;
import javax.swing.UIManager;
import javax.swing.filechooser.FileFilter;
import javax.swing.filechooser.FileNameExtensionFilter;

import marquee.adlib.ads.AdModelInterface;
import marquee.adlib.utils.IObserver;
import marquee.adlib.utils.MarqueeEntityInfo;
import marquee.config.control.ConfigControllerInterface;

import com.lge.config.marquee.control.MarqueeManagerControl;
import com.lge.config.marquee.view.MarqueeView;

public class ConfigMainFrame extends JFrame implements ActionListener, IObserver {

	static ConfigControllerInterface controlConfig = null;
	AdModelInterface modelConfig = null;

	private JTabbedPane tabPane = new JTabbedPane();
	private JPanel configPane = new JPanel();
	private JPanel MarqueePane = new JPanel();
	private InsertPaneDialog insertPaneDlg = null;
	
	public final static int widthCanvas = 960;
	public final static int heightCanvas = 540;
	
	private MarqueeEntityInfo mEntityInfo = null;
	static ConfigControllerInterface getControl(){
		return controlConfig;
	}
	
	/*
	ToDo : Have to change code from "new" to "refer" about the CONTROL 
	*/
	//static MarqueeManagerControlInterface controlMarquee = null;
//	MarqueeManagerControl controlMarquee = new MarqueeManagerControl();
	
	PaneProperty pan = new PaneProperty();
	ConfigControl ccn = new ConfigControl();

	/*Define MarqueeView Pane*/
	JPanel marqueeOption = new JPanel();
	private MarqueeView marqueeView = null;
	
	// TODO Test Code
	private Panel pDMPanel=new Panel();
	private JButton btnNew = null;
	private JButton btnLoad=null;
	private JButton btnSave=null;
	//private JButton btnPreview=null;
	
	private Panel pBtnPanel=new Panel();
	private JButton btnAddPane=null;
	//private Button btnOval=new Button("Oval");
	//private Button btnLine=new Button("Line");
	private JButton btnPackage = null;
	
	/*Button for MarqueeView Pane*/
	private JButton newb = null;
	private JButton saveb = null;
	private JButton deleteb = null;
/*	
	Function : SW and Package update 
	ToDo : It is not used in initial version
*/
//	private JButton updateb = null;
		
	CanvasPanel _canvas = new CanvasPanel();
	
	// File Open/Save 당시의 Path를 저장하고자 함.
	File choosePath = null;

	public void initial(){
		_canvas.initial();
	}
	
	public ConfigMainFrame(ConfigControllerInterface controlConfig,
			AdModelInterface modelConfig, MarqueeView viewMarquee) throws HeadlessException {
		super("Marquee Configuration Tool");
		   
		try{			
	       UIManager.setLookAndFeel (UIManager.getSystemLookAndFeelClassName ());      
		}catch(Exception c){c.printStackTrace ();}
		
		this.controlConfig = controlConfig;
		this.modelConfig = modelConfig;
		this.marqueeView = viewMarquee;
		
		modelConfig.registerObserver(this);
		
		CreateButtons();
		
		CreateGUI();
		setResizable(false);
	}
	
	private void CreateButtons(){
		btnNew=new JButton("New");
		btnLoad=new JButton("Load");
		btnSave=new JButton("Save");
		//btnPreview=new JButton("Preview");
		
		btnAddPane=new JButton("Add Pane");
		btnPackage = new JButton("Packaging");
		
		/*Button for MarqueeView Pane*/
		newb = new JButton("New");
		saveb = new JButton("Save");
		deleteb = new JButton("Delete");
//		updateb = new JButton("Update");	// SW and Package update
//		btnNew.setPreferredSize(new Dimension(80, 100));
//		btnLoad.setPreferredSize(new Dimension(80, 100));
//		btnSave.setPreferredSize(new Dimension(80, 100));
//		btnPreview.setPreferredSize(new Dimension(80, 100));
	}
	
	public void CreateGUI() {

		pDMPanel.add(btnNew);
		pDMPanel.add(btnLoad);
		pDMPanel.add(btnSave);
		//pDMPanel.add(btnPreview);
		pDMPanel.setLayout(new BoxLayout(pDMPanel, BoxLayout.X_AXIS));

		pBtnPanel.add(btnAddPane);
//		pBtnPanel.add(btnOval);
//		pBtnPanel.add(btnLine);
		pBtnPanel.add(btnPackage);
		pBtnPanel.setLayout(new BoxLayout(pBtnPanel, BoxLayout.X_AXIS));
		
		setVisible( true );
		
		pan = new PaneProperty();
		pan.setConfigControllIf(controlConfig);
		ccn.add(pDMPanel, BorderLayout.NORTH);
		ccn.add(pBtnPanel, BorderLayout.CENTER);
		
		tabPane.addTab("Config", configPane);
		tabPane.addTab("Marquee", MarqueePane);
		
		configPane.setLayout(null);
		configPane.add(_canvas);
		configPane.add(ccn);
		configPane.add(pan);
		
		add(tabPane);

		_canvas.setBounds(0, 0, 960, 540);
		pan.setBounds(0, 543, 1260, 250);
		ccn.setBounds(960, 0, 300, 540);
		
		tabPane.setBounds(0, 0, 1300, 800);
		//configPane.setBounds(0, 0, 1300, 800);
		
//		marqueeView.setLayout(new BorderLayout());
//		marqueeView.setBounds(0, 0, 1300, 350);		
//		marqueeOption.setBounds(0,800,1300,900);

		/*Configure MarqueeView Pane*/
		marqueeOption.setBackground(Color.WHITE);
//		marqueeOption.setBounds(0, 500, 1300, 100);
		marqueeOption.add(newb);
		marqueeOption.add(saveb);
		marqueeOption.add(deleteb);
//		marqueeOption.add(updateb);	// SW and Package update
		
		MarqueePane.setLayout(new BorderLayout());
		MarqueePane.add(marqueeView, BorderLayout.CENTER);
		MarqueePane.add(marqueeOption, BorderLayout.SOUTH);
		MarqueePane.setBounds(0,0,1300,500);		
		
//		MarqueePane.setLayout(new GridLayout(2,1));
//		MarqueePane.add(marqueeView);
//		MarqueePane.add(marqueeOption);
		
		setSize(1260, 850);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		btnNew.addActionListener(this);//Event 등록
		btnLoad.addActionListener(this);//Event 등록
		btnSave.addActionListener(this);//Event 등록
		//btnPreview.addActionListener(this);//Event 등록

		btnAddPane.addActionListener(this);

		btnPackage.addActionListener(this);
		
		/*Register Event for MarqueeView Pane*/
		newb.addActionListener(this);
		saveb.addActionListener(this);
		deleteb.addActionListener(this);
//		updateb.addActionListener(this);	// SW and Package update
		
		addWindowListener( new WindowAdapter() {
			
			public void windowOpened(WindowEvent e) {
				
				System.out.println("Window Opened Event");     
	        } 
			
			public void windowClosing( WindowEvent we ) {
				System.exit(0);
			}
		});
			
		insertPaneDlg = new InsertPaneDialog();
		insertPaneDlg.setConfigControllIf(controlConfig);
		insertPaneDlg.setVisible(false);
		pan.setConfigControllIf(controlConfig);  
	  }
	
	  private void newMarqueeList(){
		System.out.println("New button is pressed");	
		marqueeView.newMarqueeList();
	  }
	
	  private void saveMarqueeList(){
		System.out.println("Save button is pressed");
		marqueeView.saveMarqueeList();
	  }
	
	  private void deleteMarqueeList(){
		System.out.println("Delete button is pressed");
		marqueeView.deleteMarqueeList();
	  }	
	  
	  // SW and Package update	  
/*	  private void updateMarquee(){
		  System.out.println("Delete button is pressed");
		  marqueeView.updateMarqueeData();
	  }*/
	
	  public void actionPerformed(ActionEvent ae) 
	  {
		  if(ae.getSource() == btnNew) {
			  controlConfig.newAdPackage();
		  }
		  else if(ae.getSource() == btnLoad){
			  clickBtnLoad();
		  }
		  else if(ae.getSource() == btnSave){
			  clickBtnSave();
		  }
//		  else if(ae.getSource() == btnPreview){
//			  controlConfig.displayPreview();
//		  }
		  else if(ae.getSource() == btnAddPane){
			  insertPaneDlg.setVisible(true);
		  }
		  else if(ae.getSource() == btnPackage){
			  CDialogPackaging dialog = new CDialogPackaging();
			  dialog.setTitle("Make Package File");
			  dialog.setModal(true);
			  dialog.setBounds(400,100, 400, 110);
			  dialog.setVisible(true);
		  }
		  else if(ae.getSource() == newb){
			  JOptionPane.showMessageDialog(null, "Set the Mobile-Marquee configuration", "New", 1);
			  newMarqueeList();
		  }
		  else if(ae.getSource() == saveb){
			  saveMarqueeList();
			  JOptionPane.showMessageDialog(null, "Mobile-Marquee is saved", "Save", 1);
		  }
		  else if(ae.getSource() == deleteb){
			  deleteMarqueeList();
			  JOptionPane.showMessageDialog(null, "Mobile-Marquee is deleted", "Delete", 1);
		  }
		  // SW and Package update
/*		  else if(ae.getSource() == updateb){
			  updateMarquee();
		  }*/
	  }//end actionPerformed()

	  @Override
	  // Model Component에 변경이 발생하였을때 호출되는 Call Back 함수라고 보면됨.
	  public void update() {
		  _canvas.update(modelConfig);
		  pan.update(modelConfig);
		  repaint();
	  }

	  private void clickBtnLoad() {
		// create a filechooser
		JFileChooser fc = new JFileChooser();
//		FileFilter fPkg = new FileNameExtensionFilter("Package(*.pkg)","pkg");
//		fc.addChoosableFileFilter(fPkg);
		FileFilter fXml = new FileNameExtensionFilter("manifest(*.xml)","xml");
		fc.setFileFilter(fXml);
		if(choosePath != null)
			fc.setCurrentDirectory(choosePath);
		fc.setAcceptAllFileFilterUsed(false);
		int result = fc.showOpenDialog(this);
		
		// if we selected an image, load the image
		if(result == JFileChooser.APPROVE_OPTION) {
		    String strFile = fc.getSelectedFile().getPath();
		    choosePath = fc.getSelectedFile();
		    controlConfig.loadAdPackage(strFile);
		}
	  }
	  
	  private void clickBtnSave(){
		// create a filechooser
		JFileChooser fc = new JFileChooser();
//		FileFilter fPkg = new FileNameExtensionFilter("Package(*.pkg)","pkg");
//		fc.addChoosableFileFilter(fPkg);
		FileFilter fXml = new FileNameExtensionFilter("manifest(*.xml)","xml");
		fc.addChoosableFileFilter(fXml);
		if(choosePath != null)
			fc.setCurrentDirectory(choosePath);
		fc.setAcceptAllFileFilterUsed(false);
		int result = fc.showSaveDialog(this);
		String strFileName = new String(fc.getSelectedFile().getPath());
		if(false == CheckFileExtesion.CheckExtesion(strFileName, "xml")){
			fc.setSelectedFile(new File(strFileName + ".xml"));
			//JOptionPane.showMessageDialog(null, ".xml 붙였어요");
			//return;
		}
		
		// if we selected an image, load the image
		if(result == JFileChooser.APPROVE_OPTION) {
		    String strFile = fc.getSelectedFile().getPath();
		    controlConfig.saveAdPackage(strFile);
		}
	  }
}

//// set the current directory to be the images directory
//File swingFile = new File("resources/images/About.jpg");
//if(swingFile.exists()) {
//    fc.setCurrentDirectory(swingFile);
//    fc.setSelectedFile(swingFile);
//}
