package marquee.config.view;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Font;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.File;
import java.util.ArrayList;

import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.SwingConstants;
import javax.swing.filechooser.FileFilter;
import javax.swing.filechooser.FileNameExtensionFilter;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableColumn;

import marquee.adlib.ads.AdModelInterface;
import marquee.adlib.ads.AdPackageDataObject;
import marquee.adlib.ads.AdPaneType;
import marquee.adlib.ads.ScrollType;
import marquee.config.control.ConfigControllerInterface;

public class PaneProperty extends JPanel{
	
	private JTable tableCanvasProperty = null;
	private JTable tableContents = null;
	private JScrollPane scrollPane = null;
	private JScrollPane scrollContents = null;
	private TableModelPaneProperty tablePropertyModel = null;
	private TableModelContents tableContentModel = null;
	private int numPane = 0;
	private ConfigControllerInterface controlConfig = null;
	
	private ModifyPaneDialog modifyPaneDlg = null;
	
	private ArrayList<IDrawable> listChild = new ArrayList<IDrawable>();
	private ArrayList<AdPackageDataObject> listPane =  null;
	
	final int INITIAL_ROWHEIGHT = 33;
	
	private File choosePath = null;
	
	// Column Items
	final String colProperty[] = {
		"No.",
		"Type", 
		"Position X",
		"Position Y",
		"Width",
		"Height",
		"z-order",
		"Interval",
	};
	
	final String colContents[] = {
			"No.",
			"Content",
		};

	public void setConfigControllIf(ConfigControllerInterface cfg){
		controlConfig = cfg;
	}
	
	public JScrollPane createPropertyTable() {
		// Make Table Model
		// Table의 폼을 결정한다.
		tablePropertyModel = new TableModelPaneProperty(colProperty, 0);
		tableCanvasProperty = new JTable(tablePropertyModel){
			DefaultTableCellRenderer renderCenter=new DefaultTableCellRenderer();
	          {//initializer block
	        	  renderCenter.setHorizontalAlignment(SwingConstants.CENTER);
	          }
	        @Override
	        public TableCellRenderer getCellRenderer(int arg0, int arg1) {
	               return renderCenter;
	        }
		};
		
//		// Table중 Pane Type을 선택할때에 표 내에 ComboBox를 이용하기 위해서 추가함.
//		JComboBox comboPaneType = new JComboBox();
//		comboPaneType.addItem("Static");
//		comboPaneType.addItem("Paging");
//		comboPaneType.addItem("H_Scroll");
//		comboPaneType.addItem("V_Scroll");
//		comboPaneType.addItem("Video");
		
		TableColumn paneTypeColumn = tableCanvasProperty.getColumn("Type");
        //paneTypeColumn.setCellEditor(new DefaultCellEditor(comboPaneType));
		//paneTypeColumn.setCellEditor()
	    
        int width = 127;
        int i = 1;
        tableCanvasProperty.setFont(new Font("Verdana", Font.PLAIN , 16));
        tableCanvasProperty.setRowHeight(20);
        
        paneTypeColumn = tableCanvasProperty.getColumn(colProperty[0]);
    	paneTypeColumn.setPreferredWidth(40);
        while(i < tableCanvasProperty.getColumnCount()){
        	paneTypeColumn = tableCanvasProperty.getColumn(tableCanvasProperty.getColumnName(i));
        	paneTypeColumn.setPreferredWidth(width);
        	i++;
        }
        
        // Row 길이 자동 조절
        class WidthRenderer extends DefaultTableCellRenderer  
        {  
          public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column)  
          {  
            Component comp = super.getTableCellRendererComponent(table,value,isSelected,hasFocus,row,column);  
            int width = comp.getPreferredSize().width+4;//4=inset?  
            int tcWidth = table.getColumnModel().getColumn(column).getWidth();  
            if(width > tcWidth) table.getColumnModel().getColumn(column).setPreferredWidth(width);  
            return comp;  
          }  
        }
        
        // Row크기 자동 조절 기능 추가
        tableCanvasProperty.setDefaultRenderer(Object.class, new WidthRenderer());
        tableCanvasProperty.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);

        return new JScrollPane(tableCanvasProperty);
    }
	
	public JScrollPane createContentsTable() {
		// Make Table Model
		// Table의 폼을 결정한다.
		tableContentModel = new TableModelContents(colContents, 0);
		tableContents = new JTable(tableContentModel){
			DefaultTableCellRenderer renderCenter=new DefaultTableCellRenderer();
	          {//initializer block
	        	  renderCenter.setHorizontalAlignment(SwingConstants.CENTER);
	          }
	        @Override
	        public TableCellRenderer getCellRenderer(int arg0, int arg1) {
	               return renderCenter;
	        }
		};
		
		TableColumn paneTypeColumn = new TableColumn();

		
		tableContents.setFont(new Font("Verdana", Font.PLAIN , 16));
        tableContents.setRowHeight(20);
    	paneTypeColumn = tableContents.getColumn(colContents[0]);
    	paneTypeColumn.setPreferredWidth(40);
    	paneTypeColumn = tableContents.getColumn(colContents[1]);
    	paneTypeColumn.setPreferredWidth(240);
        
        // Row 길이 자동 조절
        class WidthRenderer extends DefaultTableCellRenderer  
        {  
          public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column)  
          {  
            Component comp = super.getTableCellRendererComponent(table,value,isSelected,hasFocus,row,column);  
            int width = comp.getPreferredSize().width+4;//4=inset?  
            int tcWidth = table.getColumnModel().getColumn(column).getWidth();  
            if(width > tcWidth) table.getColumnModel().getColumn(column).setPreferredWidth(width);  
            return comp;  
          }  
        }
        
        // Row크기 자동 조절 기능 추가
        tableContents.setDefaultRenderer(Object.class, new WidthRenderer());
        tableContents.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
        
        return new JScrollPane(tableContents);
    }
	
	private int AddList(ArrayList<String> listContent, AdPaneType paneType){
		 //ArrayList<String> listContent = new ArrayList<String>();
		  
		String[] strFilter = null;
		 String strFilterDesc = null;
		 
		 if(paneType == AdPaneType.VIDEO){
			 strFilter = FileExtFilter.GetMovieExtFilter();
			 strFilterDesc = FileExtFilter.GetMovieExtDesc();
		 }
		 else{
			 strFilter = FileExtFilter.GetPictureExtFilter();
			 strFilterDesc = FileExtFilter.GetPictureExtDesc();
		 }
		JFileChooser fc = new JFileChooser();
		FileFilter fXml = new FileNameExtensionFilter(strFilterDesc, strFilter);
		
		fc.setFileFilter(fXml);
		if(choosePath != null)
			fc.setCurrentDirectory(choosePath);
		fc.setAcceptAllFileFilterUsed(false);
		int result = fc.showOpenDialog(this);
		
		// if we selected an image, load the image
		if(result == JFileChooser.APPROVE_OPTION) {
		    String strFile = fc.getSelectedFile().getPath();
		    choosePath = fc.getSelectedFile();
		    listContent.add(strFile);
		}
		else{
			return -1;
		}
		
	   return 0;
	}
	
	private void AddContent(){
		int rowcount = tableContents.getRowCount()-1;
		int contentSelected = tableContents.getSelectedRow();
		int canvasSelected = tableCanvasProperty.getSelectedRow();
		if(canvasSelected < 0){
			System.err.println("Canvas가 선택되어야 함 현재 값이 \"" + canvasSelected + "\" 임");
			return;
		}
		if((rowcount) == contentSelected){
			String idStr = new String(tableCanvasProperty.getModel().getValueAt(canvasSelected, 0).toString());
			int id = Integer.parseInt(idStr);
			// Pane List로부터 선택된 ID와 맞는 Pane으로부터 Content List를 얻어옴
			for(AdPackageDataObject pane : listPane){
				if(id == pane.getID()){
					if(pane.getAdPaneType() == AdPaneType.STATIC){
						System.err.println("왜 static인데 추가하려고 하니");
						return;
					}
					ArrayList<String> contentList = pane.getContentsList();
					if(0 != AddList(contentList, pane.getAdPaneType())){
						System.err.println("파일 하나는 선택해야지");
						return ;
					}
					//paneContents.removeAll();
					//ArrayList<String> contentList = pane.getContentsList();
					//contentList.ldjkl
					while(tableContentModel.getRowCount()>0){
    					tableContentModel.removeRow(0);
					}
					ContentInfo contentinfo = new ContentInfo();
					for(int j=0; j < contentList.size(); j++)
			        {
			        	contentinfo.setNo(j);
			        	contentinfo.setContentPath(contentList.get(j));
			        	tableContentModel.insertRow(tableContentModel.getRowCount(), contentinfo);
			        	//paneContents.add(strContent);
			        }
					contentinfo.setNo(-1);
		        	contentinfo.setContentPath("\\...");
		        	tableContentModel.insertRow(tableContentModel.getRowCount(), contentinfo);
					controlConfig.modifyPane(id, pane);
				}
			}
		}
		
		tableCanvasProperty.setRowSelectionInterval(canvasSelected, canvasSelected);
	}
	
	private void ModifyContent(){
		int canvasSelected = tableCanvasProperty.getSelectedRow();
		if(canvasSelected < 0){
			System.err.println("Canvas가 선택되어야 함 현재 값이 \"" + canvasSelected + "\" 임");
			return;
		}
		String idStr = new String(tableCanvasProperty.getModel().getValueAt(canvasSelected, 0).toString());
		int id = Integer.parseInt(idStr);
		// Pane List로부터 선택된 ID와 맞는 Pane으로부터 Content List를 얻어옴
		for(AdPackageDataObject pane : listPane){
			if(id == pane.getID()){
				ArrayList<String> contentList = pane.getContentsList();
				if(0 != ModifyList(contentList, tableContents.getSelectedRow(), pane.getAdPaneType())){
					System.err.println("파일 하나는 선택해야지");
					return ;
				}
				//paneContents.removeAll();
				//ArrayList<String> contentList = pane.getContentsList();
				//contentList.ldjkl
				while(tableContentModel.getRowCount()>0){
					tableContentModel.removeRow(0);
				}
				ContentInfo contentinfo = new ContentInfo();
				for(int j=0; j < contentList.size(); j++)
		        {
		        	contentinfo.setNo(j);
		        	contentinfo.setContentPath(contentList.get(j));
		        	tableContentModel.insertRow(tableContentModel.getRowCount(), contentinfo);
		        	//paneContents.add(strContent);
		        }
				contentinfo.setNo(-1);
	        	contentinfo.setContentPath("\\ ...");
	        	tableContentModel.insertRow(tableContentModel.getRowCount(), contentinfo);
				controlConfig.modifyPane(id, pane);
			}
		}
		tableCanvasProperty.setRowSelectionInterval(canvasSelected, canvasSelected);
	}
	private int ModifyList(ArrayList<String> listContent, int index, AdPaneType paneType){
		 //ArrayList<String> listContent = new ArrayList<String>();
		  
		String[] strFilter = null;
		 String strFilterDesc = null;
		 
		 if(paneType == AdPaneType.VIDEO){
			 strFilter = FileExtFilter.GetMovieExtFilter();
			 strFilterDesc = FileExtFilter.GetMovieExtDesc();
		 }
		 else{
			 strFilter = FileExtFilter.GetPictureExtFilter();
			 strFilterDesc = FileExtFilter.GetPictureExtDesc();
		 }
		JFileChooser fc = new JFileChooser();
		FileFilter fXml = new FileNameExtensionFilter(strFilterDesc, strFilter);
		
		fc.setFileFilter(fXml);
		if(choosePath != null)
			fc.setCurrentDirectory(choosePath);
		fc.setAcceptAllFileFilterUsed(false);
		int result = fc.showOpenDialog(this);
		
		// if we selected an image, load the image
		if(result == JFileChooser.APPROVE_OPTION) {
			choosePath = fc.getSelectedFile();
		    String strFile = fc.getSelectedFile().getPath();
		    listContent.set(index, strFile);
		    //listContent.add(strFile);
		}
		else{
			return -1;
		}
		
	   return 0;
	}

	private void GetContentList(int id){		
		// Content List를 Clear함
		while(tableContentModel.getRowCount()>0){
			tableContentModel.removeRow(0);
		}
		
		// Pane List로부터 선택된 ID와 맞는 Pane으로부터 Content List를 얻어옴
		for(AdPackageDataObject pane : listPane){
			if(id == pane.getID()){
				//paneContents.removeAll();
				ArrayList<String> contentList = pane.getContentsList();
				ContentInfo contentinfo = new ContentInfo();
				for(int j=0; j < contentList.size(); j++)
		        {
		        	contentinfo.setNo(j);
		        	contentinfo.setContentPath(contentList.get(j));
		        	tableContentModel.insertRow(tableContentModel.getRowCount(), contentinfo);
		        	//paneContents.add(strContent);
		        }
				contentinfo.setNo(-1);
	        	contentinfo.setContentPath("\\ ...");
	        	tableContentModel.insertRow(tableContentModel.getRowCount(), contentinfo);
			}
		}
	}
	
	private void AddEventListerner(){
		// Property Table에서 Delete Key 입력시 Row 삭제 
        tableCanvasProperty.addKeyListener(new KeyAdapter() {
        	@Override
        	public void keyPressed(KeyEvent e) {
        		int keyCode = e.getKeyCode();
        		PaneInfo paneInfo = null;
        		int id;

        		if(keyCode == KeyEvent.VK_DELETE){
        			int index = tableCanvasProperty.getSelectedRowCount();
        			if(index > 0)
        			{
	        			int aIndex[] = tableCanvasProperty.getSelectedRows();
	        			while(index > 0){
	        				index--;
	        				id = (int)tableCanvasProperty.getModel().getValueAt(aIndex[index], 0);
	        				//삭제할 ID 전달
	        				RemoveProperty(id);
	        			}
	        			//index = tableCanvasProperty.getSelectedRow();
	        			if( (aIndex[0] >= 0) && (aIndex[0] < (tableCanvasProperty.getRowCount()-1)) ){
	        				tableCanvasProperty.setRowSelectionInterval(aIndex[0], aIndex[0]);
	        			}
	        			
	        			if(aIndex[0] == tableCanvasProperty.getRowCount() ){
	        				tableCanvasProperty.setRowSelectionInterval(aIndex[0]-1, aIndex[0]-1);
	        			}
        			}
                 }
        		
        		super.keyPressed(e);
            }
        	
        	@Override
        	public void keyReleased(KeyEvent e) {
        		int keyCode = e.getKeyCode();
        		int id_key;
        		int selectedRow;
        		if( (keyCode == KeyEvent.VK_UP) || (keyCode == KeyEvent.VK_DOWN) ){
        			// Content List를 Clear함
        			selectedRow = tableCanvasProperty.getSelectedRow();
        			id_key = (int)tableCanvasProperty.getModel().getValueAt(selectedRow, 0);
        			
        			controlConfig.setSelectedPaneID(id_key);
        			GetContentList(id_key);
        			
//        			while(tableContentModel.getRowCount()>0){
//    					tableContentModel.removeRow(0);
//    				}
//        			
//        			// Pane List로부터 선택된 ID와 맞는 Pane으로부터 Content List를 얻어옴
//        			for(AdPackageDataObject pane : listPane){
//        				if(id_key == pane.getID()){
//        					ArrayList<String> contentList = pane.getContentsList();
//        					ContentInfo contentinfo = new ContentInfo();
//        					for(int j=0; j < contentList.size(); j++)
//        			        {
//        			        	contentinfo.setNo(j);
//        			        	contentinfo.setContentPath(contentList.get(j));
//        			        	tableContentModel.insertRow(tableContentModel.getRowCount(), contentinfo);
//        			        }
//        					contentinfo.setNo(-1);
//    			        	contentinfo.setContentPath("\\ ...");
//    			        	tableContentModel.insertRow(tableContentModel.getRowCount(), contentinfo);
//        				}
//        			}
                 }
        	}
		});
        
        tableContents.addKeyListener(new KeyAdapter() {
        	@Override
        	public void keyPressed(KeyEvent e) {
        		int keyCode = e.getKeyCode();
        		int id;

        		if(keyCode == KeyEvent.VK_DELETE){
        			int rowSelected = tableCanvasProperty.getSelectedRow();
        			int index = tableContents.getSelectedRowCount();
        			if(index > 0)
        			{
	        			int aIndex[] = tableContents.getSelectedRows();
	        			while(index > 0){
	        				index--;
	        				id = (int)tableCanvasProperty.getModel().getValueAt(rowSelected, 0);
	        				//삭제할 ID 전달
	        				RemoveContent(id, aIndex[index]);
	        			}
	        			if( (aIndex[0] >= 0) && (aIndex[0] < (tableContents.getRowCount()-1)) ){
	        				tableContents.setRowSelectionInterval(aIndex[0], aIndex[0]);
	        			}
	        			
	        			if(aIndex[0] == tableContents.getRowCount() ){
	        				tableContents.setRowSelectionInterval(aIndex[0]-1, aIndex[0]-1);
	        			}
        			}
        			tableCanvasProperty.getSelectionModel().setSelectionInterval(rowSelected, rowSelected);
                 }
        		
        		super.keyPressed(e);
            }
		});
        
        tableContents.addMouseListener(new MouseAdapter() {
        	@Override
        	public void mouseClicked(MouseEvent e) {
        		int clickCount = e.getClickCount();

        		// Table에 선택되면 선택된 Row에 해당하 Contents List 를 보여줌
        		if(clickCount == 2){
        			ModifyContent();	 
                 }
        		if(clickCount == 1){
        			AddContent();        				 
                 }
            }
		});
        
        tableCanvasProperty.addMouseListener(new MouseAdapter() {
        	@Override
        	public void mouseClicked(MouseEvent e) {
        		int clickCount = e.getClickCount();

        		// Table에 선택되면 선택된 Row에 해당하 Contents List 를 보여줌
        		if(clickCount == 1){
        			int id = (int)tableCanvasProperty.getModel().getValueAt(tableCanvasProperty.getSelectedRow(), 0);
        			
        			controlConfig.setSelectedPaneID(id);
        			GetContentList(id);
        		}
            }
		});
        
        tableCanvasProperty.addMouseListener(new MouseAdapter() {
        	@Override
        	public void mouseClicked(MouseEvent e) {
        		int clickCount = e.getClickCount();

        		if(clickCount == 2){
        			int selectedRow = tableCanvasProperty.getSelectedRow();
        			int id_key = (int)tableCanvasProperty.getModel().getValueAt(selectedRow, 0);
        			for(AdPackageDataObject pane : listPane){
        				if(id_key == pane.getID()){
        					modifyPaneDlg.setConfigControllIf(controlConfig);
        					modifyPaneDlg.setAdPane(id_key, pane);
                			modifyPaneDlg.setVisible(true);
        				}
        			} 
                 }
            }
		});
	}
	
	private void AddProperty(PaneInfo paneinfo){
		//paneinfo.setNo(numPane++);
		tablePropertyModel.insertRow(tablePropertyModel.getRowCount(), paneinfo);
	}
	
	private void RemoveProperty(int index){
		if( (index < 0) ){
			System.err.println("값이 이상해");
		}
		else{
			//tableModel.removeRow(index);
			controlConfig.deletePane(index);
		}
	}
	
	private void RemoveContent(int id, int index){
		if( (id < 0) ){
			System.err.println("값이 이상해");
		}
		else{
			for(AdPackageDataObject pane : listPane){
				if(id == pane.getID()){
					ArrayList<String> contentList = pane.getContentsList();
					contentList.remove(index);
					
					controlConfig.modifyPane(id, pane);
					tableContentModel.removeRow(index);
				}
			}
		}
	}
	
	private void ClearProperty(){
		int count = tableCanvasProperty.getRowCount();
		int index = 0;
		while(count > index){
			tablePropertyModel.removeRow(0);
			index++;
		}
		numPane = 0;
	}

	
	public void update(AdModelInterface adInterface){
		 listPane =  adInterface.getData();
		 PaneInfo paneinfo = new PaneInfo("Static", 0, 0, 0, 0, 0, 0);
		 int selectedId = adInterface.getSelectedPaneID();
		 int selectedRow = 0;
		 int currentId;
		 int i = 0;
		 
		 ClearProperty();
		 listChild.clear();
		 
		 for(AdPackageDataObject pane : listPane){
			 AdPaneType type = pane.getAdPaneType();
			 currentId = pane.getID();
			 if(currentId == selectedId){
				 selectedRow = i;
			 }
			 i++;

			 paneinfo.setNo(currentId);
			 paneinfo.setSizeHeight((int)pane.getRectangle().getHeight());
			 paneinfo.setSizeWidth((int)pane.getRectangle().getWidth());
			 paneinfo.setPosX((int)pane.getRectangle().getX());
			 paneinfo.setPosY((int)pane.getRectangle().getY());
			 paneinfo.setZOrder(pane.getzOrder());
			 paneinfo.setInterval(pane.getInterval());
			 if(type == AdPaneType.PAGING){
				 paneinfo.setPaneType("PAGING");
			 }
			 else if(type == AdPaneType.STATIC){
				 paneinfo.setPaneType("STATIC");
			 }
			 else if(type == AdPaneType.SCROLLING){
				 if(pane.getScrollType() == ScrollType.VERTICAL){
					 paneinfo.setPaneType("V_Scroll");
				 }
				 else if(pane.getScrollType() == ScrollType.HORIZONTAL){
					 paneinfo.setPaneType("H_Scroll");
				 }
				 else
				 {
					 System.err.println("잘못된 Scroll Type");
				 }
			 }
			 else if(type == AdPaneType.VIDEO){
				 paneinfo.setPaneType("VIDEO");
			 }
			 else{
				 continue;
			 }
			 AddProperty(paneinfo);
			 
			 //paneContents
		 }
		 tableCanvasProperty.setRowSelectionInterval(selectedRow, selectedRow);
		 int id = (int)tableCanvasProperty.getModel().getValueAt(tableCanvasProperty.getSelectedRow(), 0);
		 GetContentList(id);
	 }
		
	public PaneProperty() {
		setLayout(new BorderLayout());
		setBackground(Color.WHITE);
		scrollPane = createPropertyTable();
		scrollContents = createContentsTable();
		
		AddEventListerner();
		
//		add(titleLabel, BorderLayout.NORTH);
//		add(scrollPane, BorderLayout.WEST);
//		add(scrollContents, BorderLayout.EAST);
		
		//add(titleLabel);
		add(scrollPane);
		add(scrollContents);
		
//		scrollPane.setBounds(0, (int)titleLabel.getSize().getHeight(), 950, 250);
//		scrollContents.setBounds(950, (int)titleLabel.getSize().getHeight(), 300, 250);
		scrollPane.setBounds(0, 0, 950, 250);
		scrollContents.setBounds(950, 0, 300, 250);
		add(new JPanel());
		
		modifyPaneDlg = new ModifyPaneDialog();
		modifyPaneDlg.setVisible(false);
		
	}

}
