package marquee.config.view;

import java.awt.Component;
import java.awt.Graphics;
import java.awt.Point;
import java.awt.Rectangle;
import java.util.ArrayList;

import marquee.adlib.ads.AdPackageDataObject;

public abstract class IDrawable{
	
	protected AdPackageDataObject AdDO = null;
	public enum  StateDraw { IDLE, DRAGGING, RESIZING_T, RESIZING_B, RESIZING_L, RESIZING_R,
							RESIZING_LT, RESIZING_RT, RESIZING_LB, RESIZING_RB}
	
	protected StateDraw _state = StateDraw.IDLE;
	
	protected boolean selected = false;
	
	//============================================================== Whole Area
	protected Rectangle newRect = new Rectangle();
	protected Rectangle nowRect = new Rectangle();
	
	//============================================================== Resizing Area
	protected Rectangle rectLT = null;
	protected Rectangle rectRT = null;
	protected Rectangle rectLB = null;
	protected Rectangle rectRB = null;
	protected Rectangle rectT = null;
	protected Rectangle rectB = null;
	protected Rectangle rectL = null;
	protected Rectangle rectR = null;
	//============================================================== Drawing Interface
	public abstract void draw(Component c, Graphics g);
	public abstract void setContentsList(ArrayList<String> listCon);
	
	//============================================================== Setting Data Interface
	protected void setAdPaneData(AdPackageDataObject adDO){
		this.AdDO = adDO;
		Rectangle rect = adDO.getRectangle();
		newRect.x = rect.x;
		newRect.y = rect.y;
		newRect.width = rect.width;
		newRect.height = rect.height;
		setResizingRect();
		ArrayList<String> listContents = AdDO.getContentsList();
		setContentsList(listContents);
	}
	
	protected void setResizingRect(){
		// 네 모서리 설정
		rectLT = new Rectangle(newRect.x, newRect.y, 4,4);
		rectRT = new Rectangle(newRect.x+newRect.width-4, newRect.y, 4,4);
		rectLB = new Rectangle(newRect.x, newRect.y+newRect.height-4, 4,4);
		rectRB = new Rectangle(newRect.x+newRect.width-4, newRect.y+newRect.height-4, 4,4);
		// 
		rectT = new Rectangle(newRect.x+4, newRect.y, newRect.x+newRect.width-4,4);
		rectB = new Rectangle(newRect.x+4, newRect.y+newRect.height-4, newRect.x+newRect.width-4,4);
		rectL = new Rectangle(newRect.x, newRect.y+4, 4,newRect.height-8);
		rectR = new Rectangle(newRect.x+newRect.width-4, newRect.y+4, 4,newRect.height-8);
	}
	
	public int getID(){
		return AdDO.getID();
	}
	//============================================================== Checking contains Interface
	public boolean contains(Point pt){
		Rectangle rect = AdDO.getRectangle();
		
		if(rect.contains(pt))
			return true;
		else
			return false;
	}

	//============================================================== Checking intersection Interface
	public boolean intersections(Rectangle rect){
		if(newRect.intersects(rect))
			return true;
		else
			return false;
	}
	
	//============================================================== Moving, Resizing Check From Point
	public StateDraw checkState(Point pt){
		StateDraw checkState = StateDraw.IDLE;
		
		if(rectLT.contains(pt))
			checkState = StateDraw.RESIZING_LT;
		else if(rectRT.contains(pt))
			checkState = StateDraw.RESIZING_RT;
		else if(rectLB.contains(pt))
			checkState = StateDraw.RESIZING_LB;
		else if(rectRB.contains(pt))
			checkState = StateDraw.RESIZING_RB;
		else if(rectT.contains(pt))
			checkState = StateDraw.RESIZING_T;
		else if(rectB.contains(pt))
			checkState = StateDraw.RESIZING_B;
		else if(rectL.contains(pt))
			checkState = StateDraw.RESIZING_L;
		else if(rectR.contains(pt))
			checkState = StateDraw.RESIZING_R;
		else if(newRect.contains(pt))
			checkState = StateDraw.DRAGGING;
		
		return checkState;
	}
	
	//============================================================== 드래깅 중 x,y 변화값 입력 
	public void mouseDragged(Rectangle rect){
		newRect = rect;
	}
	
	//============================================================== 마우스 놓을때  x,y 변화값 입력
	public void mouseReleased(int dx, int dy){
		newRect.x += dx;
		newRect.y += dy;
		//TODO Controller를 거쳐가야 하는데.. 일단 바로접근하자...
		AdDO.setRectangle(newRect);
		//setResizingRect();
		
		ConfigMainFrame.getControl().modifyPane(AdDO.getID(), AdDO );
		ConfigMainFrame.getControl().setSelectedPaneID(AdDO.getID());
	}

	//============================================================== Range Check
	public Rectangle checkRange(int dx, int dy){
		Rectangle tmpRect = new Rectangle(newRect);
		if(_state == StateDraw.DRAGGING){
			tmpRect.x += dx;
			tmpRect.y += dy;
			checkValidDragging(tmpRect);
		}
		else if(_state == StateDraw.RESIZING_T){
			if(checkValidT(tmpRect,dy)){
				tmpRect.y += dy;
				tmpRect.height -= dy;
				if(tmpRect.height < 20)
					tmpRect.height = 20;
			}
		}
		else if(_state == StateDraw.RESIZING_B){
			if(checkValidB(tmpRect,dy)){
				tmpRect.height += dy;
				if(tmpRect.height < 20)
					tmpRect.height = 20;
			}
		}
		else if(_state == StateDraw.RESIZING_L){
			if(checkValidL(tmpRect,dx)){
				tmpRect.x += dx;
				tmpRect.width -= dx;
				if(tmpRect.width < 20)
					tmpRect.width = 20;
			}
		}		
		else if(_state == StateDraw.RESIZING_R){
			if(checkValidR(tmpRect,dx)){
				tmpRect.width += dx;
				if(tmpRect.width < 20)
					tmpRect.width = 20;
			}
		}
		else if(_state == StateDraw.RESIZING_LT){
			if(checkValidL(tmpRect,dx)){
				tmpRect.x += dx;
				tmpRect.width -= dx;
				if(tmpRect.width < 20)
					tmpRect.width = 20;
			}
			if(checkValidT(tmpRect,dy)){
				tmpRect.y += dy;
				tmpRect.height -= dy;
				if(tmpRect.height < 20)
					tmpRect.height = 20;
			}
		}
		else if(_state == StateDraw.RESIZING_RT){
			if(checkValidR(tmpRect,dx)){
				tmpRect.width += dx;
				if(tmpRect.width < 20)
					tmpRect.width = 20;
			}
			
			if(checkValidT(tmpRect,dy)){
				tmpRect.y += dy;
				tmpRect.height -= dy;
				if(tmpRect.height < 20)
					tmpRect.height = 20;
			}
		}
		else if(_state == StateDraw.RESIZING_LB){
			if(checkValidL(tmpRect,dx)){
				tmpRect.x += dx;
				tmpRect.width -= dx;
				if(tmpRect.width < 20)
					tmpRect.width = 20;
			}
			if(checkValidB(tmpRect,dy)){
				tmpRect.height +=dy;
				if(tmpRect.height < 20)
					tmpRect.height = 20;
			}
		}
		else if(_state == StateDraw.RESIZING_RB){
			if(checkValidR(tmpRect,dx)){
				tmpRect.width += dx;
				if(tmpRect.width < 20)
					tmpRect.width = 20;
			}
			if(checkValidB(tmpRect,dy)){
				tmpRect.height +=dy;
				if(tmpRect.height < 20)
					tmpRect.height = 20;
			}
		}
		
		return tmpRect;
	}

	//============================================================== Select Item check getter/setter
	protected void setState(StateDraw stateD){
		_state = stateD;
	}
	public boolean isSelected(){
		return selected;
	}
	public void setSelected(boolean select){
		selected = select;
	}
	
	public void setSelectePanedId(){
		ConfigMainFrame.getControl().setSelectedPaneID(AdDO.getID());
	}
	
	private void checkValidDragging(Rectangle rect){
		if(rect.x <0)
			rect.x = 0;
		else if(rect.x+rect.width > ConfigMainFrame.widthCanvas)
			rect.x = ConfigMainFrame.widthCanvas - rect.width;
		
		if(rect.y <0)
			rect.y = 0;
		else if(rect.y+rect.height > ConfigMainFrame.heightCanvas)
			rect.y = ConfigMainFrame.heightCanvas - rect.height;
	}
	
	private boolean checkValidT(Rectangle rect, int dy){
		if(rect.y + dy < 0 )
			return false;
		else if(rect.y + dy > rect.y+rect.height - dy)
			return false;
		return true;
	}

	private boolean checkValidB(Rectangle rect, int dy){
		if(rect.y+rect.height + dy > ConfigMainFrame.heightCanvas){
			rect.height = ConfigMainFrame.heightCanvas - rect.y;
			return false;
		}
		else if(rect.y > rect.y+rect.height + dy){
			return false;
		}
		return true;
	}

	private boolean checkValidL(Rectangle rect, int dx){
		if(rect.x + dx < 0 )
			return false;
		else if(rect.x + dx > rect.x+rect.width - dx)
			return false;
		return true;
	}
	
	private boolean checkValidR(Rectangle rect, int dx){
		if(rect.x+rect.width + dx > ConfigMainFrame.widthCanvas){
			rect.width = ConfigMainFrame.widthCanvas - rect.x;
			return false;
		}
		else if(rect.x > rect.x + rect.width + dx )
		{
			return false;
		}
		return true;
	}
}
