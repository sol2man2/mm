package marquee.wathdog.launcher;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;
import java.net.UnknownHostException;

public class commHeartBeat extends Thread {
	
	private DatagramSocket socket = null;
	private String strHeartBeat = new String("Marquee Heartbeat");
	private boolean heartBeat = true;
	private int countAlive = 0;
	
	public commHeartBeat() {
		super();
		
		// DatagramPacket을 받기 위한 Socket 생성
		// 9999 : Listen할 Port
		try {
			try {
				socket = new DatagramSocket(8888, InetAddress.getByName("127.0.0.1"));
			} catch (UnknownHostException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		} catch (SocketException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	public boolean getHeartBeat(){
		return heartBeat;
	}

	public void run() {
		while (true)
		{
			try
			{
				// 데이터를 받을 버퍼
				byte[] inbuf = new byte[256];

				// 데이터를 받을 Packet 생성
				DatagramPacket packet = new DatagramPacket(inbuf, inbuf.length);

				// 데이터 수신
				// 데이터가 수신될 때까지 대기됨
				socket.receive(packet);

				// 수신된 데이터 출력
				String str = new String(packet.getData(), 0, packet.getLength()); 
				System.out.println("received length : " + packet.getLength() + ", received data : "
						+ str);
				
				if(strHeartBeat.equals(str)){
					heartBeat = true;
					countAlive = 0;
					System.out.println("heartBeat True!!!");
				}
				
			} catch (IOException e)
			{
				e.printStackTrace();
			}
			
			try {
				Thread.sleep(500);
				countAlive++;
				
				if(countAlive >= 6){
					System.out.println("countAlive >= 6");
					heartBeat = false;
					countAlive = 0;
				}

			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

}
