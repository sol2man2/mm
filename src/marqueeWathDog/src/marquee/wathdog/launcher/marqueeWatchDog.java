package marquee.wathdog.launcher;

import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.util.*;
import sun.jvmstat.monitor.*;

public class marqueeWatchDog {
	
	public static String nameMarquee = new String("marquee.jar");
	Process marqueeProc = null;
	
	public static void main(String[] args) {
		
		Process marqueeProc = null;
		marqueeWatchDog watchDog = new marqueeWatchDog();
		
		commHeartBeat checkBeatComm = new commHeartBeat();
		checkBeatComm.start();
		
		while(true){
			System.out.println("Check Download Status");
			// Download 중인지 확인한다. Download 중이 아니면...
			if(watchDog.checkNowDownload() == false){
				
				System.out.println("---------------------");
				System.out.println("Check Download Status - FALSE !!!");
				System.out.println("---------------------");
				System.out.println("Check Marquee Process !!");
				
				if(watchDog.checkMarqueeAlive() == false){
					// Marquee가 Process List에 없으면...
					System.out.println("Check Marquee Process - Killed!!");
					watchDog.restartMarquee();
				}
				else{
					// Marquee가 Process List에 있으면...
					System.out.println("Check Marquee Process - Alive !!");
					// dealock check
					if(checkBeatComm.getHeartBeat() == false){
						// Dead Lock 이면... Process 죽였다가 살린다.
						watchDog.restartProcess();
					}
					else{
						// Dead Lock 이 아니면.. 아무것도 안한다.
					}
				}
			}
			else{
				System.out.println("---------------------");
				System.out.println("Check Download Status - TRUE !!!");
				System.out.println("---------------------");
			}
			
			//1초 주기로 Loop를 돌면서 Marquee 가 살아 있는지 확인한다.
			try {
				Thread.sleep(1000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
	
	private boolean checkMarqueeAlive(){
		MonitoredHost local = null;
		
		try {
			local = MonitoredHost.getMonitoredHost("localhost");
		} catch (MonitorException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (URISyntaxException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		Set<Integer> vmlist = null;
		try {
			vmlist = new HashSet<Integer>(local.activeVms());
		} catch (MonitorException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		for(int id : vmlist){
			MonitoredVm vm = null;
			try {
				vm = local.getMonitoredVm(new VmIdentifier("//" + id));
			} catch (MonitorException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (URISyntaxException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			String processname = null;
			try {
				processname = MonitoredVmUtil.mainClass(vm, true);
			} catch (MonitorException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			System.out.println(id + " [" + processname+"]");
			if(nameMarquee.equals(processname)){
				return true;
			}
		  }
		return false;
	}

	private boolean checkNowDownload(){
		File flagFile = new File(".\\download.txt");
		if(flagFile.exists()){
			return true;
		}
		else{
			return false;
		}
	}
	 
	private boolean restartMarquee(){
		ProcessBuilder pb = new ProcessBuilder("java", "-jar", "marquee.jar");
		pb.directory(new File(".\\"));
		try {
			marqueeProc = pb.start();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return false;
		}
		return true;
	}
	
	private void restartProcess(){
		if(marqueeProc != null){
			marqueeProc.destroy();
			
			restartMarquee();
		}
	}
}
