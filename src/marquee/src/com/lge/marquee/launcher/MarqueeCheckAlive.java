package com.lge.marquee.launcher;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;
import java.net.UnknownHostException;
import java.util.Date;
import java.util.Timer;
import java.util.TimerTask;

public class MarqueeCheckAlive {
	Timer timer = null;
	CommTask task = null;

	public MarqueeCheckAlive(){
		timer = new Timer();
		task = new CommTask();
		
//		timer.schedule(task, new Date(), 1000);
	}
	
	public synchronized void stop(){
		if(timer != null)
			timer.cancel();
	}
	
	public synchronized void start(){
		timer = new Timer();
		if(task == null)
			task = new CommTask();
		
		timer.schedule(task, new Date(), 1000);
	}
}

class CommTask extends TimerTask{
	public void run(){
//		System.out.println("Per 1sec - Entry TimerTask ");
		
		// 전송할 데이터 : 현재 시간
		String data = "Marquee Heartbeat";

		// 전송할  DatagramPacket 생성
		DatagramPacket packet = null;
		try {
			packet = new DatagramPacket(data.getBytes(), data.getBytes().length, InetAddress
					.getByName("127.0.0.1"), 8888);
		} catch (UnknownHostException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		// DatagramSocket 생성
		DatagramSocket socket = null;
		try {
			socket = new DatagramSocket();
		} catch (SocketException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		// DatagramPacket 전송
		if(socket != null && packet != null){
			try {
				socket.send(packet);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
}
