package com.lge.marquee.launcher;

import com.lge.marquee.adplayer.AdPackageUpdater;
import com.lge.marquee.adplayer.AdPlayerModel;
import com.lge.marquee.updater.MarqueeUpdater;


public class MobileMarqueeMain {

	// main function of mobile marquee application.
	public static void main(String[] args) {
		
		System.setProperty("jna.library.path", "C:\\Program Files\\OSSBuild\\GStreamer\\v0.10.6\\bin");
		System.setProperty("apple.awt.graphics.UseQuartz", "false");
		
		AdPlayerModel adPlayerModel = new AdPlayerModel();
		AdPackageUpdater adUpdator = new AdPackageUpdater(adPlayerModel);
		
		new MarqueeUpdater(adUpdator);
		
		MarqueeCheckAlive marHeartbeat = new MarqueeCheckAlive();
		marHeartbeat.start();
	}
}