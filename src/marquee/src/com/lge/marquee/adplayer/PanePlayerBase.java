package com.lge.marquee.adplayer;

import java.awt.Dimension;
import java.util.ArrayList;

import javax.swing.JLayeredPane;

import marquee.adlib.ads.ScrollType;

public class PanePlayerBase extends JLayeredPane implements Runnable {
	public int x;
	public int y;
	public int cnt;
	public int interval;
	public ScrollType scrollType;
	public boolean shouldbeStop;
	public ArrayList<String> imgList;
	public Dimension di;
	
	public PanePlayerBase() {
		super();
	}
	
	public void initPlayer(int x, int y, Dimension di, int interval, ScrollType scrollType, ArrayList<String> imgList) {		
		this.x = x;
		this.y = y;
		this.di = di;
		this.interval = interval;
		this.scrollType = scrollType;
		this.imgList = imgList;
		this.setLocation(x, y);
		this.setLayout(null);
		//this.setPreferredSize(di);
		this.setSize(di);
		this.setDoubleBuffered(true);
		this.setVisible(true);
		this.cnt = imgList.size();
	}
	
	public void startPlayer(PanePlayerBase player) {
		Thread th = new Thread(player);
		
		if (player instanceof StaticPanePlayer) {
			th.setPriority(Thread.MIN_PRIORITY);
		} else if (player instanceof PagingPanePlayer) {
			th.setPriority(Thread.NORM_PRIORITY);
		} else if (player instanceof ScrollingPanePlayer) {
			th.setPriority(Thread.MAX_PRIORITY);
		} else if (player instanceof VideoPanePlayer) {
			th.setPriority(Thread.NORM_PRIORITY);
		}
		
		th.start();
	}
	
	public void stopPlayer() {
		this.shouldbeStop = true;
	}
	
	public boolean checkStop() {
		if (shouldbeStop == true) {
			return true;
		} else {
			return false;
		}
	}
	
	public void run() {
		
	}
}
