package com.lge.marquee.adplayer;


import java.awt.Dimension;
import java.io.File;
import java.util.ArrayList;

import marquee.adlib.ads.ScrollType;

import org.gstreamer.Bus;
import org.gstreamer.ElementFactory;
import org.gstreamer.Gst;
import org.gstreamer.GstObject;
import org.gstreamer.State;
import org.gstreamer.TagList;
import org.gstreamer.elements.PlayBin2;
import org.gstreamer.swing.VideoComponent;


public class VideoPanePlayer extends PanePlayerBase {
	private int videoIdx = 0;
	private PlayBin2 playbin;
	VideoComponent videoComponent;
	
	// constructor
	VideoPanePlayer(int x, int y, Dimension di, int interval, ScrollType scrollType, ArrayList<String> imgList) {
		super();
		initPlayer(x, y, di, interval, scrollType, imgList);
		createVideoPlayer();
		progressBus();
	}
		
	public void run() {
		while (true) {
			startVideoPlayer();
			stopVideoPlayer();
			if (checkStop() == true) {
				//System.out.println("stop!!!!video");
				this.remove(videoComponent);
				this.setVisible(false);
				playbin.setVideoSink(ElementFactory.make("fakesink", "videosink"));
				//Gst.deinit();
				return;
			}
			checkCount();
		}
	}
	
	private void createVideoPlayer() {
		Gst.init();
		
		playbin = new PlayBin2("VideoPlayer");
		videoComponent = new VideoComponent();
		playbin.setVideoSink(videoComponent.getElement());
		this.add(videoComponent);
		videoComponent.setBounds(0, 0, di.width, di.height);
	}

	private void startVideoPlayer() {		
		playbin.setInputFile(new File(imgList.get(videoIdx)));
        playbin.play();
        
        Gst.main();
	}
	
	private void stopVideoPlayer() {
		playbin.stop();
	}

	private void progressBus() {
	    // Listen for end-of-stream (i.e. when the media has finished)
	    playbin.getBus().connect(new Bus.EOS() {

	        public void endOfStream(GstObject source) {
	            System.out.println("Finished playing file");
	            Gst.quit();
	        }
	    });
	    
	    // Listen for any error conditions
	    playbin.getBus().connect(new Bus.ERROR() {
	        public void errorMessage(GstObject source, int code, String message) {
	            System.out.println("Error occurred: " + message);
	            Gst.quit();
	        }
	    });
	    
	    // Listen for metadata (tags)
	    playbin.getBus().connect(new Bus.TAG() {

	        public void tagsFound(GstObject source, TagList tagList) {
	            for (String tagName : tagList.getTagNames()) {
	                // Each tag can have multiple values, so print them all.
	                for (Object tagData : tagList.getValues(tagName)) {
	                    System.out.printf("[%s]=%s\n", tagName, tagData);
	                    if (tagData.equals("Intel Video 3")) {
	                    	Gst.quit();
	                    }
	                }
	            }
	        }
	    });
	    
	    // Listen for state-changed messages
	    playbin.getBus().connect(new Bus.STATE_CHANGED() {

	        public void stateChanged(GstObject source, State old, State current, State pending) {
	            if (source == playbin) {
	                System.out.println("Pipeline state changed from " + old + " to " + current);
	            }
	        }
	    });
	}
	
	private void checkCount() {
		// check overflow of videoCnt.
		if (cnt > 1) {
			if (cnt - 1 > videoIdx) {
				videoIdx++;
			} else {
				videoIdx = 0;
			}
		}
	}
	
	public void stopPlayer() {
		this.shouldbeStop = true;
		Gst.quit();
	}
}
