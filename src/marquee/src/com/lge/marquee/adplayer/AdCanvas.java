package com.lge.marquee.adplayer;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.GraphicsDevice;
import java.awt.GraphicsEnvironment;
import java.awt.Rectangle;
import java.awt.Toolkit;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.ArrayList;

import javax.swing.JFrame;
import javax.swing.JLayeredPane;
import javax.swing.JPanel;

import marquee.adlib.ads.AdModelInterface;
import marquee.adlib.ads.AdPackageDataObject;
import marquee.adlib.ads.AdPaneType;
import marquee.adlib.ads.ScrollType;
import marquee.adlib.utils.AdUpdateListener;
import marquee.adlib.utils.IObserver;
import marquee.adlib.utils.MarqueeResourcePath;

public class AdCanvas extends JFrame implements KeyListener, IObserver, AdUpdateListener {

	private AdPlayerModel model;
	private JLayeredPane layeredPane;
	private JPanel bgPanel;
	private ArrayList<PanePlayerBase> playerList;
	final private int CFGTOOL_WIDTH = 960;
	final private int CFGTOOL_HEIGHT = 540;
	private float widthScaleRatio = 0;
	private float heightScaleRatio = 0;
	private int screen_width;
	private int screen_height;
	private int maxZoder = 0;
	private Dimension di; 
	
	private ArrayList<AdPackageDataObject> listPane;
	
	public AdCanvas(AdModelInterface pkgData, AdPlayerModel model) {
		super("AdPlayer");
		
		this.model = model;	
		this.addKeyListener(this);
		this.setLayout(null);
		this.setBackground(Color.BLACK);
		
//		screen_width = (int)Toolkit.getDefaultToolkit().getScreenSize().getWidth();
//		screen_height = (int)Toolkit.getDefaultToolkit().getScreenSize().getHeight();		
//		di = new Dimension(screen_width, screen_height);
//		
//		this.setLocation(0,0);
//		this.setPreferredSize(di);
//		this.setSize(di);
		
		setFullScreen();
		
		widthScaleRatio = (float)((float)screen_width/(float)CFGTOOL_WIDTH);
		heightScaleRatio = (float)((float)screen_height/(float)CFGTOOL_HEIGHT);
		
	    layeredPane = new JLayeredPane();
		layeredPane.setPreferredSize(this.getSize());
		layeredPane.setSize(this.getSize());
	    layeredPane.setOpaque(true);
	    layeredPane.setDoubleBuffered(true);
		this.setContentPane(layeredPane);	
		
		bgPanel = new JPanel();
		bgPanel.setLocation(0, 0);
		bgPanel.setPreferredSize(this.getSize());
		bgPanel.setSize(this.getSize());
		bgPanel.setBackground(Color.BLACK);
		bgPanel.setOpaque(true);
		layeredPane.add(bgPanel);
	    
		// register view as observer.
		pkgData.registerObserver(this);
		pkgData.registerAdUpdateMngr(this);
	}
	
	// set frame as full screen
	private void setFullScreen() {
		GraphicsDevice gd;
		GraphicsEnvironment ge;
			    
		screen_width = (int)Toolkit.getDefaultToolkit().getScreenSize().getWidth();
		screen_height = (int)Toolkit.getDefaultToolkit().getScreenSize().getHeight();
		ge = GraphicsEnvironment.getLocalGraphicsEnvironment();
		gd = ge.getDefaultScreenDevice();
		
		gd.setFullScreenWindow(this);
		this.setSize(screen_width, screen_height);
	}

	@Override
	public void keyTyped(KeyEvent e) {
		// TODO Auto-generated method stub
		char c = e.getKeyChar();
		
		// close marquee if escape key is pressed.
		if (c == KeyEvent.VK_ESCAPE) {
			System.exit(0);
		}
	}

	@Override
	public void keyPressed(KeyEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void keyReleased(KeyEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void update() {
		createPaneList();
		startPaneList();
		showPaneList();
	}

	@Override
	public void onUpdate() {
		stopPanePlayer();
		try {
			Thread.sleep(2000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	void createPaneList() {
		AdPackageDataObject adPane;
		Rectangle rect;
		Dimension di;
		ArrayList<String> contentList;
		ScrollType scrollType;
		PanePlayerBase panePlayer = null;
		playerList = new ArrayList<PanePlayerBase>();
		int zOrder, interval;
		int scaledX, scaledY, scaledWidth, scaledHeight;
		
		listPane = model.getPaneList();
		
		for (int i = 0; i < listPane.size(); i++) {
			adPane = listPane.get(i);
			zOrder = adPane.getzOrder();
			interval = adPane.getInterval();
			scrollType = adPane.getScrollType();
			rect = adPane.getRectangle();
			scaledX = (int)(rect.x*widthScaleRatio);
			scaledY = (int)(rect.y*heightScaleRatio);
			scaledWidth = (int)(rect.width*widthScaleRatio);
			scaledHeight = (int)(rect.height*heightScaleRatio);
			di = new Dimension(scaledWidth, scaledHeight);
			contentList = adPane.getContentsList();
			
			for(int j = 0; j < contentList.size(); j++) {
				String str = MarqueeResourcePath.getInstance().getAdPkgCurrentPath() + "/" + contentList.get(j);
				contentList.set(j, str);
			}
			
			if (adPane.getAdPaneType() == AdPaneType.STATIC) {
				panePlayer = new StaticPanePlayer(scaledX, scaledY, di, interval, scrollType, contentList);
			} else if (adPane.getAdPaneType() == AdPaneType.PAGING) {				
				panePlayer = new PagingPanePlayer(scaledX, scaledY, di, interval, scrollType, contentList);
			} else if (adPane.getAdPaneType() == AdPaneType.SCROLLING) {
				panePlayer = new ScrollingPanePlayer(scaledX, scaledY, di, interval, scrollType, contentList);
			} else if (adPane.getAdPaneType() == AdPaneType.VIDEO) {
				panePlayer = new VideoPanePlayer(scaledX, scaledY, di, interval, scrollType, contentList);
			}
			
			layeredPane.add(panePlayer);
			layeredPane.setLayer(panePlayer, zOrder+1);
			playerList.add(panePlayer);
			
			if (zOrder+1 > maxZoder)
				maxZoder = zOrder+1;
		}
	}
	
	void startPaneList() {
		PanePlayerBase player;
		for (int i = 0; i < playerList.size(); i++) {
			player = playerList.get(i);
			player.startPlayer(player);
		}
	}
	
	void showPaneList() {
		layeredPane.validate();
		layeredPane.setLayer(bgPanel, 0);
		this.pack();
	    this.setVisible(true);
	}
	
	public void stopPanePlayer() {
		PanePlayerBase player;
		
		layeredPane.setLayer(bgPanel, maxZoder+1);
		
		if (playerList != null) {		
			for (int i = 0; i < playerList.size(); i++) {
				player = playerList.get(i);
				player.stopPlayer();
				try {
					Thread.sleep(100);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				layeredPane.remove(player);
			}
		}
		
		layeredPane.invalidate();
	}
}
