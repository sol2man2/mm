package com.lge.marquee.adplayer;

import java.awt.Dimension;
import java.awt.Image;
import java.util.ArrayList;

import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JPanel;

import marquee.adlib.ads.ScrollType;

public class ScrollingPanePlayer extends PanePlayerBase {
	private int scrollPosition = 0;
	private int scrollWidthSize = 0;
	private int scrollHeightSize = 0;
	private float scaleRatio = 0;
	private int scaledWidth = 0;
	private int scaledHeight = 0;
	private JPanel panel;
	
	public ScrollingPanePlayer(int x, int y, Dimension di, int interval, ScrollType scrollType, ArrayList<String> imgList) {
		super();
		initPlayer(x, y, di, interval, scrollType, imgList);
		createPane();
	}
	
	public void run() {	    
		while(true) {
			showPane();
			if (checkStop() == true) {
				//System.out.println("stop!!!!scrolling");
				stopPane();
				return;
			}	
		}
	}
	
	void createPane() {
		panel = new JPanel();
		if (scrollType == ScrollType.HORIZONTAL) {
			scrollPosition = di.width;
			panel.setLayout(new BoxLayout(panel, BoxLayout.X_AXIS));
		} else if (scrollType == ScrollType.VERTICAL) {
			scrollPosition = di.height;
			panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));
		}
		
		for (int i = 0; i < cnt; i++) {
			System.out.println("img" + i + "=" + imgList.get(i));
			ImageIcon staticImage = new ImageIcon(imgList.get(i));
			Image image = staticImage.getImage();
			Image scaledImage = null;
			
			if (scrollType == ScrollType.HORIZONTAL) {
				scaleRatio = (float)((float)di.height/(float)staticImage.getIconHeight());
				scaledWidth = (int)(staticImage.getIconWidth()*scaleRatio);
				scaledImage = image.getScaledInstance(scaledWidth, di.height, Image.SCALE_FAST);
				scrollWidthSize += scaledWidth;
			} else if (scrollType == ScrollType.VERTICAL) {
				scaleRatio = (float)((float)di.width/(float)staticImage.getIconWidth());
				scaledHeight = (int)(staticImage.getIconHeight()*scaleRatio);
				scaledImage = image.getScaledInstance(di.width, scaledHeight, Image.SCALE_FAST);
				scrollHeightSize += scaledHeight;
			}
			
			ImageIcon newStaticImage = new ImageIcon(scaledImage);
			JLabel imageLabel = new JLabel(newStaticImage);
			panel.add(imageLabel);			
		}
		
	    this.add(panel); 
	}
	
	void showPane() {
		scrollPosition -= 1;
		if (scrollType == ScrollType.HORIZONTAL) {
			panel.setBounds(scrollPosition, 0, scrollWidthSize, di.height);		
			//System.out.println("position : " + " " + position);
			if (scrollPosition == -scrollWidthSize) {
				scrollPosition = di.width;
			}
		} else if (scrollType == ScrollType.VERTICAL) {
			panel.setBounds(0, scrollPosition, di.width, scrollHeightSize);		
			//System.out.println("position : " + " " + position);
			if (scrollPosition == -scrollHeightSize) {
				scrollPosition = di.height;
			}
		}

		try {
			Thread.sleep(interval);
		} catch(InterruptedException e) {
			e.printStackTrace();
		}
	}
	
	void stopPane() {
		this.remove(panel);
		this.setVisible(false);
	}
}
