package com.lge.marquee.adplayer;

import java.awt.Dimension;
import java.awt.Image;
import java.util.ArrayList;

import javax.swing.ImageIcon;
import javax.swing.JLabel;

import marquee.adlib.ads.ScrollType;

public class StaticPanePlayer extends PanePlayerBase {
	ImageIcon staticImage;
    JLabel imageLabel;
	
	public StaticPanePlayer(int x, int y, Dimension di, int interval, ScrollType scrollType, ArrayList<String> imgList) {
		super();
		initPlayer(x, y, di, interval, scrollType, imgList);
		createPane();
	}
	
	public void run() {
		showPane();
		while(true) {
			if (checkStop() == true) {
				//System.out.println("stop!!!!static");
				stopPane();
				return;
			}	
			try {
				Thread.sleep(200);
			} catch(InterruptedException e) {
				e.printStackTrace();
			}
		}
	}
	
	void createPane() {
		for (int i = 0; i < cnt; i++) {
			staticImage = new ImageIcon(imgList.get(i));
		    Image image = staticImage.getImage();
		    Image scaledImage = image.getScaledInstance(di.width, di.height, Image.SCALE_FAST);
		    ImageIcon newStaticImage = new ImageIcon(scaledImage);
		    imageLabel = new JLabel(newStaticImage);
		    this.add(imageLabel);
		}
	}
	
	void showPane() {	    
	    imageLabel.setBounds(0, 0, di.width, di.height);
	}
	
	void stopPane() {
		this.remove(imageLabel);
		this.setVisible(false);
	}
}

