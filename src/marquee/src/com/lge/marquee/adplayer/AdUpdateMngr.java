package com.lge.marquee.adplayer;

import marquee.adlib.ads.AdModelInterface;


public class AdUpdateMngr {

	AdPlayerModel playerModel;
	AdCanvas mainCanvas;
	
	// Constructor
	public AdUpdateMngr(AdPlayerModel adModel, AdModelInterface pkgData) {
		
		playerModel = adModel;
		mainCanvas = new AdCanvas(pkgData, playerModel);

	}
	
}
