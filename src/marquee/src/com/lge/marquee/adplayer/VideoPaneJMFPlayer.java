package com.lge.marquee.adplayer;


import java.awt.Component;
import java.awt.Dimension;
import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;

import javax.media.CannotRealizeException;
import javax.media.ControllerClosedEvent;
import javax.media.ControllerEvent;
import javax.media.ControllerListener;
import javax.media.EndOfMediaEvent;
import javax.media.Manager;
import javax.media.NoPlayerException;
import javax.media.Player;
import javax.media.StopByRequestEvent;
import javax.media.Time;

import marquee.adlib.ads.ScrollType;


public class VideoPaneJMFPlayer extends PanePlayerBase implements ControllerListener {
	private int videoIdx = 0;
	Component comp = null;
	Player player = null;
	
	// constructor
	VideoPaneJMFPlayer(int x, int y, Dimension di, int interval, ScrollType scrollType, ArrayList<String> imgList) {
		super();
		initPlayer(x, y, di, interval, scrollType, imgList);
		Manager.setHint(Manager.LIGHTWEIGHT_RENDERER, true);
		createPlayer();
		startVideoPlayer();
	}
	
	private void createPlayer() {
		File file = new File(imgList.get(videoIdx));
		try {
			URL url = file.toURI().toURL();
			try {
				player = Manager.createRealizedPlayer(url);
				player.addControllerListener(this);
			} catch (NoPlayerException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (CannotRealizeException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private void startVideoPlayer() {
		comp = player.getVisualComponent();		
		this.add(comp);
		comp.setBounds(0, 0, di.width, di.height);
		
		// start playing the media clip
		player.start();
	}
	
	private void stopVideoPlayer() {		
		player.stop();
		player.deallocate();
		player.close();
		if (comp != null) {
			this.remove(comp);
		}	
	}
	
	@Override
	public synchronized void controllerUpdate(ControllerEvent event) {
		// TODO Auto-generated method stub
		// event handling of "end of play".
		if(event instanceof EndOfMediaEvent) {
			if (cnt > 1) {
				stopVideoPlayer();
			} else {
				player.setMediaTime(new Time(0));
				player.start();
			}
		} else if (event instanceof StopByRequestEvent) {

		} else if (event instanceof ControllerClosedEvent) {
			if (cnt > 1) {
				checkCount();
				createPlayer();
				startVideoPlayer();
			}
		}
	}
	
	private void checkCount() {
		// check overflow of videoCnt.
		if (cnt > 1) {
			if (cnt - 1 > videoIdx) {
				videoIdx++;
			} else {
				videoIdx = 0;
			}
		}
	}
	
	public void stopPlayer() {
		player.removeControllerListener(this);
		stopVideoPlayer();
	}
}
