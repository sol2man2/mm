package com.lge.marquee.adplayer;

import java.awt.Dimension;
import java.awt.Image;
import java.util.ArrayList;

import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JLayeredPane;
import javax.swing.JPanel;

import marquee.adlib.ads.ScrollType;

public class PagingPanePlayer extends PanePlayerBase {
	private JLabel label;
	private ArrayList<JLabel> imgLabelList; 
		
	public PagingPanePlayer(int x, int y, Dimension di, int interval, ScrollType scrollType, ArrayList<String> imgList) {
		super();
		initPlayer(x, y, di, interval, scrollType, imgList);
		createPane();
	}
	
	public void run() {
		while(true) {
			showPane();
			if (checkStop() == true) {
				//System.out.println("stop!!!!paging");
				stopPane();
				return;
			}
		}	
	}
	
	void createPane() {
		imgLabelList = new ArrayList<JLabel>();
		cnt = imgList.size();
		for (int i = 0; i < cnt; i++) {
			//System.out.println("img" + i + "=" + imgList.get(i));
			ImageIcon staticImage = new ImageIcon(imgList.get(i));
			Image image = staticImage.getImage();
			Image scaledImage = image.getScaledInstance(di.width, di.height, Image.SCALE_FAST);
			//System.out.println("this.getWidth : " + di.width + ", " + "this.getHeigh : " + di.height);
			ImageIcon newStaticImage = new ImageIcon(scaledImage);
			label = new JLabel(newStaticImage);
			imgLabelList.add(label);
			this.add(label);
		}
	}
	
	void showPane() {
		for (int i = 0; i < cnt; i++) {
			label = imgLabelList.get(i);
			//setBounds 시 x, y를 넣으면 LayeredPane에 add된 label의 setBounds이기 때문에 
			//LayeredPane의 위치를 기준으로 다시 계산하게 됨. 
			//그러므로 여기서는 0,0을 해야 다시 계산 안하고 보이게 됨
			label.setBounds(0, 0, di.width, di.height);
			this.moveToFront(label);
			try {
					Thread.sleep(interval);
			} catch(InterruptedException e) {
				e.printStackTrace();
			}	
		}
	}
	
	void stopPane() {
		for (int i = 0; i < cnt; i++) {
			label = imgLabelList.get(i);
			this.remove(label);
		}
		this.setVisible(false);
	}
}