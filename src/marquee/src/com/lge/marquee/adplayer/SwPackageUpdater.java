package com.lge.marquee.adplayer;

import marquee.adlib.utils.MarqueeResourcePath;

public class SwPackageUpdater {
	private AdPlayerModel adModel;
	
	// Constructor
	public SwPackageUpdater(AdPlayerModel model) {
		adModel = model;
	}

	public void notifySwUpdate() {
		// copy package file to MARQUEE_SWPKG_TEMP_PATH
		System.exit(0);
	}
}
