package com.lge.marquee.adplayer;

import java.io.File;
import java.util.ArrayList;

import marquee.adlib.ads.AdModelInterface;
import marquee.adlib.ads.impl.AdPackage;
import marquee.adlib.ads.impl.AonXMLBuilder;
import marquee.adlib.ads.AdPackageDataObject;
import marquee.adlib.utils.FindFileWithExtension;
import marquee.adlib.utils.MarqueeResourcePath;

public class AdPlayerModel {

	AdModelInterface pkgData;
	private String strAdPkgName;	
	private ArrayList<AdPackageDataObject> listPane;
	
	public AdPlayerModel() {
		
		// load configuration xml & get pane list.
		pkgData = new AdPackage();
		
		// create control instance.
		new AdUpdateMngr(this, pkgData);
		
		loadAdPackage(MarqueeResourcePath.getInstance().getAdPkgCurrentPath());
	}
	
	// load received advertisement package.
	public void loadAdPackage(String strFileName) {
		
		String cfgFile = null;
		
		cfgFile = FindFileWithExtension.getInstance().getFileWithExtension(strFileName, "xml");
		strFileName = strFileName + "/" + cfgFile;
		boolean ret = pkgData.loadAdPackage(strFileName);
		listPane = getPaneList();
		
	}
	
	// get pane list.
	public ArrayList<AdPackageDataObject> getPaneList() {
		return pkgData.getData();
	}
	
	// notify to observer.
	public void notifyToObservers() {
		pkgData.notifyObservers();
	}
	
	// notify Ad updated.
	public void notifyToAdUpdateMngr() {
		pkgData.notifyAdUpdateMngr();
	}

}
