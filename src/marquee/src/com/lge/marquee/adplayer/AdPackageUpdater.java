package com.lge.marquee.adplayer;

import marquee.adlib.utils.MarqueeResourcePath;

public class AdPackageUpdater {
	private AdPlayerModel adModel;
	
	// Constructor
	public AdPackageUpdater(AdPlayerModel model) {
		adModel = model;
	}

	public boolean notifyAdUpdate() {
		adModel.notifyToAdUpdateMngr();
		return true;
	}
	
	public void notifyAdLoad() {
		adModel.loadAdPackage(MarqueeResourcePath.getInstance().getAdPkgCurrentPath());
	}
}
