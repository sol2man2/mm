package com.lge.marquee.comm;

import marquee.adlib.utils.MarqueeEntityInfo;

public interface DownloadListener {
	public MarqueeEntityInfo getMarqueeEntityInfo();
	public String getContentsPath();
	public void onDownloadComplete(MarqueeEntityInfo marqueeInfo);
	public String getServerIP();
}
