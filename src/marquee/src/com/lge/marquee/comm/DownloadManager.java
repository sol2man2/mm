package com.lge.marquee.comm;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import marquee.adlib.utils.MarqueeEntityInfo;

import com.lge.adlib.comm.Acknowledge;
import com.lge.adlib.comm.Acknowledge.AcknowledgeCode;
import com.lge.adlib.comm.AdPackageRequest;
import com.lge.adlib.comm.MarqueeInfoRequest;
import com.lge.adlib.comm.MarqueeInfoResponse;
import com.lge.adlib.comm.Request;
import com.lge.adlib.comm.Response;
import com.lge.adlib.comm.SoftwareRequest;
import com.lge.adlib.comm.SystemInfo;

public class DownloadManager implements Runnable, SystemInfo {

	protected String path = "C:\\aaa\\";

	private final int BUFFER_SIZE = 1024 * 8;
	protected ScheduledExecutorService downloadScheduler = null;

	int port;

	public static DownloadManager downloadManager = null;

	public DownloadManager() {
		port = SystemInfo.SERVER_PORT;
		downloadScheduler = Executors.newScheduledThreadPool(1);
	}

	private DownloadListener downloadListener = null;

	public void setDownloadListener(DownloadListener downloadListener) {
		this.downloadListener = downloadListener;
	}

	public static DownloadManager startDownloadManager() {
		if( null == downloadManager ) {
			downloadManager = new DownloadManager();
		}
		downloadManager.execute();
		return downloadManager;
	}

	private void execute() {
		downloadScheduler.scheduleAtFixedRate(this, 0L, 1L, TimeUnit.MINUTES);
		
//		downloadScheduler.execute(this);
	}

	public static boolean stopDownloadManager() throws InterruptedException {
		if( null == downloadManager ) {
			return false;
		}
		else {
			downloadManager.shutdownNow();
			downloadManager.awaitTermination(2000, TimeUnit.MILLISECONDS);
			return true;
		}
	}

	public void shutdown() {
		downloadScheduler.shutdown();
	}

	public void shutdownNow() {
		downloadScheduler.shutdownNow();
	}

	public void awaitTermination(int i, TimeUnit milliseconds) throws InterruptedException {
		downloadScheduler.awaitTermination(i, milliseconds);
	}

	private Socket socket = null;

	private InputStream isFromSocket = null;
	private BufferedInputStream bisFromSocket = null;
	private ObjectInputStream oisFromSocket = null;

	private OutputStream osToSocket = null;
	private BufferedOutputStream bosToSocket = null;
	private ObjectOutputStream oosToSocket = null;

	private FileOutputStream fosToFile = null;
	private BufferedOutputStream bosToFile = null;

	@Override
	public void run() {
		
		try {
			socket = new Socket(downloadListener.getServerIP(), SystemInfo.SERVER_PORT);
			System.out.println("6...");
			openOutputStreamToSocket();
			System.out.println("5...");
			sendMarqueeInfoRequest();
			System.out.println("4...");
			openInputStreamFromSocket();
			System.out.println("3...");
			Response res = receiveResponse();
			System.out.println("2...");
			sendAcknowledge();
			System.out.println("1...");
			downloadListener.onDownloadComplete(((MarqueeInfoResponse)res).getMarqueeEntityInfo());
			System.out.println("Download Complete");
		} catch (UnknownHostException e1) {
			System.out.println("UnknownHostException");
			e1.printStackTrace();
		} catch (IOException e1) {
			System.out.println("IOException");
			e1.printStackTrace();
		} catch (ClassNotFoundException e) {
			System.out.println("ClassNotFoundException");
			e.printStackTrace();
		} finally {
			try {
				closeInputStreamFromSocket();
				closeOutputStreamToSocket();

				socket.close();
			} catch (IOException e) {
				e.printStackTrace();
				System.out.println("socket close exception");
			}
		}
	}

	private void sendAcknowledge() throws IOException {
		Acknowledge ack = new Acknowledge();
		ack.setResponseCode(AcknowledgeCode.SUCCESS);

		oosToSocket.writeObject(ack);
		oosToSocket.flush();
	}

	private void sendMarqueeInfoRequest() throws IOException {
		MarqueeEntityInfo marqueeEntityInfo = downloadListener.getMarqueeEntityInfo();
		MarqueeInfoRequest request = new MarqueeInfoRequest(marqueeEntityInfo);
		request.setVersion(101L);
		oosToSocket.writeObject(request);
		oosToSocket.flush();
	}

	private void closeOutputStreamToSocket() throws IOException {
		oosToSocket.close();
		bosToSocket.close();
		osToSocket.close();
	}

	private void closeInputStreamFromSocket() throws IOException {
		oisFromSocket.close();
		bisFromSocket.close();
		isFromSocket.close();
	}

	private void sendAdPackageRequest() throws IOException {
		AdPackageRequest adPackageRequest = new AdPackageRequest();

		adPackageRequest.setVersion(201L);
		oosToSocket.writeObject(adPackageRequest);
		oosToSocket.flush();
	}

	private void closeOutputStreamToFile() throws IOException {
		fosToFile.close();
		bosToFile.close();
	}

	private void openOutputStreamToFile(String filePath) throws FileNotFoundException {
		String path = downloadListener.getContentsPath();
		String fileName = new File(filePath).getName();
		
		File file = new File(path, fileName);

		fosToFile  = new FileOutputStream(file);
		bosToFile = new BufferedOutputStream(fosToFile, BUFFER_SIZE);
	}

	private Response receiveResponse() throws IOException, ClassNotFoundException {
		Response res = (Response) oisFromSocket.readObject();

		MarqueeInfoResponse marqueeInfoResponse = (MarqueeInfoResponse) res;
		MarqueeEntityInfo marqueeEntityInfo = marqueeInfoResponse.getMarqueeEntityInfo();

		if( marqueeEntityInfo.isbIsNewApplication() ) {
			receiveFileThruSocket(marqueeEntityInfo.getMarqueeApplication(), marqueeEntityInfo.getSizeMarqueeApplication());
		}

		if( marqueeEntityInfo.isbIsNewPackage() ) {
			receiveFileThruSocket(marqueeEntityInfo.getMarqueePackage(), marqueeEntityInfo.getSizeMarqueePackage());
		}

		return res;
	}

	private void receiveFileThruSocket(String filePath, long fileSize) throws IOException {
		openOutputStreamToFile(filePath);

		final int buffLength = BUFFER_SIZE;
		byte[] buff = new byte[buffLength];

		int receivedSize = 0;
		int receivedTotalSize = 0;

		long sizeToReceive = fileSize;

		for(;;) {
			if( sizeToReceive >= buffLength ){
				receivedSize = bisFromSocket.read(buff, 0, buffLength);
				sizeToReceive -= receivedSize;
				bosToFile.write(buff, 0, receivedSize);
				bosToFile.flush();
			} else {
				receivedSize = bisFromSocket.read(buff, 0, (int)sizeToReceive);
				sizeToReceive -= receivedSize;
				bosToFile.write(buff, 0, receivedSize);
				bosToFile.flush();
				if( sizeToReceive == 0 ) break;
			}
		}

		closeOutputStreamToFile();
	}

	private void openInputStreamFromSocket() throws IOException {
		isFromSocket = socket.getInputStream();
		bisFromSocket = new BufferedInputStream(isFromSocket, 1024 * 8);
		oisFromSocket = new ObjectInputStream(bisFromSocket);
	}

	private void openOutputStreamToSocket() throws IOException {
		osToSocket = socket.getOutputStream();
		bosToSocket = new BufferedOutputStream(osToSocket, 1024 * 8);
		oosToSocket = new ObjectOutputStream(bosToSocket);
	}

}
