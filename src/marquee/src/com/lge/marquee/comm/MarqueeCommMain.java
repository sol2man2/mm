package com.lge.marquee.comm;

import marquee.adlib.utils.MarqueeEntityInfo;

public class MarqueeCommMain {
	public static void main(String[] arg) {
		DownloadManager downloadmanager = DownloadManager.startDownloadManager();
		downloadmanager.setDownloadListener(new UpdaterMock());

		try {
			while(true) {
				Thread.sleep(20000);
			}
		} catch (InterruptedException e) {
			e.printStackTrace();
		}

		boolean result = false;
		try {
			result = DownloadManager.stopDownloadManager();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		System.out.println("DownloadManager down: " + result);
	}
}

class UpdaterMock implements DownloadListener {

	@Override
	public MarqueeEntityInfo getMarqueeEntityInfo() {
//		MarqueeEntityInfo marqueeInfo = new MarqueeEntityInfo(0L, 1111, 0L, null, false, 0L, null, false, 0L, 0L, 0L);
		MarqueeEntityInfo marqueeInfo = new MarqueeEntityInfo(0L, 1111, 1L, null, false, 1L, null, false, 0L, 0L, 0L);
		return marqueeInfo;
	}

	@Override
	public String getContentsPath() {
		String curDir = System.getProperty("user.dir");
System.out.println("cur dir: " + curDir);
		return curDir;
	}

	@Override
	public void onDownloadComplete(MarqueeEntityInfo marqueeInfo) {
		return;
	}

	@Override
	public String getServerIP() {
		// TODO Auto-generated method stub
		return null;
	}
	
}
