package com.lge.marquee.updater;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.lang.management.ManagementFactory;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.util.Enumeration;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import marquee.adlib.utils.ConfigFileUtils;
import marquee.adlib.utils.MarqueeEntityInfo;
import marquee.adlib.utils.MarqueeResourcePath;

import com.lge.marquee.adplayer.AdPackageUpdater;
import com.lge.marquee.comm.DownloadListener;
import com.lge.marquee.comm.DownloadManager;

public class MarqueeUpdater implements DownloadListener, Runnable {
	private 	String 		path4MarqueeInfoDB 	= null;
	private		long		marqueeDeviceID		= 0L;
	private		String		serverIP			= null;
	private		int			serverPort			= 0;
	
	
	private 	MarqueeEntityInfo 	marqueeDeviceInfo	= null;
	private		AdPackageUpdater 	updateMarquee		= null;
	protected 	ExecutorService 	updaterThread 		= null;

	private boolean 	bIsValidNetworkStatue		= false;
	private boolean 	bIsDownloadManagerStarted	= false;
	
	public MarqueeUpdater(AdPackageUpdater adUpdator) {
		super();

		ConfigFileUtils myUtils 	= new ConfigFileUtils();
		File configFile 			= null; 

		this.path4MarqueeInfoDB	= MarqueeResourcePath.getInstance().getConfigPath() + "/marqueeInfo.dat";
		this.updateMarquee 		= adUpdator;
		this.updaterThread 		= Executors.newFixedThreadPool(1);
		this.updaterThread.execute(this);
		
		if(!this.loadData()) {
			this.marqueeDeviceInfo = new MarqueeEntityInfo(0L, 0L, 1L, null, false, 0L, null, false, 0L, 0L, 0L);
		} else {
			
			if (0L == this.marqueeDeviceInfo.getMarqueeDeviceID()) {
				this.marqueeDeviceInfo = new MarqueeEntityInfo(0L, 1111, 1L, null, false, 0L, null, false, 0L, 0L, 0L);		
			}
		}

		// Use device ID in config.txt
		configFile = new File(MarqueeResourcePath.getInstance().getConfigPath(), "config.txt");
		if (configFile.exists()) {
			this.marqueeDeviceID 	= myUtils.getMarqueeID(configFile);
			this.serverIP 			= myUtils.getServerIP(configFile);
			this.serverPort 		= myUtils.getServerPort(configFile);
			
			System.out.println("config.txt : ID(" + this.marqueeDeviceID + ") IP(" + this.serverIP + ") Port(" + this.serverPort + ")");
			this.marqueeDeviceInfo.setMarqueeDeviceID(this.marqueeDeviceID);			
		}

		// read configuration
		// set device ID this.marqueeDeviceInfo.set
	//	this.marqueeDeviceInfo.setMarqueeDeviceID(marqueeDeviceID);
		
		this.saveData();
	}
	public MarqueeUpdater() {
	}
	private boolean controlDownloadManager(boolean bStart) {		
		if (bStart) {
			(DownloadManager.startDownloadManager()).setDownloadListener(this);
		} else {
			try {
				DownloadManager.stopDownloadManager();
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				return false;
			}
		}		
			
		return true;
	}
		
	public boolean checkNetworkInterface() {	
		
		boolean bRetValue = false;
		
		this.bIsValidNetworkStatue = true;
	
		try {
			Enumeration<NetworkInterface> interfaces = NetworkInterface.getNetworkInterfaces();
			
			while (interfaces.hasMoreElements()) {
				NetworkInterface netInterface = interfaces.nextElement();
				if (netInterface.isUp()) {
					Enumeration<InetAddress> addresses = netInterface.getInetAddresses();
					byte[] mac = netInterface.getHardwareAddress();
//					String macAddr = mac.toString();
					
//					System.out.println("Name : " + netInterface.getName() + " is ON");
					
					while(addresses.hasMoreElements()){
						InetAddress address = addresses.nextElement();
//						System.out.println("Address : " + address);
					}
//					System.out.println("MAC : " + mac + "\n");
						            
					bRetValue = true;
				}
				else {
//					System.out.println(netInterface.getName() + " is OFF");
				}	
			}
		} catch (SocketException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			this.bIsValidNetworkStatue = false;
		}
		
		return bRetValue;
	}	

	public String getProcessID() {	
		return ManagementFactory.getRuntimeMXBean().getName();
	}	

	public void startNotePad() {	
		String[] cmds = { "CMD", "/C", "C:\\Windows\\System32\\notepad.exe" };
		
		try {
			Process proc = Runtime.getRuntime().exec(cmds);
			System.out.println("Notepad process : " + proc);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void update2NewApplication(MarqueeEntityInfo marqueeInfo) {
		ConfigFileUtils myUtils 		= new ConfigFileUtils();

		String path4Download 			= MarqueeResourcePath.getInstance().getDownloadPath();
		
		String path4Application			= MarqueeResourcePath.getInstance().getSwPkgCurrentPath();
		String path4ApplicationTemp		= MarqueeResourcePath.getInstance().getSwPkgTempPath();
		String path4ApplicationBackup	= MarqueeResourcePath.getInstance().getSwPkgBackupPath();
		
		String path4Package				= MarqueeResourcePath.getInstance().getAdPkgCurrentPath();
		String path4PackageTemp			= MarqueeResourcePath.getInstance().getAdPkgTempPath();
		String path4PackageBackup		= MarqueeResourcePath.getInstance().getAdPkgBackupPath();
		
		File file			= null;
		File downloadFile 	= null;
		File tempFile		= null;
		File currentFile	= null;
		File backupFile		= null;
		
		
		file = new File (path4Download);
		if (!file.exists())	file.mkdirs();

		file = new File (path4ApplicationTemp);
		if (!file.exists())	file.mkdirs();

		file = new File (path4PackageTemp);
		if (!file.exists())	file.mkdirs();

		// Check new application.
		downloadFile = new File(path4Download, marqueeInfo.getMarqueeApplication());	
		if (!downloadFile.exists()) {
			// No New application
			return;
		}
//
//		try {
//			Thread.sleep(3000);
//		} catch (InterruptedException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
//
		
		// Move new application file from download to application temp.
		tempFile = new File(path4ApplicationTemp, marqueeInfo.getMarqueeApplication());
		myUtils.moveSingleFile(downloadFile, tempFile);
		
	
		// Delete backup directory
		myUtils.deleteFilesInDirectory(new File(path4ApplicationBackup));
		
		// Backup current application.
		currentFile	= new File(path4Application, this.marqueeDeviceInfo.getMarqueeApplication());	
		backupFile	= new File(path4ApplicationBackup, this.marqueeDeviceInfo.getMarqueeApplication());	
		myUtils.copyFileDirectory(currentFile, backupFile);	
		
		// Check new applicationUpdater.
		downloadFile = new File(path4Download, "updater.jar");	
		if (downloadFile.exists()) {

			// Move new applicationUpdater file from download to application temp.
			tempFile = new File(path4ApplicationTemp, "updater.jar");	
			myUtils.moveSingleFile(downloadFile, tempFile);					
		
			// Backup current updater.
			currentFile	= new File(path4Application, "updater.jar");	
			backupFile	= new File(path4ApplicationBackup, "updater.jar");	
			myUtils.moveSingleFile(currentFile, backupFile);
			
			// Restore new applicationUpdater
			myUtils.moveSingleFile(tempFile, currentFile);	
		}
		
		// Check New AD Package	
		if (marqueeInfo.isbIsNewPackage()) {
			
			// Untar the new Package
			System.out.println("___SW_______ UNTAR __________");	
			myUtils.unpackAdPackage(marqueeInfo.getMarqueePackage(), path4Download, path4PackageTemp);
	
			if (!myUtils.validationOfContent(path4PackageTemp)) {
				System.out.println("___SW_______ ( FAIL ) validationOfContent __________");	
				myUtils.deleteFilesInDirectory(new File(path4PackageTemp));
				
				marqueeInfo.setMarqueePackageVer(this.marqueeDeviceInfo.getMarqueePackageVer());
				marqueeInfo.setMarqueePackage(this.marqueeDeviceInfo.getMarqueePackage());
				marqueeInfo.setbIsNewPackage(false);
			} else {
				System.out.println("___SW_______ ( OK ) validationOfContent __________");	
			}			
			
			// Backup current AD package.
			myUtils.copyFileDirectory(new File(path4Package), new File(path4PackageBackup));			
		}

// GOLD TEMP		
//		myUtils.deleteFilesInDirectory(new File(path4Download));
		
		this.marqueeDeviceInfo = marqueeInfo;
		this.saveData();			

		// Disable ad player
		System.out.println("___SW_______ CALL notifyAdUpdate __________");	
		this.updateMarquee.notifyAdUpdate();
		
		// Disable Download Manager.
		System.out.println("___SW_______ CALL controlDownloadManager __________");	
		this.controlDownloadManager(false);
		
		// 추가 해야 함. --> sb94 : flag 파일 생성.
		File flagFile = new File(".\\download.txt");
		if(flagFile.exists() == false){
			try {
				flagFile.createNewFile();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
			
		// Launch applicationUpdater.
		System.out.println("___SW_______ LAUNCH UPDATE.JAR __________");	
		ProcessBuilder pb = new ProcessBuilder("java", "-jar", "updater.jar");
		pb.directory(new File(path4Application));
		try {
			pb.start();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		// Suicide
		System.exit(0);
	}

	// Return false : invalid package.
	// Mr. Gold
	public boolean update2NewPackage(MarqueeEntityInfo marqueeInfo) {
		ConfigFileUtils myUtils 	= new ConfigFileUtils();
		String path4Download 		= MarqueeResourcePath.getInstance().getDownloadPath();
		String path4PackageTemp		= MarqueeResourcePath.getInstance().getAdPkgTempPath();
		String path4PackageCurrent	= MarqueeResourcePath.getInstance().getAdPkgCurrentPath();
		String path4PackageBackup	= MarqueeResourcePath.getInstance().getAdPkgBackupPath();

		File file = null;
		file = new File (path4Download);
		if (!file.exists())	file.mkdirs();

		file = new File (path4PackageTemp);
		if (!file.exists())	file.mkdirs();

		file = new File (path4PackageCurrent);
		if (!file.exists())	file.mkdirs();

		// Untar the new Package : from path4Download to path4TempApplication.
		System.out.println("___SW_______ UNTAR __________");	
		myUtils.deleteFilesInDirectory(new File(path4PackageTemp));
		myUtils.unpackAdPackage(marqueeInfo.getMarqueePackage(), path4Download, path4PackageTemp);
		
		System.out.println("___SW_______ UNTAR OK __________");	
		if (!myUtils.validationOfContent(path4PackageTemp)) {
			myUtils.deleteFilesInDirectory(new File(path4PackageTemp));
			System.out.println("___AD_______ ( FAIL ) validationOfContent __________");	
			return false;
		}
		else {
			System.out.println("___AD_______ ( OK ) validationOfContent __________");						
		}

		// Should I check the return value ???? (No retry)
		System.out.println("___AD_______B4 : notifyAdUpdate() __________");	
		if (this.updateMarquee.notifyAdUpdate()) {
			System.out.println("___AD_______ ( OK ) notifyAdUpdate() __________");	
			
			// Backup package
			System.out.println("___AD_______ COPY CURRENT 2 BACKUP __________");	
			myUtils.copyFileDirectory(new File(path4PackageCurrent), new File(path4PackageBackup));			
			
			// Delete current package files
			System.out.println("___AD_______ DEL CURRENT __________");	
			myUtils.deleteFilesInDirectory(new File(path4PackageCurrent));
			
			// Move new package files.
			System.out.println("___AD_______ COPY TEMP 2 CURRENT __________");	
			myUtils.copyFileDirectory(new File(path4PackageTemp), new File(path4PackageCurrent));	
			
			// Delete temporary files.
			System.out.println("___AD_______ DEL TEMP __________");	
			myUtils.deleteFilesInDirectory(new File(path4PackageTemp));
			
			this.updateMarquee.notifyAdLoad();								
		} else {
			System.out.println("___AD_______ ( FAIL ) notifyAdUpdate() __________");
		}
	
		this.marqueeDeviceInfo = marqueeInfo;
		this.saveData();			

		return true;
	}
	
	@Override
	public void onDownloadComplete(MarqueeEntityInfo marqueeInfo) {
		ConfigFileUtils myUtils 	= new ConfigFileUtils();

		printMarqueeEntity(marqueeInfo, "[DOWNLOADED          ]");

		if (marqueeInfo.isbIsNewApplication()) {
			System.out.println("__________onDownloadComplete (New SW Package) __________");	
			// Update and re-load application
			update2NewApplication (marqueeInfo);
		}		

		if (marqueeInfo.isbIsNewPackage()) {
			System.out.println("__________onDownloadComplete (New AD Package) __________");	
			
			if (!update2NewPackage (marqueeInfo)) {	
				System.out.println("__________ INVALID AD Package __________");			
			}
		} else {
			System.out.println("__________onDownloadComplete (SAME package) __________");			
		}
		
		System.out.println("GrpID(" + marqueeInfo.getMarqueeGroupID() + ") DevID(" + marqueeInfo.getMarqueeDeviceID() + ")");
		System.out.println("Application V" + marqueeInfo.getMarqueeApplicationVer() + " : "+ marqueeInfo.getMarqueeApplication());
		System.out.println("Package V" + marqueeInfo.getMarqueePackageVer() + " : " + marqueeInfo.getMarqueePackage());			
		System.out.println("Display(" + marqueeInfo.getMarqueeDiplaySize() + " inches) (" + marqueeInfo.getMarqueeResolustionWidth() + " x " + marqueeInfo.getMarqueeResolustionHeight() + ")");		

		myUtils.deleteFilesInDirectory(new File(MarqueeResourcePath.getInstance().getDownloadPath()));
	}	

	@Override
	public MarqueeEntityInfo getMarqueeEntityInfo() {
		return this.marqueeDeviceInfo;
	}

	@Override
	public String getContentsPath() {
		return (MarqueeResourcePath.getInstance().getDownloadPath());
		//return null;
	}	
	
	private boolean saveData() {
		File file = new File(this.path4MarqueeInfoDB);
		if( file.exists() )
			file.delete();

		FileOutputStream fos 	= null;
		ObjectOutputStream oos 	= null;
			
		try {
			
			fos = new FileOutputStream(file);
			oos = new ObjectOutputStream(fos);

			oos.writeObject(this.marqueeDeviceInfo);
			oos.flush();
			
			oos.close();
			fos.close();
			
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}		
		
		return true;
	}
	
	private boolean loadData() {
		File file = new File(this.path4MarqueeInfoDB);
		
		if( !file.exists() )
			return false;
		
		FileInputStream fis 	= null;
		ObjectInputStream ois 	= null;
		
		Object obj = null;
		
		try {
			
			fis = new FileInputStream(file);
			ois = new ObjectInputStream(fis);

			obj = ois.readObject();

			ois.close();
			fis.close();
			
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		this.marqueeDeviceInfo = (MarqueeEntityInfo) obj;
		
		return true;
	}

	@Override
	public void run() {
		boolean bNetworkStatus = false;
			
		while(true) {
			bNetworkStatus = this.checkNetworkInterface();
			
			if (!this.bIsValidNetworkStatue) {
				// wait 1 seconds
				try {
					Thread.sleep(1000);	
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}			
				
				continue;
			}
			
			if ((bNetworkStatus) && (!bIsDownloadManagerStarted)) {
				if (this.controlDownloadManager(true)) {
					this.bIsDownloadManagerStarted 	= true;
				} else {
					this.bIsDownloadManagerStarted = false;
				}
			} else if ((!bNetworkStatus) && (bIsDownloadManagerStarted)) {			
				if (this.controlDownloadManager(false)) {
					bIsDownloadManagerStarted = false;
				} else {
					bIsDownloadManagerStarted = true;
				}
			}	
			
			// wait 30 seconds
			try {
				Thread.sleep(30000);	
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}					
		}
	}
	@Override
	public String getServerIP() {
		// TODO Auto-generated method stub
		return this.serverIP;
	}	
	
	private void printMarqueeEntity (MarqueeEntityInfo marqueeInfo, String title) {			
		System.out.println(title + "  ID(" + marqueeInfo.getMarqueeGroupID() + " : " + marqueeInfo.getMarqueeDeviceID()
				+ ")     SW(" + marqueeInfo.getMarqueeApplicationVer() + (marqueeInfo.isbIsNewApplication() ? " (New) : " : " (Same) : ") 
				+ marqueeInfo.getMarqueeApplication() 
				+ ")     AD(" + marqueeInfo.getMarqueePackageVer() + (marqueeInfo.isbIsNewPackage() ? " (New) : " : " (Same) : ") 
				+ marqueeInfo.getMarqueePackage() 
				+ ")     SIZE(" + marqueeInfo.getMarqueeDiplaySize() 
				+ " : " + marqueeInfo.getMarqueeResolustionWidth() + " x " + marqueeInfo.getMarqueeResolustionHeight() + ")");		
	}	
}
