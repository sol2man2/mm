package com.lge.marquee.updater.test;

import java.io.File;
import java.io.IOException;

import junit.framework.TestCase;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.lge.marquee.updater.MarqueeUpdater;

public class MarqueeUpdaterTestCase extends TestCase {
	private MarqueeUpdater marqueeUpdater = null;

	@Before
	protected void setUp() throws Exception {
		super.setUp();
		this.marqueeUpdater = new MarqueeUpdater();
	}

	@After
	protected void tearDown() throws Exception {
		super.tearDown();
	}

//	@Test
//	public void testOnDownloadComplete() {
//		this.marqueeUpdater.onDownloadComplete(new MarqueeEntityInfo(0, 6789, 11, "marqueeApplication_v1.1.jar", false, 1, "AdPackage_ver1.tar", true, 22, 1366, 768));
//	}
//	@Test
//	public void testCheckNetworkStatus() {	
//		this.marqueeUpdater.checkNetworkInterface();
//	}

//	@Test
//	public void testChekProcessID() {
//		System.out.println(marqueeUpdater.getProcessID());
//	}
//	
	@Test
	public void testStartNotePad() {
		 ProcessBuilder pb = new ProcessBuilder("java", "-jar", "server.jar");
		 pb.directory(new File("C://CMU_Project//mm//src//config"));
		 try {
			pb.start();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		marqueeUpdater.startNotePad();
	}
}
