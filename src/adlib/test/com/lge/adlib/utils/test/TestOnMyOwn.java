package com.lge.adlib.utils.test;

import java.io.File;

import marquee.adlib.utils.ConfigFileUtils;
//import marquee.adlib.utils.CreateTarFile;
//import marquee.adlib.utils.ExtractTarFile;

//------------------------------------------------------------------------------------------
// Purpose: To test ConfigFileUtils(tar and untar) and provide the usage of ConfigFileUtils.
//			Counterparts using ConfigFileUtils can refer to the below usage example.
//------------------------------------------------------------------------------------------
public class TestOnMyOwn {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		
		//******************************************************************
		//  DO NOT USE FILE NAME OR FOLDER NAME BY KOREAN !!!!!!
		//  PLEASE, USE IT IN ENGILSH!
		//******************************************************************
		
		//File Names and Paths
		String xmlNameAndPath = "c:/test/config_tmp/config_xml.xml"; //Local XML file name and path of config tool.
		String packageName = "AdPackage.tar"; // The file name of AD package in config tool.
		String destPath; //The folder in which AD package file is extracted. It is the pre-defined Path.
		String fileNameAndPath = "c:/test/mmcontrol/config.txt"; // this text file has Marquee ID, server IP and server port.
		
		ConfigFileUtils myUtils = new ConfigFileUtils();
				
		myUtils.packAdPackage(xmlNameAndPath,packageName); //To make AD package. Need validation and need to return err or ok.
		destPath = myUtils.getPathOfConfigToPlay();
		myUtils.unpackAdPackage(packageName, destPath); //To unpack AD package. 일단 MM을 위한 구현, 차후 config tool의 load 기능 고려하여 수정 예정
		
		myUtils.getMarqueeID(new File(fileNameAndPath)); //return type: long (-1 if error)
		myUtils.getServerIP(new File(fileNameAndPath));  //return type: String ("000.000.000.000" if error)
		myUtils.getServerPort(new File(fileNameAndPath)); //trturn type: int (0 if error)
		
	} //main()
}