package marquee.adlib.ads;

public enum AdPaneType {
	STATIC, PAGING, SCROLLING, VIDEO
}