package marquee.adlib.ads;

import java.awt.Rectangle;
import java.util.ArrayList;

import marquee.adlib.ads.impl.AdPane;

public abstract class AdPackageDataObject implements Comparable<AdPackageDataObject>{
	
	public static AdPackageDataObject createDO()
	{
		return new AdPane();
	}
	// Getter
	public abstract Rectangle getRectangle();
	public abstract ScrollType getScrollType();
	public abstract AdPaneType getAdPaneType();
	public abstract int getInterval();
	public abstract int getzOrder();
	public abstract ArrayList<String> getContentsList();	// 각 미디어 파일의 경로+파일이름 의 리스트를 던진다.
	
	// Setter
	public abstract void setRectangle(Rectangle rect);
	public abstract void setScrollType(ScrollType scrollType);
	public abstract void setAdPaneType(AdPaneType type);
	public abstract void setInterval(int nInterval);
	public abstract void setzOrder(int zOrder);
	public abstract void setContentsList(ArrayList<String> listContent);
	
	// ID
	public abstract void setID(int ID);
	public abstract int  getID();
}