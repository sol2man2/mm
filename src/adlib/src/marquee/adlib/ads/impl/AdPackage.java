package marquee.adlib.ads.impl;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;

import marquee.adlib.ads.AdModelInterface;
import marquee.adlib.ads.AdPackageDataObject;
import marquee.adlib.utils.AdUpdateListener;
import marquee.adlib.utils.IObserver;


public class AdPackage implements AdModelInterface{

	private ArrayList<AdPackageDataObject> listPane = new ArrayList<AdPackageDataObject>();
	private ArrayList<IObserver> mObservers = new ArrayList<IObserver>();
	private ArrayList<AdUpdateListener> mAdUpdateMngr = new ArrayList<AdUpdateListener>();
	
	private String extName;
	private int selectedId;
	
	//==============================================================
	//AdPackage 내의 Pane List를 얻어오는 Interface
	@Override
	public ArrayList<AdPackageDataObject> getData() {
		// TODO Auto-generated method stub
		return listPane;
	}

	//==============================================================
	//AdPackage 저장하는 Interface
	@Override
	public boolean saveAdPackage(String strFileName) {
		// TODO Auto-generated method stub
		// 저장할 위치 와 파일 이름만 온다. xml로 저장한다.
		// 2가지로 나뉜다. pkg와.. 오리지날...xml로..
		SplitPath(strFileName);
		if(extName.equals("xml")){
			if(AonXMLBuilder.createXML(strFileName, listPane) == false)
				return false;
		}
		else if(extName.equals("pkg")){
			// 장책임님이 만든 모듈을 호출한다.
		}
		else
			return false;
		
		return true;
	}
	//==============================================================
	// AdPackage 로딩하는 Interface
	@Override
	public boolean loadAdPackage(String strFileName) {

		// TODO Auto-generated method stub
		// 읽어야할 파일의 경로와 이름이 온다.
		// 2가지로 나뉜다. pkg와 오리지날 xml로...
	    SplitPath(strFileName);
	    if(extName.equals("xml"))
	    {
	    	ArrayList<AdPackageDataObject> listAD;
	    	listAD = AonXMLBuilder.parserXML(strFileName);
	    	if(listAD == null)
	    		return false;
	    	
	    	if(checkContentsFile(listAD) == false)
	    		return false;
	    	
	    	// load가 끝나고 나면 noti를 날린다.
	    	Collections.sort(listAD); 
	    	listPane = listAD;
	    	notifyObservers();
	    }
	    else if(extName.equals("pkg"))
	    {
	    	// 장책임님이 만든 모듈을 호출한다. 호출 결과는 UnPackage한 결과값의 위치 경로 정도...
	    	
	    	// load가 끝나고 나면 noti를 날린다.
			notifyObservers();	
	    }
	    else
	    {
	    	return false;
	    }

	    return true;
	}

	//==============================================================
	//AdPackage를 초기화하는 Interface
	@Override
	public void newAdPackage() {
		// TODO Auto-generated method stub
		// Model 정보 초기화하기..
		for(int i = 0; i < listPane.size(); i++) {
			AdPackageDataObject data = (AdPackageDataObject)listPane.get(i);
			ArrayList<String> listStr = data.getContentsList();
			listStr.clear();
		}
		listPane.clear();
		notifyObservers();
	}
	
	@Override
	public boolean addPane(AdPackageDataObject configData) {
		// 현재 List의 마지막 놈의 ID를 구해와서 +1 해서 ID를 채워 넣는다.
		int nSize = listPane.size();
		if(nSize == 0){
			configData.setID(1);
			configData.setzOrder(1);
		}
		else{
			// zOrder로 정렬되어 있기 때문에 List 마지막 아이템에서 다음 zorder를 구하면 된다.
			AdPackageDataObject adDO = listPane.get(listPane.size()-1);
			int zorder = adDO.getzOrder();
			configData.setzOrder(zorder+1);
			
			// Max ID를 찾아서 다음 ID를 구한다.
			int nID = FindMaxID();			
			configData.setID(nID+1);
		}
		listPane.add(configData);
		notifyObservers();
		return true;
	}

	@Override
	public boolean modifyPane(int id, AdPackageDataObject configData) {
		
		for(int i = 0; i < listPane.size(); i++) {
			AdPackageDataObject DO = (AdPackageDataObject)listPane.get(i);
			// 같은 ID를 찾으면..
			if(DO.getID() == id){
//				DO = configData;
				listPane.set(i, configData);
				Collections.sort(listPane);

				notifyObservers();
				return true;
			}
		}
		return false;
	}

	@Override
	public boolean deletePane(int id) {
		
		for(int i = 0; i < listPane.size(); i++) {
			AdPackageDataObject data = (AdPackageDataObject)listPane.get(i);
			// 같은 ID를 찾으면..
			if(data.getID() == id){
				data.getContentsList().clear();
				listPane.remove(i);
				notifyObservers();
				return true;
			}
		}
		return false;
	}
	
	////////////////////////////////////////////////////////////////////
	// IObserver 를 위한 Interface
	@Override
	public void registerObserver(IObserver o) {
		// TODO Auto-generated method stub
		mObservers.add(o);
	}

	@Override
	public void removeObserver(IObserver o) {
		// TODO Auto-generated method stub
		int i = mObservers.indexOf(o);
		if (i >= 0) {
			mObservers.remove(i);
		}
	}

	@Override
	public void notifyObservers() {
		for(int i = 0; i < mObservers.size(); i++) {
			IObserver observer = (IObserver)mObservers.get(i);
			observer.update();
		}
	}
	
	////////////////////////////////////////////////////////////////////
	// Interfaces using in Marquee.
	@Override
	public void registerAdUpdateMngr(AdUpdateListener l) {
		// TODO Auto-generated method stub
		mAdUpdateMngr.add(l);
	}

	@Override
	public void unregisterAdUpdateMngr(AdUpdateListener l) {
		// TODO Auto-generated method stub
		int i = mAdUpdateMngr.indexOf(l);
		if (i >= 0) {
			mAdUpdateMngr.remove(i);
		}
	}
	
	@Override
	public void notifyAdUpdateMngr() {
		for (int i = 0; i < mAdUpdateMngr.size(); i++) {
			AdUpdateListener listener = (AdUpdateListener)mAdUpdateMngr.get(i);
			listener.onUpdate();
		}
	}
	
	@Override
	public int getSelectedPaneID(){
		return this.selectedId;
	}
	
	@Override
	public void setSelectedPaneID(int selectedId){
		this.selectedId = selectedId;
		notifyObservers();
	}
	///////////////////////////////////////////////////////////////////////

	///////////////////////////////////////////////////////////////////////////////
	private void SplitPath(String strPath){
		//int file = strPath.lastIndexOf("/");
	    int ext = strPath.lastIndexOf(".");
	    int length = strPath.length();
	    //fileName = strPath.substring(file+1,ext);
	    extName = strPath.substring(ext+1,length);
	}
	
	private boolean checkContentsFile(ArrayList<AdPackageDataObject> listAD){
	
		for(AdPackageDataObject DO : listAD){
			ArrayList<String> listContents = DO.getContentsList();

			for(String strName : listContents){
				File fileContent = new File(strName);
				
				// sehanee temp block
//				if(fileContent.exists() == false)
//					return false;
			}
		}
		return true;
	}
	
	private int FindMaxID(){
		int Ret = -1;
		for(AdPackageDataObject DO : listPane){
			int adoID = DO.getID();
			if(Ret < adoID){
				Ret = adoID;
			}
		}
		return Ret;
	}
}
