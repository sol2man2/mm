package marquee.adlib.ads.impl;

import java.awt.Rectangle;
import java.util.ArrayList;

import marquee.adlib.ads.AdPackageDataObject;
import marquee.adlib.ads.AdPaneType;
import marquee.adlib.ads.ScrollType;

public class AdPane extends AdPackageDataObject{
	private int ID;
	private int x;
	private int y;
	private int width;
	private int height;
	private int zOrder;
	private AdPaneType adType;
	private ScrollType scrollType;
	private int interval;
	private ArrayList<String> listContents = new ArrayList<String>();
	
	@Override
	public Rectangle getRectangle() {
		Rectangle rect = new Rectangle(x,y,width,height);
		return rect;
	}
	@Override
	public ScrollType getScrollType() {
		return scrollType;
	}
	@Override
	public AdPaneType getAdPaneType() {
		return adType;
	}
	@Override
	public ArrayList<String> getContentsList() {
		return listContents;
	}
	@Override
	public void setRectangle(Rectangle rect) {
		this.x = rect.x;
		this.y = rect.y;
		this.width = rect.width;
		this.height = rect.height;
	}
	@Override
	public void setScrollType(ScrollType scrollType) {
		this.scrollType = scrollType;
	}
	@Override
	public void setAdPaneType(AdPaneType type) {
		this.adType = type;
	}
	@Override
	public void setContentsList(ArrayList<String> listContent) {
		this.listContents = listContent;
	}
	@Override
	public int getInterval() {
		return interval;
	}
	@Override
	public void setInterval(int nInterval) {
		this.interval = nInterval;
	}
	@Override
	public void setID(int ID) {
		this.ID = ID;
	}
	@Override
	public int getID() {
		// TODO Auto-generated method stub
		return this.ID;
	}
	@Override
	public int getzOrder() {
		return zOrder;
	}
	@Override
	public void setzOrder(int zOrder) {
		this.zOrder = zOrder;
	}
	
	////////////////////////////////////////////////////////
	@Override
	public int compareTo(AdPackageDataObject o) {
		return this.zOrder - o.getzOrder();
	}
}
