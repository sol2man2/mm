package marquee.adlib.ads.impl;

import java.awt.Rectangle;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.util.ArrayList;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import marquee.adlib.ads.AdPackageDataObject;
import marquee.adlib.ads.AdPaneType;
import marquee.adlib.ads.ScrollType;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public class AonXMLBuilder {
	
	public static boolean tarProcess(String strFileName){
		
		ArrayList<AdPackageDataObject> ADO = parserXML(strFileName);
		
		if(ADO == null)
			return false;
		
		// for(int i=0; i < ADO.size(); i++)
		for(AdPackageDataObject adDO : ADO){
			ArrayList<String> strReplace = new ArrayList<String>();
			
			for(String str : adDO.getContentsList()){
				String exeName = null, fileName = null;
				int file = str.lastIndexOf("\\");
			    //int ext = str.lastIndexOf(".");
			    int length = str.length();
			    fileName = str.substring(file+1,length);
			    //exeName = str.substring(ext+1,length);
			    
			    String strRet = new String(fileName);
			    strReplace.add(strRet);
			}
			adDO.setContentsList(strReplace);
		}
		
		boolean bRet = createXML(strFileName,ADO);		
		
		return bRet;
	}

	public static boolean createXML(String strFileName, ArrayList<AdPackageDataObject> listDO){
		try
		{
	        DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
	        DocumentBuilder docBuilder = docFactory.newDocumentBuilder();

	        // 루트 엘리먼트
	        Document doc = docBuilder.newDocument();
	        Element rootElement = doc.createElement("aon");
	        doc.appendChild(rootElement);

	        // header 엘리먼트
	        Element header = doc.createElement("header");
	        rootElement.appendChild(header);
	        
	        // version 엘리먼트
	        Element version = doc.createElement("version");
	        version.appendChild(doc.createTextNode("0.2"));
	        header.appendChild(version);
	        
	        Element canvas = doc.createElement("Canvas");
	        header.appendChild(canvas);
	        canvas.setAttribute("CanvasWidth", Integer.toString(960));
	        canvas.setAttribute("CanvasHeight", Integer.toString(540));
	        

	        // body 엘리먼트
	        Element body = doc.createElement("body");
	        header.appendChild(body);
	        //rootElement.appendChild(body);
	        // Loop를 돌아야 한다. --> AdPackage의 AD Data를 돌면서 얻어온다.
	        for(int i = 0; i < listDO.size(); i++) {
				AdPackageDataObject data = (AdPackageDataObject)listDO.get(i);
				
	        	// pane 엘리먼트
		        Element pane = doc.createElement("pane");
		        body.appendChild(pane);
		        pane.setAttribute("id", Integer.toString(data.getID()));
		        pane.setAttribute("panetype", data.getAdPaneType().toString());
		        Rectangle rect = data.getRectangle();
		        pane.setAttribute("x", Integer.toString(rect.x));
		        pane.setAttribute("y", Integer.toString(rect.y));
		        pane.setAttribute("width", Integer.toString(rect.width));
		        pane.setAttribute("height", Integer.toString(rect.height));
		        pane.setAttribute("zorder", Integer.toString(data.getzOrder()));
		        pane.setAttribute("scrolltype", data.getScrollType().toString());
		        pane.setAttribute("interval", Integer.toString(data.getInterval()));
		        
		        // Loop를 돌아야 한다. --> listContents
		        //x 엘리먼트
		        ArrayList<String> listStr = data.getContentsList();
		        for(int j=0; j < listStr.size(); j++)
		        {
		        	String strContent = listStr.get(j);
		        	Element content = doc.createElement("content");
		        	content.appendChild(doc.createTextNode(strContent));
			        pane.appendChild(content);
		        }
	        }
	        // XML 파일로 쓰기
	        TransformerFactory transformerFactory = TransformerFactory.newInstance();
	        Transformer transformer = transformerFactory.newTransformer();

	        transformer.setOutputProperty(OutputKeys.ENCODING, "UTF-8");
	        transformer.setOutputProperty(OutputKeys.INDENT, "yes");        
	        DOMSource source = new DOMSource(doc);
	        StreamResult result = null;
	        
			try {
				result = new StreamResult(new FileOutputStream(new File(strFileName)));
			} catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				return false;
			}

	        // 파일로 쓰지 않고 콘솔에 찍어보고 싶을 경우 다음을 사용 (디버깅용)
	        // StreamResult result = new StreamResult(System.out);

	        transformer.transform(source, result);

	        System.out.println("File saved!");
		}
		catch (ParserConfigurationException pce)
		{
			pce.printStackTrace();
			return false;
		}
		catch (TransformerException tfe)
		{
			tfe.printStackTrace();
			return false;
		}
		return true;
	}
	
	public static ArrayList<AdPackageDataObject> parserXML(String strFileName)
	{
		ArrayList<AdPackageDataObject> listAD = new ArrayList<AdPackageDataObject>();
		
		try
        {
             //DOM Document 객체 생성하기 위한 메서드 
             DocumentBuilderFactory f = DocumentBuilderFactory.newInstance();
             //DOM 파서로부터 입력받은 파일을 파싱하도록 요청
             DocumentBuilder parser = f.newDocumentBuilder();
             
             //XML 파일 지정
             Document xmlDoc = null; 
             //DOM 파서로부터 입력받은 파일을 파싱하도록 요청 
             xmlDoc = parser.parse(new File(strFileName));
             
             //루트 엘리먼트 접근 
             Element root = xmlDoc.getDocumentElement();
             
             // Header Node
//             NodeList headerNodes = root.getElementsByTagName("header");
             
             // Body Node
             NodeList paneNodes = root.getElementsByTagName("pane");
             //전체 출력
             //속성값 : panetype,x,y,width,height,zorder,interval
             //엘리먼트 텍스트 값 : content
             System.out.println("------------------------------------------------------------------");
             for(int i=0; i<paneNodes.getLength(); i++)
             {
            	 AdPane pane = new AdPane();
                 //속성 접근
                 Node paneNode = paneNodes.item(i);
                 NamedNodeMap paneNodeAttrs = paneNode.getAttributes();    //컬렉션을 반환받음
                 
                 String paneID = paneNodeAttrs.getNamedItem("id").getNodeValue();
                 pane.setID(Integer.parseInt(paneID));
                 String paneType = paneNodeAttrs.getNamedItem("panetype").getNodeValue();
                 pane.setAdPaneType(getPaneType(paneType));
                 String x = paneNodeAttrs.getNamedItem("x").getNodeValue();
                 String y = paneNodeAttrs.getNamedItem("y").getNodeValue();
                 String width = paneNodeAttrs.getNamedItem("width").getNodeValue();
                 String height = paneNodeAttrs.getNamedItem("height").getNodeValue();
                 Rectangle rect = new Rectangle(Integer.parseInt(x), Integer.parseInt(y),
                		 Integer.parseInt(width), Integer.parseInt(height));
                 pane.setRectangle(rect);
                 String zorder = paneNodeAttrs.getNamedItem("zorder").getNodeValue();
                 pane.setzOrder(Integer.parseInt(zorder));
                 String scrollType = paneNodeAttrs.getNamedItem("scrolltype").getNodeValue();
                 pane.setScrollType(getScrollType(scrollType));
                 String interval = paneNodeAttrs.getNamedItem("interval").getNodeValue();
                 pane.setInterval(Integer.parseInt(interval));
                 
                 //엘리먼트의 텍스트 값 접근
                 ArrayList<String> listContent = pane.getContentsList();
                 NodeList contentNodes = paneNode.getChildNodes();
                 String strContent;
                 for(int j=0; j<contentNodes.getLength(); j++)
                 {
                     Node childNode = contentNodes.item(j);
                     if(childNode.getNodeType() == Node.ELEMENT_NODE)
                     {
                         strContent = childNode.getChildNodes().item(0).getNodeValue();
                         listContent.add(strContent);
                     }
                 }
                 listAD.add(pane);
             }
        } catch(Exception e) {
            System.out.println(e.toString());
            listAD = null;
        }
		
		return listAD;
	}
	
	private static AdPaneType getPaneType(String paneType){
		if(paneType.equals("STATIC")){
			return AdPaneType.STATIC;
		}
		else if(paneType.equals("PAGING")){
			return AdPaneType.PAGING;
		}
		else if(paneType.equals("SCROLLING")){
			return AdPaneType.SCROLLING;
		}
		else
			return AdPaneType.VIDEO;
	}
	
	private static ScrollType getScrollType(String scrollType){
		if(scrollType.equals("VERTICAL")){
			return ScrollType.VERTICAL;
		}
		else
			return ScrollType.HORIZONTAL;
	}
}


// 속성값 정의
//Attr attr = doc.createAttribute("id");
//attr.setValue("1");
//staff.setAttributeNode(attr);

// 속성값을 정의하는 더 쉬운 방법
// staff.setAttribute("id", "1");
