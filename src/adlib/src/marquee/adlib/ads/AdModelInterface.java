package marquee.adlib.ads;

import java.util.ArrayList;

import marquee.adlib.utils.AdUpdateListener;
import marquee.adlib.utils.IObserver;

public interface AdModelInterface {
	
	// Model Component 의 Core Data를 전달하기 위한 Interface
	public ArrayList<AdPackageDataObject> getData();
	
	// Model Component의 Service Interface
	public boolean saveAdPackage(String strFileName);   // ret : false - save 실패할 경우
	public boolean loadAdPackage(String strFileName);	// ret : false - 잘못된 파일 포맷일 경우
	public void newAdPackage();
	
	// 새로운 Pane 만들기 - 넘겨준 ConfigData의 zOrder가 겹치는게 있을 경우 Fail로 던진다. 기본은 -1로 Model에서 관리한다.
	public boolean addPane(AdPackageDataObject configData);
	public boolean modifyPane(int id, AdPackageDataObject configData);
	public boolean deletePane(int id);
	
	////////////////////////////////////////////
	// Observer Pattern Base Function
	// Attach Observer Interface
	public void registerObserver(IObserver o);
	// Detach Observer Interface
	public void removeObserver(IObserver o);
	// Observer Notify Interface
	public void notifyObservers();
	
	///////////////////////////////////////////////
	// Observer Notify Interface for Marquee.
	public void registerAdUpdateMngr(AdUpdateListener l);
	public void unregisterAdUpdateMngr(AdUpdateListener l);
	public void notifyAdUpdateMngr();
	
	public int getSelectedPaneID();
	public void setSelectedPaneID(int selectedId);
}
