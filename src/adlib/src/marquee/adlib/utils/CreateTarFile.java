package marquee.adlib.utils;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

import org.kamranzafar.jtar.TarEntry;
import org.kamranzafar.jtar.TarOutputStream;

public class CreateTarFile {
	
	//----------------------------------------------------------------------
	// using TarOutputStream (tar)
	//----------------------------------------------------------------------
	public void tarFile(String xmlFile, String[] contentFile, String pkgFile){
	   // Output file stream
	   FileOutputStream dest = null;
	     
	   try {
		   //dest = new FileOutputStream( "c:/test/test.tar" );
		   dest = new FileOutputStream(pkgFile);
	   } catch (FileNotFoundException e) {
		   // TODO Auto-generated catch block
		   e.printStackTrace();
	   }

	   // Create a TarOutputStream
	   TarOutputStream out = new TarOutputStream( new BufferedOutputStream( dest ) );

	   // Files to tar
	   /*
	   File[] filesToTar=new File[2];
	   //filesToTar[0]=new File("c:/test/myfile1.txt");
	   //filesToTar[1]=new File("c:/test/myfile2.txt");
	   filesToTar[0]=new File(xmlFile);
	   filesToTar[1]=new File(contentFile);
	   */
	   File[] filesToTar=new File[(contentFile.length)+1];
	   
	   for(int i= 0; i <contentFile.length; i++){
		   filesToTar[i]=new File(contentFile[i]);
		   System.out.println("packageFile["+ i + "] = " + filesToTar[i]);
	   }
	   
	   filesToTar[contentFile.length]=new File(xmlFile);
	   System.out.println("packageFile["+ contentFile.length + "] = " + filesToTar[contentFile.length]);
	   
	   for(File f:filesToTar){
	      try {
	    	  out.putNextEntry(new TarEntry(f, f.getName()));
		  } catch (IOException e) {
			  // TODO Auto-generated catch block
			  e.printStackTrace();
		  }
	      
	      BufferedInputStream origin = null;
	      try {
	    	  origin = new BufferedInputStream(new FileInputStream( f ));
	      } catch (FileNotFoundException e) {
	    	  // TODO Auto-generated catch block
	    	  e.printStackTrace();
	      }

	      int count;
	      byte data[] = new byte[2048];
	      try {
	    	  while((count = origin.read(data)) != -1) {
	    		  out.write(data, 0, count);
			  }
	      } catch (IOException e) {
	    	  // TODO Auto-generated catch block
	    	  e.printStackTrace();
	      }

	      try {
	    	  out.flush();
	      } catch (IOException e) {
	    	  // TODO Auto-generated catch block
	    	  e.printStackTrace();
	      }
	      
	      try {
	    	  origin.close();
	      } catch (IOException e) {
	    	  // TODO Auto-generated catch block
	    	  e.printStackTrace();
	      }
	   }

	   try {
		   out.close();
	   } catch (IOException e) {
		   // TODO Auto-generated catch block
		   e.printStackTrace();
	   }
	   
	   System.out.println("Success tarFile()");
	} //void TarFile()
}
