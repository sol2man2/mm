package marquee.adlib.utils;

public interface IObserver {
	public void update();
}
