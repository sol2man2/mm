package marquee.adlib.utils;

import java.io.File;

public class FindFileWithExtension {
	private File[] fileList;
	private static FindFileWithExtension findFile;
	
	FindFileWithExtension() {

	}

	// singleton class.
	public static FindFileWithExtension getInstance() {
		if (FindFileWithExtension.findFile == null) {
			findFile = new FindFileWithExtension();
		}
		
		return findFile;
	}
	
	// find file with given extension.
	public String getFileWithExtension(String path, String ext) {
		String fileName = null;
		fileList = new File(path).listFiles();
		
		for (int i = 0; i < fileList.length; i++) {
			fileName = fileList[i].getName();
			if (fileName.endsWith("." + ext)) {
				break;
			}
		}
		return fileName;
	}
	
	
}
