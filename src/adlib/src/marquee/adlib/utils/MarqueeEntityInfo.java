package marquee.adlib.utils;

import java.io.Serializable;

public class MarqueeEntityInfo implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID 	= 1L;
	
	private long 	marqueeGroupID		= 0x00;	// Marquee group ID	(will be used 2nd version)
	private long 	marqueeDeviceID		= 0x00;	// Marquee device ID	(MAC address)
	
	private long 	marqueeApplicationVer 		= 0x00;		// Application software version.
	private String 	marqueeApplication			= null;		// Package file name.
	private boolean bIsNewApplication	= false;	// Not transferred between client and server.
	private long sizeMarqueeApplication = 0L;

	public long getSizeMarqueeApplication() {
		return sizeMarqueeApplication;
	}

	public void setSizeMarqueeApplication(long sizeMarqueeApplication) {
		this.sizeMarqueeApplication = sizeMarqueeApplication;
	}

	private long 	marqueePackageVer			= 0x00;		// Advertisement package version.
	private String 	marqueePackage				= null;		// Advertisement package file name.
	private boolean bIsNewPackage 	= false;	// Not transferred between client and server.
	private long sizeMarqueePackage = 0L;

	public long getSizeMarqueePackage() {
		return sizeMarqueePackage;
	}

	public void setSizeMarqueePackage(long sizeMarqueePackage) {
		this.sizeMarqueePackage = sizeMarqueePackage;
	}

	private long 	marqueeDiplaySize			= 0;
	private long 	marqueeResolustionWidth		= 0;
	private long 	marqueeResolustionHeight	= 0;
	
	public MarqueeEntityInfo(long marqueeGroupID, long marqueeDeviceID,
			long marqueeApplicationVer, String marqueeApplication,
			boolean bIsNewApplication, long marqueePackageVer,
			String marqueePackage, boolean bIsNewPackage,
			long marqueeDiplaySize, long marqueeResolustionWidth,
			long marqueeResolustionHeight) {
		super();
		this.marqueeGroupID = marqueeGroupID;
		this.marqueeDeviceID = marqueeDeviceID;
		this.marqueeApplicationVer = marqueeApplicationVer;
		this.marqueeApplication = marqueeApplication;
		this.bIsNewApplication = bIsNewApplication;
		this.marqueePackageVer = marqueePackageVer;
		this.marqueePackage = marqueePackage;
		this.bIsNewPackage = bIsNewPackage;
		this.marqueeDiplaySize = marqueeDiplaySize;
		this.marqueeResolustionWidth = marqueeResolustionWidth;
		this.marqueeResolustionHeight = marqueeResolustionHeight;
	}

	public long getMarqueeGroupID() {
		return marqueeGroupID;
	}

	public void setMarqueeGroupID(long marqueeGroupID) {
		this.marqueeGroupID = marqueeGroupID;
	}

	public long getMarqueeDeviceID() {
		return marqueeDeviceID;
	}

	public void setMarqueeDeviceID(long marqueeDeviceID) {
		this.marqueeDeviceID = marqueeDeviceID;
	}

	public long getMarqueeApplicationVer() {
		return marqueeApplicationVer;
	}

	public void setMarqueeApplicationVer(long marqueeApplicationVer) {
		this.marqueeApplicationVer = marqueeApplicationVer;
	}

	public String getMarqueeApplication() {
		return marqueeApplication;
	}

	public void setMarqueeApplication(String marqueeApplication) {
		this.marqueeApplication = marqueeApplication;
	}

	public boolean isbIsNewApplication() {
		return bIsNewApplication;
	}

	public void setbIsNewApplication(boolean bIsNewApplication) {
		this.bIsNewApplication = bIsNewApplication;
	}

	public long getMarqueePackageVer() {
		return marqueePackageVer;
	}

	public void setMarqueePackageVer(long marqueePackageVer) {
		this.marqueePackageVer = marqueePackageVer;
	}

	public String getMarqueePackage() {
		return marqueePackage;
	}

	public void setMarqueePackage(String marqueePackage) {
		this.marqueePackage = marqueePackage;
	}

	public boolean isbIsNewPackage() {
		return bIsNewPackage;
	}

	public void setbIsNewPackage(boolean bIsNewPackage) {
		this.bIsNewPackage = bIsNewPackage;
	}

	public long getMarqueeDiplaySize() {
		return marqueeDiplaySize;
	}

	public void setMarqueeDiplaySize(long marqueeDiplaySize) {
		this.marqueeDiplaySize = marqueeDiplaySize;
	}

	public long getMarqueeResolustionWidth() {
		return marqueeResolustionWidth;
	}

	public void setMarqueeResolustionWidth(long marqueeResolustionWidth) {
		this.marqueeResolustionWidth = marqueeResolustionWidth;
	}

	public long getMarqueeResolustionHeight() {
		return marqueeResolustionHeight;
	}

	public void setMarqueeResolustionHeight(long marqueeResolustionHeight) {
		this.marqueeResolustionHeight = marqueeResolustionHeight;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}
}
