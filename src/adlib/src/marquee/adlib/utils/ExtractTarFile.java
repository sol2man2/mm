package marquee.adlib.utils;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

//We use open source(library) called jtar, and
//all source codes in package org.kamranzafar.jtar have "Copyright 2012 Kamran Zafar"
import org.kamranzafar.jtar.TarEntry;
import org.kamranzafar.jtar.TarInputStream;

public class ExtractTarFile {
	
	//----------------------------------------------------------------------
	// using TarInputStream (untar)
	//----------------------------------------------------------------------
	public void untarFile(String targetFile, String destPath){
	   //String tarFile = "c:/test/test.tar";
	   //String destFolder = "c:/test/myfiles";
	   String tarFile = targetFile;
	   String destFolder = destPath;

	   // Create a TarInputStream
	   TarInputStream tis = null;
	   try {
		   tis = new TarInputStream(new BufferedInputStream(new FileInputStream(tarFile)));
	   } catch (FileNotFoundException e) {
		   // TODO Auto-generated catch block
		   e.printStackTrace();
	   }
	   TarEntry entry;
	   try {
			while((entry = tis.getNextEntry()) != null) {
			      int count;
			      byte data[] = new byte[2048];
	
			      FileOutputStream fos = new FileOutputStream(destFolder + "/" + entry.getName());
			      BufferedOutputStream dest = new BufferedOutputStream(fos);
	
			      while((count = tis.read(data)) != -1) {
			         dest.write(data, 0, count);
			      }
	
			      dest.flush();
			      dest.close();
			   }
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	   
	   try {
		   tis.close();
	   } catch (IOException e) {
		   // TODO Auto-generated catch block
		   e.printStackTrace();
	   }
	   
	   System.out.println("Success untarFile()");
	}//void untarFile()
}
