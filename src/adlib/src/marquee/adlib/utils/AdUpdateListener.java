package marquee.adlib.utils;

public interface AdUpdateListener {
	public void onUpdate();
}
