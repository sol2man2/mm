package marquee.adlib.utils;

import java.io.BufferedInputStream;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Vector;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import marquee.adlib.ads.impl.AonXMLBuilder;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public class ConfigFileUtils {

    String[] contentsNameAndPath; //the paths and names of contents in XML file.
    String[] contentsNamePathInMm; // the names of contents in XML file in MM for validation of content.
    String adPkgStorage = "c:/test/ad_pkg"; //Pre-defined Path to save the packed AD package in config Tool.
    String adPkgTempStorage = "c:/user/res/adpkg/temp"; //Pre-defined Path in which AD package have to be saved during updating.
    String adPkgCurrentStorage = "c:/user/res/adpkg/current"; //Pre-defined Path to be unpacked after updating in MM. Also it has xml file and contents to be played in MM.
    
	//-------------------------------------------------------------------------
	// To make AD package using tar. If there is no content written in XML, false will be returned.
	//-------------------------------------------------------------------------
	public boolean packAdPackage(String xmlNameAndPath, String pkgName){  
		
	    //String contentNameAndPath = "c:/test/content_tmp/content1.gif";
	    getPathAndNameOfContents(xmlNameAndPath);
	    
	    // Validation of content
	    for(int a = 0 ; a < contentsNameAndPath.length; a++){
	    	boolean fileIs = true;
	    	fileIs = checkFileExist(contentsNameAndPath[a]); 
	    	if(fileIs == false){
	    		System.out.println("Content File - " + contentsNameAndPath[a] + " is not exist!!!!");
	    		return false;
	    	}	    	
	    }
	    
	    //To make sure File Path. Directory path will be made if there is no directory.
	    checkAndMakeDir(adPkgStorage); 
	    
	   //full path and file name of AD package.
	    String adPkgPathAndName = adPkgStorage + "/";
	    adPkgPathAndName = adPkgPathAndName + pkgName;

	    CreateTarFile workTar = new CreateTarFile();
	    workTar.tarFile(xmlNameAndPath, contentsNameAndPath, adPkgPathAndName);
	    
	    return true;
	}
	
	//-------------------------------------------------------------------------
	// To unpack AD package using untar
	//-------------------------------------------------------------------------
	public void unpackAdPackage(String targetFile, String destPath) {
		
		checkAndMakeDir(destPath); 
		
		//full path and file name of AD package in MM
		String adPkgPathAndNameInMm = adPkgTempStorage + "/";
		adPkgPathAndNameInMm = adPkgPathAndNameInMm + targetFile;
		
	    ExtractTarFile workUntar = new ExtractTarFile();
	    workUntar.untarFile(adPkgPathAndNameInMm, destPath);
	    
	    //-----------------------------------------------------------------------
	    // To remove directory path of contents in XML in MM
	    //-----------------------------------------------------------------------
	    //Vector<String> cassFiles = getFileNames(rootDir + "/CBND", "shp"); 
//	    Vector<String> tarFiles = getFileNames("c:/user/res/adpkg/current", "xml");
	    Vector<String> tarFiles = getFileNames(destPath, "xml");
	    for(int a =0; a < tarFiles.size(); a++){
	    	//System.out.println("c:/user/res/adpkg/current/");
	    	System.out.println(tarFiles.elementAt(a));
		    //AonXMLBuilder.tarProcess("c:/user/res/adpkg/current/test2.xml");
		    AonXMLBuilder.tarProcess(tarFiles.elementAt(a));
	    }	   
	    
//	    validationOfContent();
	    
	}

	public void unpackAdPackage(String targetFile, String srcPath, String destPath) {
		
		System.out.println("_____ : unpackAdPackage");
		checkAndMakeDir(srcPath); 
		System.out.println("_____ : srcPath");
		checkAndMakeDir(destPath); 
		System.out.println("_____ : destPath");
	
		//full path and file name of AD package in MM
		String adPkgPathAndNameInMm = srcPath + "/";
		adPkgPathAndNameInMm = adPkgPathAndNameInMm + targetFile;
		
	    ExtractTarFile workUntar = new ExtractTarFile();
	    workUntar.untarFile(adPkgPathAndNameInMm, destPath);
	    
	    Vector<String> tarFiles = getFileNames(destPath, "xml");
	    for(int a =0; a < tarFiles.size(); a++){
	    	//System.out.println("c:/user/res/adpkg/current/");
	    	System.out.println(tarFiles.elementAt(a));
		    //AonXMLBuilder.tarProcess("c:/user/res/adpkg/current/test2.xml");
		    AonXMLBuilder.tarProcess(tarFiles.elementAt(a));
	    }	
	}

	//-------------------------------------------------------------------------
	// XML parsing for getting PATH and FILE NAME of contents from XML file.
	//-------------------------------------------------------------------------	
	public void getPathAndNameOfContents(String xmlNameAndPath) {
				
		try {
			  //File file = new File("c:\\MyXMLFile.xml");
			  File file = new File(xmlNameAndPath);
			  DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
			  DocumentBuilder db = dbf.newDocumentBuilder();
			  Document doc = db.parse(file);
			  doc.getDocumentElement().normalize();
			  
			  System.out.println("Root element " + doc.getDocumentElement().getNodeName());
			  NodeList nodeLst = doc.getElementsByTagName("pane");
			  System.out.println("=====Information of all contents=====");
			  
			  System.out.println("The number of Panes = " + nodeLst.getLength());
			  //contentsNameAndPath = new String[nodeLst.getLength()];;
			  
			  NodeList nodeLstOfContents = doc.getElementsByTagName("content");
			  System.out.println("The number of all Contents = " + nodeLstOfContents.getLength());
			  contentsNameAndPath = new String[nodeLstOfContents.getLength()];
			  
			  //----------------------------------------------------------------------
			  //  In case of one content per one pane
			  //----------------------------------------------------------------------
			  /*
			  for (int s = 0; s < nodeLst.getLength(); s++) {

			    Node fstNode = nodeLst.item(s);
			    
			    if (fstNode.getNodeType() == Node.ELEMENT_NODE) {		  
		    	  Element fstElmnt = (Element) fstNode;
		    	  
			      NodeList fstNmElmntLst = fstElmnt.getElementsByTagName("content");
			      Element fstNmElmnt = (Element) fstNmElmntLst.item(0);
			      NodeList fstNm = fstNmElmnt.getChildNodes();

			      System.out.println("Full Path of Content[" + s + "]:"  + ((Node) fstNm.item(0)).getNodeValue());
			      contentsNameAndPath[s] = ((Node) fstNm.item(0)).getNodeValue();
			    }
			  }
			  */

			  //----------------------------------------------------------------------
			  //  In case of many contents per one pane
			  //----------------------------------------------------------------------
			  int totoalNumberOfContents = 0;
			  
			  for (int s = 0; s < nodeLst.getLength(); s++) {
				    
				    Node fstNode = nodeLst.item(s);
				    
				    if (fstNode.getNodeType() == Node.ELEMENT_NODE) {		  
			    	  Element fstElmnt = (Element) fstNode;
			    	  		      
			    	  	  NodeList fstNmElmntLst = fstElmnt.getElementsByTagName("content");
			    	  	  System.out.println("XXXXXXXXX The numberOfcontentsPerPane = " + fstNmElmntLst.getLength());
			    	  	  Element fstNmElmnt; 
			    	  	  for (int c = 0; c < fstNmElmntLst.getLength(); c++){
						      fstNmElmnt= (Element) fstNmElmntLst.item(c);
						      NodeList fstNm = fstNmElmnt.getChildNodes();
						      System.out.println("Full Path of Content[" + totoalNumberOfContents + "]:"  + ((Node) fstNm.item(0)).getNodeValue());
						      contentsNameAndPath[totoalNumberOfContents] = ((Node) fstNm.item(0)).getNodeValue();   	  
						      totoalNumberOfContents++;
			    	  	  } //for (int c = 0; c < fstNmElmntLst.getLength(); c++)
				    } //if (fstNode.getNodeType() == Node.ELEMENT_NODE)
				  } //for (int s = 0; s < nodeLst.getLength(); s++)
			  } catch (Exception e) {
			    e.printStackTrace();
			  }
	}
	
	public void getPathAndNameOfContentsInMm(String xmlNameAndPath) {
		
		try {
			  File file = new File(xmlNameAndPath);
			  DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
			  DocumentBuilder db = dbf.newDocumentBuilder();
			  Document doc = db.parse(file);
			  doc.getDocumentElement().normalize();
			  
			  System.out.println("Root element " + doc.getDocumentElement().getNodeName());
			  NodeList nodeLst = doc.getElementsByTagName("pane");
			  
			  System.out.println("The number of Panes = " + nodeLst.getLength());
			  
			  NodeList nodeLstOfContents = doc.getElementsByTagName("content");
			  System.out.println("The number of all Contents = " + nodeLstOfContents.getLength());
			  contentsNamePathInMm = new String[nodeLstOfContents.getLength()];
			  
			  int totoalNumberOfContents = 0;
			  
			  for (int s = 0; s < nodeLst.getLength(); s++) {
				    
				    Node fstNode = nodeLst.item(s);
				    
				    if (fstNode.getNodeType() == Node.ELEMENT_NODE) {		  
			    	  Element fstElmnt = (Element) fstNode;
			    	  		      
			    	  	  NodeList fstNmElmntLst = fstElmnt.getElementsByTagName("content");
			    	  	  System.out.println("XXXXXXXXX The numberOfcontentsPerPane = " + fstNmElmntLst.getLength());
			    	  	  Element fstNmElmnt; 
			    	  	  for (int c = 0; c < fstNmElmntLst.getLength(); c++){
						      fstNmElmnt= (Element) fstNmElmntLst.item(c);
						      NodeList fstNm = fstNmElmnt.getChildNodes();
						      System.out.println("Full Path of Content[" + totoalNumberOfContents + "]:"  + ((Node) fstNm.item(0)).getNodeValue());
						      contentsNamePathInMm[totoalNumberOfContents] = ((Node) fstNm.item(0)).getNodeValue();
						      totoalNumberOfContents++;
			    	  	  } //for (int c = 0; c < fstNmElmntLst.getLength(); c++)
				    } //if (fstNode.getNodeType() == Node.ELEMENT_NODE)
				  } //for (int s = 0; s < nodeLst.getLength(); s++)
			  } catch (Exception e) {
			    e.printStackTrace();
			  }
	}
	
	//---------------------------------------------------------------------------	
	// To offer the path of AD Package in config tool. Pre-defined.
	//---------------------------------------------------------------------------
	public String getPathOfAdPkg() {
		
		checkAndMakeDir(adPkgStorage);
		return adPkgStorage;
	}
	
	//---------------------------------------------------------------------------	
	// To offer the path to be downloaded in. Pre-defined.
	//---------------------------------------------------------------------------
	public String getPathOfDownload() {
		
		checkAndMakeDir(adPkgTempStorage);
		return adPkgTempStorage;
	}
	
	//---------------------------------------------------------------------------	
	// To offer the path of unpacked xml and contents in MM. Pre-defined.
	//---------------------------------------------------------------------------
	public String getPathOfConfigToPlay() {
		
		checkAndMakeDir(adPkgCurrentStorage);
		return adPkgCurrentStorage;
	}
	
	//---------------------------------------------------------------------------	
	// To make sure File Path. Directory path will be made if there is no directory.
	//---------------------------------------------------------------------------
	public void checkAndMakeDir(String directoryPath){
		
		   File targetDir = new File(directoryPath);
		   
		   if(!(targetDir.exists())){
			   targetDir.mkdirs();
		   }
	}
	
	//---------------------------------------------------------------------------	
	// To check whether the file exists or not. If there is not file, an error will occur.
	//---------------------------------------------------------------------------
	public boolean checkFileExist(String fileName){
		
			File f= new File(fileName);
			
			   if(!(f.exists())){
				   System.out.println("File not exist!");
				   return false; 
			   }
			   else{
				   return true;
			   }
	}

/*
	Copy Files and directories (sub-directories and files)
	Mr.Gold	
*/
	public boolean copyFileDirectory(File sourceFile, File destFile){	  
		
	   if (!sourceFile.exists())
		   return false;
	   
	   if (sourceFile.isDirectory())
	   {
		   if (!destFile.exists())
			   destFile.mkdirs();
	   
	       String[] lists = sourceFile.list();
	       for (String list : lists) {
	    	   copyFileDirectory(new File(sourceFile, list), new File(destFile, list));
	       }	   
	   } else {	// file
		   try {
			   FileInputStream 	fis = new FileInputStream(sourceFile);   
			   FileOutputStream fos = new FileOutputStream(destFile);			   
			   
			   // Copy the bits from instream to outstream
			   
			   byte[] buf = new byte[1024];
			   int len;
			   
			   while ((len = fis.read(buf)) > 0) {
				   fos.write(buf, 0, len);
			   }

			   fis.close();
			   fos.close();
			   fos.flush();
//			   FileDescriptor fd = fos.getFD();
//			   fd.sync();
		   } catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
		   } catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
		   }
	   }
	   
       return true;
	}
/*
	Delete directory and its all sub directories and files.
	Mr.Gold	
*/
	public boolean deleteFilesInDirectory(File path){	  
		
	   if (!path.exists())
		   return false;
	   
	   if (!path.isDirectory())
		   return false;
	   
        File[] files = path.listFiles();
        
        for (File file : files) {
            if (file.isFile()) {
            	file.delete();
            } 
        }

        return true;
	}
/*
	Move the sourceFile to destinationFile.
	Mr.Gold	
*/	
	public boolean moveSingleFile(File sourceFile, File destinationFile) {
		if (!sourceFile.exists()) {
			return false;
		}
	 
		if (destinationFile.exists()) {				// Destination file.
			while (!destinationFile.delete()) {		// Delete Destination file.
				try {
					Thread.sleep(100);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}			
		}
		
		// Copy new sourceFile to destinationFile
		this.copyFileDirectory(sourceFile, destinationFile);	
		
		// Delete sourceFile
		while (!sourceFile.delete()) {		
			try {
				Thread.sleep(100);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}			
			
		return true;
	}

	

	//---------------------------------------------------------------------------	
	// To do validation of contents whether contents exist or not in MM after unpacking AD package.
	//---------------------------------------------------------------------------
	public boolean validationOfContent(String configContentPath){
		
		System.out.println("validationOfContent()");
		    
		//Vector<String> tarFiles = getFileNames(adPkgCurrentStorage, "xml");
		Vector<String> tarFiles = getFileNames(configContentPath, "xml");
		
	    for(int a =0; a < tarFiles.size(); a++){
	    	System.out.println(tarFiles.elementAt(a));
			getPathAndNameOfContentsInMm(tarFiles.elementAt(a));
	    }
	    	    
	    // Validation of content
	    for(int a = 0 ; a < contentsNamePathInMm.length; a++){
	    	boolean fileIs = true;	   
	    	//String fullName = adPkgCurrentStorage + "/";
	    	String fullName = configContentPath + "/";
	    	fullName = fullName + contentsNamePathInMm[a];
	    	fileIs = checkFileExist(fullName);
	    	if(fileIs == false){
	    		System.out.println("Content File - " + contentsNamePathInMm[a] + " is not exist!!!!");
	    		return false;
	    	}	    	
	    }
		
		return true;//OK! No problem!
	}
	
	//---------------------------------------------------------------------------	
	// To get file name in a specific directory for removing directory path of contents in XML
	//---------------------------------------------------------------------------
	 public Vector<String> getFileNames(String targetDirName, String fileExt) {   
	    Vector<String> fileNames = new Vector<String>();   
	    File dir = new File(targetDirName);   
	    fileExt = fileExt.toLowerCase();   
	     
	    if(dir.isDirectory()) {   
	        String dirName = dir.getPath();   
	        String[] filenames = dir.list(null);   
	        int cntFiles = filenames.length;   
	          
	        for(int iFile=0; iFile<cntFiles; ++iFile) {   
	            String filename = filenames[iFile];   
	            String fullFileName = dirName + "/" + filename;   
	            File file = new File(fullFileName);   
	    
	            boolean isDirectory = file.isDirectory();   
	            if(!isDirectory && filename.toLowerCase().endsWith(fileExt)) {   
	                fileNames.add(fullFileName);   
	            }   
	        }   
	    }   
	  
	    return fileNames;   
	 }  

	//---------------------------------------------------------------------------	
	// To get Information of MM
	//---------------------------------------------------------------------------
	 public long getMarqueeID(File file){
		 
		 	long marqueeID = -1;
		 	String[] tempMarqueeID = {"-1", "-1", "-1"};
		 	
		    FileInputStream fis = null;
		    BufferedInputStream bis = null;
		    DataInputStream dis = null;

		    try {
		      fis = new FileInputStream(file);

		      // Here BufferedInputStream is added for fast reading.
		      bis = new BufferedInputStream(fis);
		      dis = new DataInputStream(bis);

		      int lineCounter = 0;
		      // dis.available() returns 0 if the file does not have more lines.
		      while (dis.available() != 0) {
		       // this statement reads the line from the file and print it to the console.
			    	tempMarqueeID[lineCounter] = dis.readLine();
			    	lineCounter++;
		      }
		      
		    	marqueeID = Long.parseLong(tempMarqueeID[0]);
		        System.out.println(marqueeID);
		        //System.out.println(dis.readLine());
		        
		      // dispose all the resources after using them.
		      fis.close();
		      bis.close();
		      dis.close();

		    } catch (FileNotFoundException e) {
		      e.printStackTrace();
		      return marqueeID;
		    } catch (IOException e) {
		      e.printStackTrace();
		      return marqueeID;
		    }
		    
		 return marqueeID;
	 }
	 
	//---------------------------------------------------------------------------	
	// To get Information of Config Tool Server
	//---------------------------------------------------------------------------
	 @SuppressWarnings("deprecation")
	public String getServerIP(File file){
		 String[] serverIP = {"000.000.000.000", "000.000.000.000","000.000.000.000"}; //default (ERROR!!!)
		 
		    FileInputStream fis = null;
		    BufferedInputStream bis = null;
		    DataInputStream dis = null;

		    try {
		      fis = new FileInputStream(file);

		      // Here BufferedInputStream is added for fast reading.
		      bis = new BufferedInputStream(fis);
		      dis = new DataInputStream(bis);

		      int lineCounter = 0;
		      // dis.available() returns 0 if the file does not have more lines.
		      while (dis.available() != 0) {
		       // this statement reads the line from the file and print it to
		       // the console.
			    	serverIP[lineCounter] = dis.readLine();
			    	lineCounter++;
		      }
		      
		        System.out.println(serverIP[1]);
		        //System.out.println(dis.readLine());

		      // dispose all the resources after using them.
		      fis.close();
		      bis.close();
		      dis.close();

		    } catch (FileNotFoundException e) {
		      e.printStackTrace();
		      return serverIP[1];
		    } catch (IOException e) {
		      e.printStackTrace();
		      return serverIP[1];
		    }
		 
		 return serverIP[1];
	 }
	 
	//---------------------------------------------------------------------------	
	// To get Information of Config Tool Server
	//---------------------------------------------------------------------------
	 public int getServerPort(File file){
		 int serverPort = 0;
		 String[] tempServerPort = {"0", "0", "0"};
		 
		    FileInputStream fis = null;
		    BufferedInputStream bis = null;
		    DataInputStream dis = null;

		    try {
		      fis = new FileInputStream(file);

		      // Here BufferedInputStream is added for fast reading.
		      bis = new BufferedInputStream(fis);
		      dis = new DataInputStream(bis);

		      int lineCounter = 0;
		      // dis.available() returns 0 if the file does not have more lines.
		      while (dis.available() != 0) {
		       // this statement reads the line from the file and print it to the console.
			    	tempServerPort[lineCounter] = dis.readLine();
			    	lineCounter++;
		      }
		      
		    	serverPort = Integer.parseInt(tempServerPort[2]);
		        System.out.println(serverPort);
		        //System.out.println(dis.readLine());

		      // dispose all the resources after using them.
		      fis.close();
		      bis.close();
		      dis.close();

		    } catch (FileNotFoundException e) {
		      e.printStackTrace();
		      return serverPort;
		    } catch (IOException e) {
		      e.printStackTrace();
		      return serverPort;
		    }
		 
		 return serverPort;
	 }
}
