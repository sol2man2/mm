package marquee.adlib.utils;

import java.io.File;
import java.io.IOException;

public class MarqueeResourcePath {
	ConfigFileUtils utils = new ConfigFileUtils();

	private String MARQUEE_ADPKG_CURRENT_PATH;	// path used in marquee with current resources.
	private String MARQUEE_ADPKG_TEMP_PATH;		// path used in marquee with updating resources.
	private String MARQUEE_ADPKG_BACKUP_PATH;	// path used in marquee with backup resources.
	private String MARQUEE_SWPKG_CURRENT_PATH;	// path used in marquee with current software.
	private String MARQUEE_SWPKG_TEMP_PATH;		// path used in marquee with updating software.
	private String MARQUEE_SWPKG_BACKUP_PATH;	// path used in marquee with backup software.
	private String MARQUEE_DOWNLOAD_PATH;		// path used in updater with adpackage or downloaded software.
	private String MARQUEE_CONFIG_PATH;			// path used in updater with device configurations.

	private static MarqueeResourcePath resPath;

	MarqueeResourcePath() {
//		String path = this.getClass().getResource("").getPath();
		File file = new File("./");
		String path = null;
		try {
			path = file.getCanonicalPath();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		MARQUEE_ADPKG_CURRENT_PATH 	= path + "/adpackage";
		MARQUEE_ADPKG_TEMP_PATH 	= path + "/temp/adpackage";
		MARQUEE_ADPKG_BACKUP_PATH	= path + "/backup/adpackage";

		MARQUEE_SWPKG_CURRENT_PATH 	= path;
		MARQUEE_SWPKG_TEMP_PATH 	= path + "/temp/swpackage";
		MARQUEE_SWPKG_BACKUP_PATH 	= path + "/backup/swpackage";
		
		MARQUEE_DOWNLOAD_PATH		= path + "/download";
		MARQUEE_CONFIG_PATH			= path + "/config";
	}

	public static MarqueeResourcePath getInstance() {
		if (MarqueeResourcePath.resPath == null) {
			resPath = new MarqueeResourcePath();
		}
		
		return resPath;
	}
	
	public String getAdPkgCurrentPath() {
	    //To make sure File Path. Directory path will be made if there is no directory.
		utils.checkAndMakeDir(MARQUEE_ADPKG_CURRENT_PATH); 	    
		return MARQUEE_ADPKG_CURRENT_PATH;
	}
	public String getAdPkgTempPath() {
		utils.checkAndMakeDir(MARQUEE_ADPKG_TEMP_PATH); 
		return MARQUEE_ADPKG_TEMP_PATH;
	}
	public String getAdPkgBackupPath() {
		utils.checkAndMakeDir(MARQUEE_ADPKG_BACKUP_PATH); 
		return MARQUEE_ADPKG_BACKUP_PATH;
	}
	public String getSwPkgCurrentPath() {
		utils.checkAndMakeDir(MARQUEE_SWPKG_CURRENT_PATH); 
		return MARQUEE_SWPKG_CURRENT_PATH;
	}
	public String getSwPkgTempPath() {
		utils.checkAndMakeDir(MARQUEE_SWPKG_TEMP_PATH); 
		return MARQUEE_SWPKG_TEMP_PATH;
	}
	public String getSwPkgBackupPath() {
		utils.checkAndMakeDir(MARQUEE_SWPKG_BACKUP_PATH); 
		return MARQUEE_SWPKG_BACKUP_PATH;
	}
	public String getDownloadPath() {
		utils.checkAndMakeDir(MARQUEE_DOWNLOAD_PATH); 
		return MARQUEE_DOWNLOAD_PATH;
	}
	public String getConfigPath() {
		utils.checkAndMakeDir(MARQUEE_CONFIG_PATH); 
		return MARQUEE_CONFIG_PATH;
	}
}
