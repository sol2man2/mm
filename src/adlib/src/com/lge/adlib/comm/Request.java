package com.lge.adlib.comm;

import java.io.Serializable;

public abstract class Request implements Serializable {
	protected long version;

	public long getVersion() {
		return version;
	}

	public void setVersion(long version) {
		this.version = version;
	}
}
