package com.lge.adlib.comm;

import java.io.Serializable;

import com.lge.adlib.comm.Response.ResponseCode;

public class Acknowledge implements Serializable {
	protected long version;
	private AcknowledgeCode responseCode;

	public AcknowledgeCode getResponseCode() {
		return responseCode;
	}

	public void setResponseCode(AcknowledgeCode responseCode) {
		this.responseCode = responseCode;
	}

	public static enum AcknowledgeCode {
		SUCCESS,
		ERROR_UNKNOWN
	}

}
