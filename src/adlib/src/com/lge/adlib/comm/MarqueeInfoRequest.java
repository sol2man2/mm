package com.lge.adlib.comm;

import java.io.Serializable;

import marquee.adlib.utils.MarqueeEntityInfo;

public class MarqueeInfoRequest extends Request implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private MarqueeEntityInfo marqueeEntityInfo = null;
	
	public MarqueeEntityInfo getMarqueeEntityInfo() {
		return marqueeEntityInfo;
	}

	public void setMarqueeEntityInfo(MarqueeEntityInfo marqueeEntityInfo) {
		this.marqueeEntityInfo = marqueeEntityInfo;
	}

	public MarqueeInfoRequest(MarqueeEntityInfo marqueeEntityInfo) {
		this.marqueeEntityInfo = marqueeEntityInfo;
	}

}
