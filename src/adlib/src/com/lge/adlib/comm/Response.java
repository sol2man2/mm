package com.lge.adlib.comm;

import java.io.Serializable;

public abstract class Response implements Serializable {
	protected long version;
	protected ResponseCode responseCode;
	protected String fileName;
	protected long fileSize;

	public static enum ResponseCode {
		EXISTENCE_NEW_UPDATE,
		NO_EXISTENCE_NEW_UPDATE,
		UNKNOWN
	}
	
	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public long getFileSize() {
		return fileSize;
	}

	public void setFileSize(long fileSize) {
		this.fileSize = fileSize;
	}

	public long getVersion() {
		return version;
	}

	public void setVersion(long version) {
		this.version = version;
	}

	public ResponseCode getResponseCode() {
		return responseCode;
	}

	public void setResponseCode(ResponseCode responseCode) {
		this.responseCode = responseCode;
	}
}
