package com.lge.updater;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.ObjectInputStream;

import marquee.adlib.utils.ConfigFileUtils;
import marquee.adlib.utils.MarqueeEntityInfo;
import marquee.adlib.utils.MarqueeResourcePath;

public class ApplicationUpdaterMain {
	private 	String 		marqueeInfoDB 	= null;
	
	private 	MarqueeEntityInfo 	marqueeDeviceInfo	= null;

	
	public ApplicationUpdaterMain() {
		super();

		this.marqueeInfoDB	= MarqueeResourcePath.getInstance().getConfigPath() + "/marqueeInfo.dat";
		this.loadData();	
	}

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		
		ApplicationUpdaterMain 	applicationUpdater	= new ApplicationUpdaterMain();

		ConfigFileUtils myUtils 		= new ConfigFileUtils();		
		
		String path4Application			= MarqueeResourcePath.getInstance().getSwPkgCurrentPath();
		String path4ApplicationTemp		= MarqueeResourcePath.getInstance().getSwPkgTempPath();

		String path4Package				= MarqueeResourcePath.getInstance().getAdPkgCurrentPath();
		String path4PackageTemp			= MarqueeResourcePath.getInstance().getAdPkgTempPath();

		File file			= null;
		File newFile 		= null;
		File currentFile 	= null;
		
		file = new File (path4ApplicationTemp);
		if (!file.exists())	file.mkdirs();

		file = new File (path4Package);
		if (!file.exists())	file.mkdirs();

		file = new File (path4PackageTemp);
		if (!file.exists())	file.mkdirs();

		// New Application
		newFile 	= new File(path4ApplicationTemp, applicationUpdater.marqueeDeviceInfo.getMarqueeApplication());	
		currentFile = new File(path4Application, applicationUpdater.marqueeDeviceInfo.getMarqueeApplication());	
		
		try {
			Thread.sleep(500);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		// Delete current application
		while (!myUtils.deleteFilesInDirectory(new File(path4Application))) {
			try {
				Thread.sleep(100);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		// Restore new application
		myUtils.moveSingleFile(newFile, currentFile);
		
		
		// Check new Package
		if (applicationUpdater.marqueeDeviceInfo.isbIsNewPackage()) {
			// New Package
			newFile 	= new File(path4PackageTemp, applicationUpdater.marqueeDeviceInfo.getMarqueePackage());
			if (newFile.exists()) {
				
				if (myUtils.validationOfContent(path4PackageTemp)) {
					while (!myUtils.deleteFilesInDirectory(new File(path4Package))) {
						try {
							Thread.sleep(100);
						} catch (InterruptedException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					}
					
					currentFile = new File(path4Package, applicationUpdater.marqueeDeviceInfo.getMarqueePackage());				
					myUtils.moveSingleFile(newFile, currentFile);			
				}
			}
		}
	
		while (!myUtils.deleteFilesInDirectory(new File(path4ApplicationTemp))) {
			try {
				Thread.sleep(100);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		while (!myUtils.deleteFilesInDirectory(new File(path4PackageTemp))) {
			try {
				Thread.sleep(100);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		// Launch new application.
		ProcessBuilder pb = new ProcessBuilder("java", "-jar", applicationUpdater.marqueeDeviceInfo.getMarqueeApplication());
		pb.directory(new File(path4Application));
		try {
			pb.start();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
			
		// sb94 플래그 파일 삭제 추가
		File flagFile = new File(".\\download.txt");
		if(flagFile.exists()){
			flagFile.delete();
		}
		// Suicide
		System.exit(0);		
	}
	
	private boolean loadData() {
		File file = new File(this.marqueeInfoDB);
		
		if( !file.exists() )
			return false;
		
		FileInputStream fis 	= null;
		ObjectInputStream ois 	= null;
		
		Object obj = null;
		
		try {
			
			fis = new FileInputStream(file);
			ois = new ObjectInputStream(fis);

			obj = ois.readObject();

			ois.close();
			fis.close();
			
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		this.marqueeDeviceInfo = (MarqueeEntityInfo) obj;
		
		return true;
	}
}
