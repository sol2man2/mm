/**
 * Class : PersonManager
 */

package workshop.person.control;

import workshop.person.entity.PersonEntity;

/**
 * Person 정보를 Display하기위한 Control Class
 */
public class PersonManager {

    /**
     PersonManager Default Constructor
     */
    public PersonManager() {

    }

    /**
     * 본 프로그램의 시작 위치
     * @param args
     */
    public static void main( String[] args ) {

        PersonManager personManager = new PersonManager();

        personManager.printTitle( "              @@@ 인물 정보 조회 시스템 @@@" );
        personManager.printTitleLine();

        PersonEntity[]  persons = new  PersonEntity[10];

        personManager.fillPersons( persons );
        
        personManager.showPerson( persons ); 

        char gender = '여';
        
        System.out.println( " 성별 : '" + gender + "'(은)는  " + personManager.findByGender( persons, gender ) + "명 입니다. " );
      
        personManager.printTitleLine();
        
        personManager.showPerson( persons, "김하늘" ); 

     }

    /**
     * PersonEntity[]의  정보를 set한다.
     * @param persons
     */
 
    public void fillPersons( PersonEntity[]  persons ){
        
        persons[0] = new PersonEntity( "이성호", "7212121028102", "인천 남동구", "032-392-2932" );
        persons[1] = new PersonEntity( "김하늘", "7302132363217", "서울 강동구", "02-362-1932" );
        persons[2] = new PersonEntity( "박영수", "7503111233201", "서울 성북구", "02-887-1542" );
        persons[3] = new PersonEntity( "나인수", "7312041038988", "대전 유성구", "032-384-2223" );
        persons[4] = new PersonEntity( "홍정수", "7606221021341", "서울 양천구", "02-158-7333" );
        persons[5] = new PersonEntity( "이미숙", "7502142021321", "서울 강서구", "02-323-1934" );
        persons[6] = new PersonEntity( "박성구", "7402061023101", "인천 중구", "032-327-2202" );
        persons[7] = new PersonEntity( "유성미", "7103282025101", "서울 은평구", "02-452-0939" );
        persons[8] = new PersonEntity( "황재현", "7806231031101", "인천 중구", "032-327-2202" );
        persons[9] = new PersonEntity( "최철수", "7601211025101", "인천 계양구", "032-122-7832" ); 
    }
    
    /**
     * PersonEntity[]의  정보를 화면에 보여준다.
     * @param persons
     */
    public void showPerson( PersonEntity[] persons ) {

        for( PersonEntity person : persons ){
            System.out.println( " [이름] " + person.getName() + 
                                          "    [성별] " +  person.getGender() + 
                                          "    [전화번호] " +  person.getPhone() );
            printItemLine();
        }
    }

    /**
     * PersonEntity[]의  정보 중  param의 gender의 명수를 return해 준다.
     * @param persons
     * @param gender
     * @return count 
     */

    public int findByGender( PersonEntity[]  persons, char gender ){

        int count = 0;
        
        for ( PersonEntity person : persons ){
            if ( person.getGender() == gender ){
                count++;
            }
        }

        return count;
    }

    /**
     * PersonEntity[]의  정보 중  param의 name과 일치하는 person의 상세정보를 보여준다.
     * @param persons
     * @param name
     */
    public void showPerson( PersonEntity[]  persons, String name ){
        System.out.println( "\n-- 이름 : '" + name + "'(으)로 찾기 결과입니다. --" );
        
        for ( PersonEntity person : persons ){
            if ( person.getName().equals( name ) ){
                printItemLine();
                
                System.out.println( " [이름] " + person.getName()  );
                System.out.println( " [성별] " + person.getGender() );
                System.out.println( " [전화번호] " + person.getPhone() );
                System.out.println( " [주소] " + person.getAddress() ); 
            }
        }
    }
    
    /**
     * 타이틀을 출력한다.
     * @param title
     */
    public void printTitle( String title ) {

        System.out.println( "\n" + title + "\n" );
    }

    /**
     * 타이틀 밑줄을 출력한다.
     */
    public void printTitleLine() {

        System.out.println( "==================================================" + "==========" );

    }

    /**
     * 아이템 경계 줄을 출력한다.  
     */
    public void printItemLine() {

        System.out.println( "--------------------------------------------------" + "----------" );

    }
}
