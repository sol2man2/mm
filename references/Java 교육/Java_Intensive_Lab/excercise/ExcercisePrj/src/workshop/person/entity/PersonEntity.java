/**
 * Class : PersonEntity
 */

package workshop.person.entity;

/** 
 * Person의 정보를 담는 Entity Class
 */
public class PersonEntity {

    /**
     * Person의 이름을 저장하는 멤버변수
     */
    private String name;

    /**
     * Person의 성별을 저장하는 멤버변수
     */
    private char gender;
 
    /**
     * Person의 주민번호를 저장하는 멤버변수
     */
    private String ssn;

    /**
     * Person의 주소를 저장하는 멤버변수
     */
    private String address;

    /**
     * Person의 전화번호를 저장하는 멤버변수
     */
    private String phone;

    /**
     * PersonEntity의 Default Constructor
     */
    public PersonEntity() {

    }

    /**
     * PersonEntity의 Constructor로 name, ssn, address, phone을 Argument로 받아서 
     * 객체를 생성한다.
     * @param name
     * @param ssn
     * @param address
     * @param phone
     */
    public PersonEntity( String name, String ssn, String address, String phone ) {

        setName( name );
        setSSN( ssn );
        setAddress( address );
        setPhone( phone );
    }

    /**
     * 멤버변수 name의 Getter
     * @return String
     */
    public String getName() {

        return name;
    }

    /**
     * 멤버변수 gender의 Getter
     * @return char
     */
    public char getGender() {

        return gender;
    }

    /**
     * 멤버변수 ssn의 Getter
     * @return String
     */
    public String getSSN() {

        return ssn;
    }

    /**
     * 멤버변수 address의 Getter
     * @return String
     */
    public String getAddress() {

        return address;
    }

    /**
     * 멤버변수 phone의 Getter
     * @return String
     */
    public String getPhone() {

        return phone;
    }

    /**
     * 멤버변수 name의 Setter
     * @param name
     */
    public void setName( String name ) {

        this.name = name;
    }

    /**
     * 멤버변수 ssn의 Setter
     * @param ssn
     */
    public void setSSN( String ssn ) {

        this.ssn = ssn;

        if ( (ssn.charAt( 6 ) == '1') || (ssn.charAt( 6 ) == '3') )
            setGender( '남' );
        else
            setGender( '여' );
    }

    /**
     * 멤버변수 gender의  Setter
     * @param gender
     */
    public void setGender( char gender ) {

        this.gender = gender;

    }

    /**
     * 멤버변수 address의 Setter
     * @param address
     */
    public void setAddress( String address ) {

        this.address = address;
    }

    /**
     * 멤버변수 phone의 Setter
     * @param phone
     */
    public void setPhone( String phone ) {

        this.phone = phone;
    }
}
