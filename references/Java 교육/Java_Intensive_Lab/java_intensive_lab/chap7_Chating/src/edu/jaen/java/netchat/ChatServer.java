package edu.jaen.java.netchat;
import java.io.*;
import java.net.*;
import java.util.*;

public class ChatServer{

	private ArrayList users= new ArrayList();

	private int port=5500;

	public void go() {
		
		try{
		//1. ServerSocket 생성
		ServerSocket ss = new ServerSocket( port );
		System.out.println("ServerSocket 생성 성공. port="+port);
		while( true ) {
			//2. Socket생성: Client 접속시
			
			// 구현해 보세요.
			
			//3. 각각 Client에 I/O Stream

			// 구현해 보세요.
			
			//4. Client 정보 저장

			// 구현해 보세요.
			
			//5. 각각의 Client로 부터 입력을 담당하는 Thread생성

			// 구현해 보세요.
			
		}
		}catch(Exception ee ) {
			System.out.println("오류발생 ... : "+ee.getMessage() );
		}
	}

	public void broadcast( String msg ) {
		
		
		// 구현해 보세요.
		
	}

	public void removeClient( ObjectInputStream ois ) {
		for( int i=0; i<users.size(); i++){
			User u=(User)users.get(i);
			ObjectInputStream us = u.getObjectInputStream();
			if(ois == us){
				try{	
					us.close();
					u.getObjectOutputStream().close();
					u.getSocket().close();
				}catch( Exception e ) {}
				users.remove(u); 
				System.out.println("1 client exited !!");
			}
		}
	}

	public static void main(String[] args) 	{
			// ChatServer cs = new ChatServer(port);
			// cs.go();
			new ChatServer().go();
	}

class ChatServerThread extends Thread {
	private ObjectInputStream ois ;

	public ChatServerThread(  ObjectInputStream ois){
		this.ois=ois;
	}

	public void run(){

		// 구현해 보세요.
		
	}
}
}


class User{
	private Socket s;
	private ObjectInputStream ois;
	private ObjectOutputStream oos;
	
	public User(Socket s, ObjectInputStream ois,ObjectOutputStream oos ){
		this.s = s;
		this.ois=ois;
		this.oos=oos;
	}
	public Socket getSocket(){ return s; }
	public ObjectInputStream getObjectInputStream(){ return ois; }
	public ObjectOutputStream getObjectOutputStream(){ return oos; }
}

