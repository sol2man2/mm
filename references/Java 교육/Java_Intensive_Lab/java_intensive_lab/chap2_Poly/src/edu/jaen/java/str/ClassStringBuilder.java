package edu.jaen.java.str;
public class ClassStringBuilder{
        public static void main(String args[]){
                
        	StringBuilder str = new StringBuilder("StringBuilder");

             str.append("AAA");		//문자열 끝에 문자열 추가
             System.out.println("append('AAA') = " + str);
                
             str.insert(0, "BBB");	//특정 index에 문자열 insert
             System.out.println("insert(0, 'BBB') = " + str);

             str.setCharAt(0, 'X');	//특정위치의 문자 변경
             System.out.println("setCharAt(0, 'X') = " + str);
                
             str.reverse();			//순서를 바꿈
             System.out.println("reverse() = " + str);
                
             str.setLength(30);		//문자열 길이 설정           
             System.out.println("setLength(30) = " + str);
        }
}