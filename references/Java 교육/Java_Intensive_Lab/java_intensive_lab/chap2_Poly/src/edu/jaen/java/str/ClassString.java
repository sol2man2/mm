package edu.jaen.java.str;
public class ClassString{
  public static void main(String args[]){
               
    String str = " This is a String ";
                
    /***** 새로운 문자열 생성 *****/
    String c1 = str.concat("AAA"); //문자열 연결 ' +' 와 같다.
	if (str==c1)
	   System.out.println("str==c1");
	String c2 = str.replace('T', 'O'); //문자열에 있는 특정문자를 다른문자로 치환
    String c3 = str.substring(2,5); //문자열중 일부 추출
    String c4 = str.toLowerCase(); //모든 문자를 소문자로바꿈
    String c5 = str.toUpperCase(); //모든 문자를 대문자로 바꿈
    String c6 = str.trim(); //문자열의 앞뒤 여백을 제거
                
   /***** 검색 *****/
    boolean s1 = str.endsWith("String "); // 특정문자열로 끝나는지의 여부
    boolean s2 = str.startsWith(" This"); //특정 문자열로 시작하는 지의 여부
    int s3 = str.indexOf('i'); //특정문자가 처음 나타나는 위치
    int s4 = str.indexOf('i', s3+1); //특정문자가 두번째 나타나는 위치
    int s5 = str.lastIndexOf('i', s4-1); //특정문자가 두번째 나타나는 위치에서 역순으로 검색
                                
    /***** comparsion *****/
    boolean p1 = str.equals(" This is a String "); //문자열 비교
    boolean p2 = str.equalsIgnoreCase(" this is a string "); //대소문자 구별없이 문자열 비교
    int p3 = str.compareTo(" String "); //알파벳의 순서 비교
              
    /***** others *****/
    char o1 = str.charAt(2); //특정위치의 문자
    int o2 = str.length(); //문자열 길이

    System.out.println("c1 = " + c1);
    System.out.println("c2 = " + c2);
    System.out.println("c3 = " + c3);
    System.out.println("c4 = " + c4);
    System.out.println("c5 = " + c5);
    System.out.println("c6 = " + c6);

    System.out.println("s1 = " + s1);
    System.out.println("s2 = " + s2);
    System.out.println("s3 = " + s3);
    System.out.println("s4 = " + s4);
    System.out.println("s5 = " + s5);
                
    System.out.println("p1 = " + p1);
    System.out.println("p2 = " + p2);
    System.out.println("p3 = " + p3);
                
    System.out.println("o1 = " + o1);
    System.out.println("o2 = " + o2);
   }
}