package edu.jaen.java.str;
class EqString 
{
	public static void main(String[] args) 
	{	String st1="seoul";
		String st2="seoul";
		String st3=new String("seoul");
		String st4=new String("seoul");
	
		if (st1==st2)
			System.out.println("st1==st2");
		if (st2==st3)
			System.out.println("st2==st3");
		if (st3==st4)
			System.out.println("st3==st4");
		if (st1.equals(st2))
			System.out.println("st1.equals(st2)");
		if (st2.equals(st3))
			System.out.println("st2.equals(st3)");
		if (st3.equals(st4))
			System.out.println("st3.equals(st4)");
		}
}
