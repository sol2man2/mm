package edu.jaen.java.modifier;
class StaticTest {
        static int count;
        static void setCount(int k) {
                count = k;
                System.out.println("setCount() : " + count);
        }
       static {
                System.out.println("static initialize! count: " + count);
        }

		{ System.out.println("instance initialize...");}

	public static void main(String args[]) {
			System.out.println("Main Start!");
			//StaticTest.setNum(10);
			StaticTest si=new StaticTest();
			System.out.println("Main...");
			StaticTest si2=new StaticTest();
		}
}
