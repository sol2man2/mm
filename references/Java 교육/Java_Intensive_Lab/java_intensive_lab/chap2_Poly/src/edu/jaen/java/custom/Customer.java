package edu.jaen.java.custom;
public class Customer {
	private int num;
	private String name;

	public Customer() { }
	public Customer(int num, String name){
		//this.num=num;
		setNum(num);
		//this.name=name;
		setName(name);
	}
	public void setNum(int num){
		//입력형식, 입력값의 범위에 맞나 체크
		this.num=num;
	}


	public int getNum(){
		//출력 권한 체크
		return num;
	}
	public void setName(String name){
		this.name=name;
	}
	public String getName(){
		return name;
	}
	@Override
	public String toString(){
		String str="번호 : "+getNum()+"\n이름 : "+getName();
		return str;
	}
   @Override
	public boolean equals(Object ob){
		if(ob !=null && ob instanceof Customer){
			Customer cu=(Customer)ob;
			if(num==cu.num && name.equals(cu.name)){
				return true;
			}
		}
		return false;
	}
   @Override
	public int hashCode(){
		Integer in=new Integer(getNum());
		return in.hashCode() ^ getName().hashCode();
	}
}
