package edu.jaen.java.util;
import java.util.*;

public class CurrentTime {
	static public  void main(String args[]){
 		Calendar rightNow = Calendar.getInstance();
		int hour = rightNow.get(Calendar.HOUR);
		int min = rightNow.get(Calendar.MINUTE);
		int sec = rightNow.get(Calendar.SECOND);
 		int year = rightNow.get(Calendar.YEAR);
		int month = rightNow.get(Calendar.MONTH);
		int date = rightNow.get(Calendar.DATE);
		System.out.println("현재 시간은 " + year + "년 " + 
                (month+1) + "월 " + date + "일");
		System.out.println("현재 시간은 " + hour + "시 " + 
                min + "분 " + sec + "초");

	}
}
