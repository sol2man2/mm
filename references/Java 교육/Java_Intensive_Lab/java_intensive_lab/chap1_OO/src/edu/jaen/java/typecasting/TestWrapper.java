package edu.jaen.java.typecasting;
public class TestWrapper{
        public static void main(String args[]){
				int a=300; 
			//Primitive==>Object
				Integer in=new Integer(a);
			//Object==> primitive
				a=in.intValue();
				System.out.println(a);
			//Object ==> String 
				String st = in.toString();
			//Primitive ==>String
               	st=new Integer(a).toString();
                st = String.valueOf(a);
				st=a+"";
                System.out.println(st);
			//String ==> Object
                Integer it = Integer.valueOf(st);
                Double db = Double.valueOf(st);
                System.out.println(it);
                System.out.println(db);
			//String ==> primitive
                int i = Integer.parseInt(st);
                double d = Double.parseDouble("34.5");
                System.out.println(i);
                System.out.println(d);
        }
     }
