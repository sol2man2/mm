package edu.jaen.java.typecasting;
class Parent{
	int k=10;
	void over()	{
		System.out.println("Parent Class!");
	}
	void mParent(){
		System.out.println("Parent method");
	}
}
class Child   extends Parent{
	int k=20;
	void over() {   //Overriding
	
		System.out.println("Child class!");
	}
	void mChild(){
	
		System.out.println("Child method");
	}
}
class OverridingTest
{
	public static void main(String a[])
	{	
		Parent p=new Parent();
		System.out.println(p.k);
		p.over();
		p.mParent();

		Child c=new Child();
		System.out.println(c.k);
		c.over();
		c.mParent();
		c.mChild();

		//Parent p2=new Child(); //가능  문법:::: super클래스 변수 <== sub클래스
		Parent p2= c;
		System.out.println(p2.k);  //parent클래스 변수 값. parent에 없으면 에러
		p2.over();     //Child클래스 메서드 수행
		p2.mParent();
		//p2.mChild();   //ERROR
		
		//mChild() 수행 하고자 할 경우
		Child c2= (Child)p2;    //문법:::: sub클래스 변수 <== (sub클래스) super클래스
		c2.mChild();

	}
}