package edu.jaen.swing.ui1;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

public class ChooserTest extends JFrame
  implements ActionListener {

  private JButton goButton;
  private JLabel display;
  
  //현재 directory에서 열린다.
  //private JFileChooser jfc = new JFileChooser(".");
  //private JFileChooser jfc = new JFileChooser("C:/");//root c
  
  private JFileChooser jfc = new JFileChooser();//user's home directory
  public ChooserTest() {
	super("chooser");
   
		try{			
	       UIManager.setLookAndFeel (UIManager.getSystemLookAndFeelClassName ());
	   //    UIManager.setLookAndFeel (UIManager.getCrossPlatformLookAndFeelClassName ());
	      
		}catch(Exception c){c.printStackTrace ();}
     
	goButton = new JButton("파일"); 
	goButton.addActionListener(this);
    display = new JLabel("Chosen file will be shown here"); 
	
	/****************************filter 추가 **********************************/
	jfc.addChoosableFileFilter(new FileFilterDemo());
	
	getContentPane().add(goButton,"North");
    getContentPane().add(display,"South");
	setSize(300,300);
    setVisible(true);
  }

  public void actionPerformed(ActionEvent ae) 
  {
	 /** FileChooser를 연다. 선택하면 state값 return open(0), cancel(1)
	  *  int status = jfc.showDialog(this,"저장");//user가 button에 글씨줄수 있다.
	  */
	int status = jfc.showOpenDialog(this);//열기전용 Dialog 보여주기
	if (status == 0){ //열기버튼 클릭시
      display.setText("You chose: " + jfc.getSelectedFile().getName());
    }else {
      display.setText("File chooser was cancelled");
    }
  }//end actionPerformed()

  public static void main(String args[]) 
  {
    ChooserTest it = new ChooserTest();
  }
}
