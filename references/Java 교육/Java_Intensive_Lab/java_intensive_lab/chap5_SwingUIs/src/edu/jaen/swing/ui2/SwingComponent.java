package edu.jaen.swing.ui2;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

// Funtion  : Swing에서 사용되는 다양한 Component들을 연결하여 사용해 본다.
// Comment  : 입력되는 콤보박스,라디오버튼,체크박스,팝업메뉴,메뉴 등등이 있다.

public class SwingComponent extends JFrame implements ItemListener,ActionListener{
	// 버튼에 붙일 아이콘을 생성한다.
	Icon cutIcon,copyIcon,pasteIcon;
    JButton cutBtn,copyBtn,pasteBtn;
	JCheckBox cutChb,copyChb,pasteChb;
	JRadioButton cutRbtn,copyRbtn,pasteRbtn;
	// 라이오버튼과 체크박스 버튼용 버튼그룹
	ButtonGroup bg, bg1;
	// 콤보박스
	JComboBox airCbx,helCbx,shipCbx;
	// 콤보박스 데이타. 모델을 사용해도 된다.
	String[] cbxData = {"EF-2000","Rafale","F-15K","SU-37"};
	// 책에있는 클래스를 사용하기 위해서 사용.
	CutCopyPaste ccp;
	// 메뉴를 추가한다.
	JMenuBar mb;
	JMenu editMnu;
	JMenuItem cutMim,copyMim,pasteMim;
	// 팝업메뉴를 만든다.
	JPopupMenu jpm;
	JMenuItem cutPopMim,copyPopMim,pastePopMim;

	public SwingComponent(){

		ccp = new CutCopyPaste("SwingComponent");
		// Icon 생성
		cutIcon = new ImageIcon("images/cut.gif");
		copyIcon = new ImageIcon("images/copy.gif");
		pasteIcon = new ImageIcon("images/paste.gif");
		// 버튼생성
		cutBtn = new JButton(cutIcon);
		copyBtn = new JButton(copyIcon);
		pasteBtn = new JButton(pasteIcon);
		// 체크박스 생성
		cutChb = new JCheckBox(CutCopyPaste.CUT);
		copyChb = new JCheckBox(CutCopyPaste.COPY);
		pasteChb = new JCheckBox(CutCopyPaste.PASTE);
		// 라디오버튼 생성
		// 전부 true로 선정을 했을경우 가장 먼저 ButtonGroup에 등록되는 것이 설정이 된다.
		cutRbtn = new JRadioButton(CutCopyPaste.CUT, true);
		copyRbtn = new JRadioButton(CutCopyPaste.COPY, true);
		pasteRbtn = new JRadioButton(CutCopyPaste.PASTE, true);
		// 라디오버튼 묶을 그룹버튼 생성
		bg = new ButtonGroup();
		bg1 = new ButtonGroup();
		// 콤보생성
		airCbx = new JComboBox(cbxData);
		// 책의 내용을 이용하기 위해서 setActionCommand 사용
		cutBtn.setActionCommand(CutCopyPaste.CUT);
		copyBtn.setActionCommand(CutCopyPaste.COPY);
		pasteBtn.setActionCommand(CutCopyPaste.PASTE);

// 이벤트 등록
		cutBtn.addActionListener(ccp);  // 이게 나중에 실행. 먼저 등록한게 나중에 실행된다.
		cutBtn.addActionListener(this); // ==> 같은 이벤트를 다중으로 등록하는것도 가능하다. 둘다 실행된다.

		copyBtn.addActionListener(ccp);
		pasteBtn.addActionListener(ccp);
		// 체크박스 이벤트 등록
		cutChb.addItemListener(this);
		copyChb.addItemListener(this);
		pasteChb.addItemListener(this);
		// 라디오 버튼 이벤트 등록
		cutRbtn.addItemListener(this);
		copyRbtn.addItemListener(this);
		pasteRbtn.addItemListener(this);
		// 콤보박스에 새로운 내용을 넣기위해서 이벤트를 등록한다.
		airCbx.addItemListener(this);

//ToolTip 등록
		// 버튼 툴팁
		cutBtn.setToolTipText("Cut Button");
		copyBtn.setToolTipText("Copy Button");
		pasteBtn.setToolTipText("Paste Button");
		// 체크박스 툴팁
		cutChb.setToolTipText("Cut JCheckBox");
		copyChb.setToolTipText("Copy JCheckBox");
		pasteChb.setToolTipText("Paste JCheckBox");
		// 라디오버튼 툴팁
		cutRbtn.setToolTipText("Cut JRadioButton");
		copyRbtn.setToolTipText("Copy JRadioButton");
		pasteRbtn.setToolTipText("Paste JRadioButton");
		// 콤보박스 툴팁
		airCbx.setToolTipText("FX 도입 후보기종");
		// 입력을 위해 수정가능하게 설정
		airCbx.setEditable(true);

// 메뉴생성
		mb = new JMenuBar();

		editMnu = new JMenu("편집(E)");
		// 팝업메뉴 생성
		jpm = new JPopupMenu("팝업메뉴");
		// 메뉴 아이템 생성
		cutMim = new JMenuItem("<>잘라내기",cutIcon);
		copyMim = new JMenuItem("<>복사하기",copyIcon);
		pasteMim = new JMenuItem("<>붙여넣기",pasteIcon);
		// 팝업 메뉴 아이템 생성
		cutPopMim = new JMenuItem("<<잘라내기",cutIcon);
		cutPopMim.setBackground(Color.red);
		copyPopMim = new JMenuItem("<<복사하기",copyIcon);
		pastePopMim = new JMenuItem("<<붙여넣기",pasteIcon);
		// 메뉴 이벤트 등록
		cutMim.addActionListener(this);
		copyMim.addActionListener(this);
		pasteMim.addActionListener(this);
		// 팝업메뉴 이벤트 등록
		cutPopMim.addActionListener(this);
		copyPopMim.addActionListener(this);
		pastePopMim.addActionListener(this);
		// 메뉴 툴팁
		cutMim.setToolTipText("잘라내기");
		copyMim.setToolTipText("복사하기");
		pasteMim.setToolTipText("붙여넣기");
		// 메뉴 단축키
		editMnu.setMnemonic(KeyEvent.VK_E);  //Alt+E 에 메뉴 열림(focus 있을때)
		// 파워유저용 단축키
		cutMim.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_X, KeyEvent.CTRL_MASK,false));
		copyMim.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_C, KeyEvent.CTRL_MASK,false));
		pasteMim.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_V, KeyEvent.CTRL_MASK,false));
		// 메뉴 붙이기
		mb.add(editMnu);
		editMnu.add(cutMim);
		editMnu.add(copyMim);
		editMnu.add(pasteMim);
		
		// 팝업 메뉴 붙이기
		jpm.add(cutPopMim);
		jpm.add(copyPopMim);
		jpm.add(pastePopMim);
//		//jpm.setVisible(true);
//		// JPopupMene를 보여주는 부분으로 메뉴바 부분하고 콤보다음의 빈공간에서 생긴다.
//		// Why ? JFrame에 붙였기 때문이다. 다른 부분은 다른 컴포넌트들이 차지하고 있으니까
//		// 실질적인 이벤트가 JFrame이 아닌 해당 컴퍼넌트의 이벤트로 인식이 되기 때문이다.
		this.addMouseListener(new MouseAdapter(){
			public void mouseReleased(MouseEvent e){
				if(e.isPopupTrigger()){
					jpm.show(e.getComponent(), e.getX(), e.getY());
				}
			}
		});

		// 레이아웃 정한것만큼 컴포넌트를 채워넣지 않으면 빈칸으로 처리된다.
		this.getContentPane().setLayout(new GridLayout(4,3));
		// 버튼 붙이기
		this.getContentPane().add(cutBtn);
		this.getContentPane().add(copyBtn);
		this.getContentPane().add(pasteBtn);
		// JCheckBox 도 JRadioButton처럼 사용할 수 있다.
		/*bg1.add(cutChb);
		bg1.add(copyChb);
		bg1.add(pasteChb);*/
		// 체크박스 붙이기
		this.getContentPane().add(cutChb);
		this.getContentPane().add(copyChb);
		this.getContentPane().add(pasteChb);
		// 라디오버튼을 그룹으로 묶는다. 안묶으면? 체크박스처럼 된다.
		bg.add(cutRbtn);
		bg.add(copyRbtn);
		bg.add(pasteRbtn);
		// 라디오버튼 붙이기
		this.getContentPane().add(cutRbtn);
		this.getContentPane().add(copyRbtn);
		this.getContentPane().add(pasteRbtn);
		// 콤보박스 붙이기
		this.getContentPane().add(airCbx);
		// 메뉴 붙이기. 요렇게 붙이면 화면에 붙는다.
		//this.getContentPane().add(mb);
		this.setJMenuBar(mb);
		
		Font f = new Font("Serif", Font.BOLD+Font.ITALIC, 16);
		this.setFont(f);

		this.setVisible(true);
		this.pack();

		this.addWindowListener(new WindowAdapter(){
			public void windowClosing(WindowEvent e){
				System.exit(0);
			}
		});
	}
	// 메뉴 이벤트 처리
	public void actionPerformed(ActionEvent e){
		Object src = e.getSource();
		if(src == cutBtn)
			System.out.println("다중 등록 테스트 중이다.");
		if(src == cutMim || src == cutPopMim)
			System.out.println("Cut JMenuItem");
		else if(src == copyMim || src == copyPopMim)
			System.out.println("Copy JMenuItem");
		else if(src == pasteMim || src == pastePopMim)
			System.out.println("Paste JMenuItem");
	}
	// 체크박스,라디오버튼,콤보박스 이벤트 처리
	public void itemStateChanged(ItemEvent e){
		Object src = e.getSource();
		// 아이템을 선택했을 경우 이벤트 처리를 한다.
		// 그러지 않을경우 이벤트는 항상 두번씩 일어난다.
		// SELECTED와 DESELECTED 이렇게 두번일어난다.
		if(e.getStateChange() == ItemEvent.SELECTED)
			if(src == cutChb)
				System.out.println("Cut JCheckBox");
			else if(src == copyChb)
				System.out.println("Copy JCheckBox");
			else if(src == pasteChb)
				System.out.println("Paste JCheckBox");
			else if(src == cutRbtn)
				System.out.println("Current output device is "+cutRbtn.getText());
			else if(src == copyRbtn)
				System.out.println("Current output device is "+copyRbtn.getText());
			else if(src == pasteRbtn)
				System.out.println("Current output device is "+pasteRbtn.getText());
			else if(src == airCbx)
				// 선택을 하지 않고 입력일 경우 아이템을 추가한다.
				if(airCbx.getSelectedIndex() == -1){
					// 현재 입력한 아이템을 읽어와 추가한다.
					airCbx.addItem(e.getItem().toString());
					// 등록된 아이템을 알려주는 메세지 박스이다.
					JOptionPane.showMessageDialog(this,e.getItem().toString()+"이 등록되었습니다.","등록확인",
													JOptionPane.INFORMATION_MESSAGE);
			    }
				else // 현재 선택한 아이템에 관해서 출력해주는 문구이다.
					System.out.println("You choose "+ airCbx.getSelectedItem().toString() +" for FX");
	}

	public static void main(String[] args){
		new SwingComponent();
	}
}