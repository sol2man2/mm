package edu.jaen.swing.ui1;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

public class IFrameTest extends JPanel {

  public IFrameTest() {
		try{
			UIManager.setLookAndFeel (UIManager.getCrossPlatformLookAndFeelClassName ());
		//	UIManager.setLookAndFeel (UIManager.getSystemLookAndFeelClassName ());
		}catch(Exception c){c.printStackTrace ();}
		
	setLayout(new BorderLayout());
	JDesktopPane desktop=new JDesktopPane();
	JInternalFrame iframe=new JInternalFrame("첫번째",true,true,true,true);
	desktop.add(iframe);
	iframe.setBounds(50,50,200,100);
	iframe.setVisible(true);
	JInternalFrame iframe2=new JInternalFrame("두번째",true,true,true,true);
	desktop.add(iframe2);
	iframe2.setBounds(60,60,200,100);
	iframe2.setVisible(true);
	add(desktop, BorderLayout.CENTER); 
  
  }

  public static void main(String args[]) {
    JFrame jf = new JFrame("IFramePane Test");
    IFrameTest st = new IFrameTest();
    jf.getContentPane().add(st, BorderLayout.CENTER);
    jf.setSize(500,200);
    jf.setVisible(true);
  }
}

