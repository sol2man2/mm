package edu.jaen.swing.ui2;
import java.awt.event.*;
import javax.swing.*;

public class CutCopyPaste implements ActionListener{
	public static final String CUT = "CUT";
	public static final String COPY = "COPY";
	public static final String PASTE = "PASTE";
	String testMode;	

	public CutCopyPaste(String name){
		testMode = name;		
	}

	public void actionPerformed(ActionEvent e){
		if(testMode != null)
			if(e.getActionCommand() == CUT)
				System.out.println("Cut from "+testMode);
			else if(e.getActionCommand() == COPY)
				System.out.println("Copy from "+testMode);
			else if(e.getActionCommand() == PASTE)
				System.out.println("Paste from "+testMode);
	}
}
	
