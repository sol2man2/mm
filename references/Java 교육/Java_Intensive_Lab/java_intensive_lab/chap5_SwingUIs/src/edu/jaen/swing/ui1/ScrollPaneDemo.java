package edu.jaen.swing.ui1;
import javax.swing.*;
import java.awt.*;
import java.awt.event.*;

public class ScrollPaneDemo extends JFrame  
{
	public ScrollPaneDemo()
	{
		super("Demo");
		JLabel label=new JLabel("title",JLabel.CENTER);
		JTextArea area=new JTextArea();
		JScrollPane pane=new JScrollPane(area);
		pane.setVerticalScrollBarPolicy( JScrollPane.VERTICAL_SCROLLBAR_NEVER);
		pane.setColumnHeaderView(label);
		pane.setCorner(JScrollPane.UPPER_RIGHT_CORNER, new JButton(new ImageIcon("../images/open.gif")));
		pane.setCorner(JScrollPane.LOWER_RIGHT_CORNER, new JButton(new ImageIcon("../images/open.gif")));
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		getContentPane().add(pane,"Center");
		setBounds(300,300,500,400);
		setVisible(true);
	}
	public static void main(String[] args) 
	{
		new ScrollPaneDemo();
	}
}
