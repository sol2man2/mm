package edu.jaen.swing.ui3;

import java.awt.Insets;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JScrollPane;
import javax.swing.JTextPane;
import javax.swing.text.BadLocationException;
import javax.swing.text.Document;
import javax.swing.text.Style;
import javax.swing.text.StyleConstants;
import javax.swing.text.StyleContext;


// Funtion  : JTextPane에 다양한 폰트와 아인콘 그리고 버튼등을 추가해본다.
//Comment  : Style 객체를 이용하여 다양한 스타일을 지정할 수 있다.

public class TextPaneAdvance extends JFrame {
	private JTextPane textPane;
	private JScrollPane textScp;

	public TextPaneAdvance() {
		super("TextPane Test");
		textPane = new JTextPane();
		textScp = new JScrollPane(textPane);
		textScp.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);
		createTextPane();
		this.getContentPane().add(textScp);
		this.setSize(500, 500);
		this.setVisible(true);

		this.addWindowListener(new WindowAdapter() {
			public void windowClosing(WindowEvent e) {
				System.exit(0);
			}
		});
	}

	public JTextPane createTextPane() {
		String[] initString = {
				"This is an editable JTextPane, \n", // regular
				"another \n", // italic
				"styled \n", // bold
				"text \n", // small
				"component, \n", // large
				"which supports embedded components...\n",// regular
				" ", // button
				"\n...and embedded icons...\n", // regular
				"iconicon...icon ", // icon
				"\nJTextPane is a subclass of JEditorPane that \n"
						+ "uses a StyledEditorKit and StyledDocument, and provides \n"
						+ "cover methods for interacting with those objects.\n" };

		String[] initStyles = { "regular", "italic", "bold", "small", "large",
				"regular", "button", "regular", "icon", "regular" };

		initStylesForTextPane(textPane);

		Document doc = textPane.getDocument();

		// 0번째 부터 각각의 문자열을 스타일을 지정해서 만든다.
		try {
			for (int i = 0; i < initString.length; i++) {
				System.out.println("doc.getLength()");
				doc.insertString(doc.getLength(), initString[i],
						textPane.getStyle(initStyles[i]));// initStylesForTextPane()에
															// 의해 지정된 스타일을 꺼내서
															// 적용
			}
		} catch (BadLocationException ble) {
			System.err.println("Couldn't insert initial text.");
		}

		return textPane;
	}

	public void initStylesForTextPane(JTextPane textPane) {
		// 모든 도큐먼트에 공용되는 기본 스타일을 얻어온다.
		Style def = StyleContext.getDefaultStyleContext().getStyle(
				StyleContext.DEFAULT_STYLE);

		// 그리고 def 스타일은 SansSerif 체의 글씨로 나타낸다.
		Style style = textPane.addStyle("regular", def);
		StyleConstants.setFontFamily(style, "SansSerif");

		// 이것은 이탈리아체로 나타낸다.
		style = textPane.addStyle("italic", def);
		StyleConstants.setItalic(style, true);

		// bold는 bold체로
		style = textPane.addStyle("bold", def);
		StyleConstants.setBold(style, true);
		// small은 작게
		style = textPane.addStyle("small", def);
		StyleConstants.setFontSize(style, 10);

		// large는 크게
		style = textPane.addStyle("large", def);
		StyleConstants.setFontSize(style, 16);

		// icon은 pig.gif로
		style = textPane.addStyle("icon", def);
		StyleConstants.setAlignment(style, StyleConstants.ALIGN_LEFT);
		StyleConstants.setIcon(style, new ImageIcon("images/Pig.gif"));// 아이콘으로

		// button은 sound.gif의 아이콘을 얹은 button을 이용하며, button을 눌렀을경우 소리나게
		style = textPane.addStyle("button", def);
		StyleConstants.setAlignment(style, StyleConstants.ALIGN_CENTER);// 중앙에 위치
		JButton button = new JButton(new ImageIcon("images/sound.gif"));// 아이콘
																		// 얹은
																		// 버튼으로
		button.setMargin(new Insets(0, 0, 0, 0));// 마진지정

		button.addActionListener(new ActionListener() {// 버튼동작 지정
			public void actionPerformed(ActionEvent e) {
				Toolkit.getDefaultToolkit().beep();
			}
		});                                                                                            
		StyleConstants.setComponent(style, button);// 버튼으로 지정
	}

	public static void main(String[] args) {
		new TextPaneAdvance();
	}
}
