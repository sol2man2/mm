package edu.jaen.swing.ui3;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.event.*;
import javax.swing.text.*;

public class TextPaneBegin extends JFrame{
	private JTextPane tp;
	private JScrollPane sp;

	public TextPaneBegin(){
		super("TextPane Begin");
		tp = new JTextPane();
		sp = new JScrollPane(tp);
		sp.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);
		
		textPaneDraw();   //JTextPane에 글자쓰기
		
		this.getContentPane().add(sp);
		this.setSize(300, 500);
        this.setVisible(true);
		
		this.addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e) {
                System.exit(0);
            }
        });
	}

	public void textPaneDraw(){
		String[] name = {"Park","kkk","Nim"};
		String[] style = {"1","2","3"};
		
		setStyle();		//style정의 메소드 call
		Document tpDoc = tp.getDocument();
		try{
			for(int i=0 ; i < name.length ; i++){
				tpDoc.insertString(tpDoc.getLength(), 
							name[i]+"  ", tp.getStyle(style[i]));
            }

        } catch (BadLocationException ble) {
            System.err.println("Couldn't insert initial text.");
        }
	}

	public void setStyle(){ //style정의
		Style st = StyleContext.getDefaultStyleContext().
			getStyle(StyleContext.DEFAULT_STYLE);

		Style s = tp.addStyle("1", st);
        StyleConstants.setFontSize(s, 40);
		tp.setLogicalStyle(s);//tp.getStyle(style[0]));

		s = tp.addStyle("2", st);
		StyleConstants.setFontSize(s, 30);
        StyleConstants.setItalic(s, true);

		s = tp.addStyle("3", st);
		StyleConstants.setFontSize(s, 20);
		StyleConstants.setFontFamily(s, "SansSerif");
	}

	public static void main(String[] args){
		new TextPaneBegin();
	}
}
