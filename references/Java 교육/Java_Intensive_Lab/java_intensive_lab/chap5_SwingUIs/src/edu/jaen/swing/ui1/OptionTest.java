package edu.jaen.swing.ui1;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

public class OptionTest extends JPanel
  implements ActionListener{
  private JButton goButton;

  public OptionTest() {
    setLayout(new BorderLayout());
    goButton = new JButton("Press Me");
    add(goButton, BorderLayout.CENTER);
    goButton.addActionListener(this);
  }

  public void actionPerformed(ActionEvent ev) {
    //return void 
	JOptionPane.showMessageDialog(null, "Alert", "Warning",JOptionPane.WARNING_MESSAGE);
	System.out.println("확인");//ok눌러야 뜬다.
	
	//return int
	int value=JOptionPane.showConfirmDialog(null,"are you sure?","confirm",JOptionPane.YES_NO_CANCEL_OPTION,JOptionPane.INFORMATION_MESSAGE);
    System.out.println("value  "+value);

	//return String -->cancel하면 null return
	String input=JOptionPane.showInputDialog(this,"what's your name?","input",JOptionPane.QUESTION_MESSAGE);
	System.out.println("my name is a "+input);
	
  }

  public static void main(String args[]) {
    JFrame jf = new JFrame("Option Test");
    OptionTest ot = new OptionTest();
    jf.getContentPane().add(ot, BorderLayout.CENTER);
    jf.pack();
    jf.setVisible(true);
  }
}
