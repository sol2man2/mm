package edu.jaen.swing.ui1;
import java.awt.*;
import java.util.*;
import java.awt.event.*;
import javax.swing.*;

public class TabTest extends JPanel  implements ItemListener {

  private JTabbedPane jtp;
  private JCheckBox netCheckBox;
  private JPanel systemPane = new JPanel();//각각의 탭에 붙이기 위한 패널
  private JPanel audioPane = new JPanel();
  private JPanel internetPane = new JPanel();

  public TabTest() {
    setLayout(new BorderLayout());
    jtp = new JTabbedPane();

    netCheckBox = new JCheckBox("Internet Connection", true);
    netCheckBox.addItemListener(this);//이벤트 등록
    systemPane.add(netCheckBox);
    audioPane.add(new JLabel("Audio configuration"));
    internetPane.add(new JLabel("Internet configuration"));

    jtp.addTab("System", systemPane);
    jtp.addTab("Audio", audioPane);
    jtp.addTab("Internet", internetPane);

    add(jtp, BorderLayout.CENTER);  //전체 패널에 탭패인 넣기
  }

  public static void main(String args[]) {
    JFrame jf = new JFrame("Tabbed Pane Test");
    TabTest tt = new TabTest();
    jf.getContentPane().add(tt, BorderLayout.CENTER); //프레임에 전체패널 넣기
    jf.setSize(500, 500);
    jf.setVisible(true);
  }

  public void itemStateChanged(ItemEvent ie) {  //체크박스 선택시 호출
    int index = jtp.indexOfComponent(internetPane);  //인터넷패인의 인덱스 구하기
	System.out.println(index);
    if (index != -1) {
      jtp.setEnabledAt(index, netCheckBox.isSelected());//체크박스의 결과에 따라 인터넷 패인 사용여부 셋팅
    }
    jtp.repaint();
  }
}  
