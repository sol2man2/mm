package edu.jaen.swing.ui1;
import java.util.*;
import java.io.*;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.border.*;
 
public class ShowProgress extends JPanel
{
	JProgressBar progBar;
  Label status;
    
  public ShowProgress()
  {        
  	setLayout(new BorderLayout());
  	
    progBar = new JProgressBar();
    progBar.setMinimum(0);
    progBar.setMaximum(100);
    progBar.setValue(0);
        
    status = new Label("");
        
    add(progBar,"Center");
    add(status,"South");
 	}
    
  public void go()
  {
  	int i;

		try{                
	    for(i=0;i<=100;i++)
  	  {
    		progBar.setValue(i);                
      	Thread.sleep(200);
      	status.setText(i + "% 가 진행되었습니다");
   		}
		} catch (InterruptedException e)
  	{
  		status.setText("중지되었습니다");
  	}
           
 	}
   //JComponent의 메소드 Overriding
  public Dimension getPreferredSize()
  {
  	return new Dimension(300, 100);
  }
    
  public static void main(String s[])
  {
  	JFrame frame = new JFrame("ProgressBar Example");
    ShowProgress panel = new ShowProgress();
        
    frame.addWindowListener(new WindowCloser());
    frame.getContentPane().add(panel,"Center");
        
    frame.setSize(panel.getPreferredSize());
    frame.setVisible(true);
        
    panel.go();
	}
}

class WindowCloser extends WindowAdapter
{
    public void windowClosing(WindowEvent e)
    {
        Window win = e.getWindow();
        win.setVisible(false);
        System.exit(0);
    }
}