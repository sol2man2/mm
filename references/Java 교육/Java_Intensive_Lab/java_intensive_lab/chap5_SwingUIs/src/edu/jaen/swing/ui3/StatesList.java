package edu.jaen.swing.ui3;
import java.awt.*;
import java.awt.event.*;
import java.util.*;

import javax.swing.*;
import javax.swing.border.*;
import javax.swing.event.*;

public class StatesList extends JFrame 
{
	protected JList listView;

	public StatesList()
	{
		super("Swing List [Base]");
		setSize(500, 240);

		String [] states = {
			"AK     Alaska          Juneau",
			"AL     Alabama         Montgomery",
			"AR     Arkansas        Little Rock",
			"AZ     Arizona         Phoenix",
			"CA     California      Sacramento",
			"CO     Colorado        Denver",
			"CT     Connecticut     Hartford",
			"DE     Delaware        Dover",
			"FL     Florida         Tallahassee",
			"GA     Georgia         Atlanta",
			"HI     Hawaii          Honolulu",
			"IA     Iowa            Des Moines",
			"ID     Idaho           Boise",
			"IL     Illinois        Springfield",
			"IN     Indiana         Indianapolis",
			"KS     Kansas          Topeka",
			"KY     Kentucky        Frankfort",
			"LA     Louisiana       Baton Rouge",
			"MA     Massachusetts   Boston",
			"MD     Maryland        Annapolis",
			"ME     Maine           Augusta",
			"MI     Michigan        Lansing",
			"MN     Minnesota       St.Paul",
			"MO     Missouri        Jefferson City",
			"MS     Mississippi     Jackson",
			"MT     Montana         Helena",
			"NC     North Carolina  Raleigh",
			"ND     North Dakota    Bismarck",
			"NE     Nebraska        Lincoln",
			"NH     New Hampshire   Concord",
			"NJ     New Jersey      Trenton",
			"NM     New Mexico      SantaFe",
			"NV     Nevada          Carson City",
			"NY     New York        Albany",
			"OH     Ohio            Columbus",
			"OK     Oklahoma        Oklahoma City",
			"OR     Oregon          Salem",
			"PA     Pennsylvania    Harrisburg",
			"RI     Rhode Island    Providence",
			"SC     South Carolina  Columbia",
			"SD     South Dakota    Pierre",
			"TN     Tennessee       Nashville",
			"TX     Texas           Austin",
			"UT     Utah            Salt Lake City",
			"VA     Virginia        Richmond",
			"VT     Vermont         Montpelier",
			"WA     Washington      Olympia",
			"WV     West Virginia   Charleston",
			"WI     Wisconsin       Madison",
			"WY     Wyoming         Cheyenne"
			};

		//ListModel model=new DefaultComboBoxModel(states);
		//listView = new JList(model);
		listView = new JList(states);
		
		//JScrollPane ps = new JScrollPane();  //생성시에 초기값 지정하지 않을시
		//ps.getViewport().add(listView);
		JScrollPane ps=new JScrollPane(listView);  //생성시 초기값 지정

		getContentPane().add(ps, BorderLayout.CENTER);  //컨테이너에 추가

		addWindowListener(new WindowAdapter() { //x 버튼 처리
			public void windowClosing(WindowEvent e) 
			{
				System.exit(0);
			}
		});
		
		setVisible(true);
	}

	public static void main(String argv[]) 
	{
		new StatesList();
	}
}
