package edu.jaen.swing.ui1;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

public class SplitTest extends JPanel {

  public SplitTest() {
    JTextArea text=new JTextArea("This is one text area");
	//component의 최소size지정
	text.setMinimumSize(new Dimension(100,200));  //크기 조절시 최소크기 설정
	JSplitPane sp = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT,text,new JTextArea("This is another text area"));
    setLayout(new BorderLayout());
    add(sp, BorderLayout.CENTER);
  }

  public static void main(String args[]) {
    JFrame jf = new JFrame("SplitPane Test");
    SplitTest st = new SplitTest();
    jf.getContentPane().add(st, BorderLayout.CENTER);
    jf.setSize(500,200);
    jf.setVisible(true);
  }
}

