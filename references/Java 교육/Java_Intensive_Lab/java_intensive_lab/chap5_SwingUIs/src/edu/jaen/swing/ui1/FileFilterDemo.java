package edu.jaen.swing.ui1;
/** 
* Comment :FileFilter Demo
*/


import javax.swing.filechooser.FileFilter;
import java.io.*;

public class FileFilterDemo extends FileFilter  
{
	public String filtering(File f)
	{
		String ext = null;
        String fileName = f.getName();
        int i = fileName.lastIndexOf('.');//.앞의 문자수 리턴
        
        if(i > 0 && i < fileName.length() -1)
            ext = fileName.substring(i+1).toLowerCase();//.이후 String return
        
        return ext;
	}//end filtering

	public boolean accept(File f)
	{if(f.isDirectory())
		{  return true;}
		if(f==null)
		{	return false;}
		String ext=filtering(f);
		//System.out.println(ext);
        if(ext == null)
		{return false;}
            
		if((ext.equals("java")) || (ext.equals("html")) || (ext.equals("jsp")))
		{    System.out.println(ext=="java");
			return true;
		}
		
		
        
		return false;	        
		
	}//end accept

	public String getDescription()
	{
		return "*.java , *.html , *.jsp";
	}//end getDescription
}
